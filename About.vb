﻿Imports System.Windows.Forms

Public Class About

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub About_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim copyright As String = My.Application.Info.Copyright
        Dim versionFormat As String = "V{0}.{1}.{2}.{3}"
        Dim version As String = System.String.Format(versionFormat,
                                                        My.Application.Info.Version.Major,
                                                            My.Application.Info.Version.Minor,
                                                                My.Application.Info.Version.Build,
                                                                    My.Application.Info.Version.Revision)
        TextAbout.Text = My.Application.Info.CompanyName & vbCrLf & My.Application.Info.Description & vbCrLf & copyright & vbCrLf & version
    End Sub
End Class
