﻿#Region " Imports "
Imports System
Imports System.Threading.Tasks
Imports System.Timers
Imports System.IO.Ports
Imports System.Threading
Imports System.Text
Imports System.Net
Imports System.Net.Sockets
Imports System.Net.NetworkInformation
Imports ExtIO.S2001D.M2001D
Imports tcpmodbus.modbus
Imports ExtIO.WMISample.MyWMIQuery

#End Region

#Region " Summary "
''' <summary>
''' Extended I/O software for Master/Follower
''' Written by Turgay Bircek for
''' Beckwith Electric Inc 
''' </summary>
#End Region

Public Class Form1

#Region " Message Format "
    ''' <summary>
    ''' Message Format :
    '''  Function Byte         | Address Byte   | Port B  | Port D | Port A | LRCTotal
    '''  0x01 = Read           | 0x01 = ID1     |  data   |  data  |  data  | data
    '''  0x02 = Write          | 0x02 = ID2     |  
    '''  0x03 = Good Response  |                | 
    '''  0x83 = Bad Response   |                |             
    ''' </summary>
#End Region

#Region " Definitions "
    Public M_Breakers() As String = New String(47) {}
    Dim message2send() As Byte = New Byte() {}
    Public WithEvents udp_message_textbox As New TextBox
    Public Shared receivedUDPMessage As String
    Public Shared masters() As String = New String(15) {}
    Public Shared masterTO() As String = New String(15) {}
    Public Shared IPAddresses() As IPAddress = New IPAddress(15) {}
    Public Shared NetworkAdaptersConnected() As IPAddress = New IPAddress(9) {}
    Public Shared NetworkNumber As Integer = 0
    Public dummybit7, dummybit6, dummybit5, dummybit4, dummybit3, dummybit1, dummybit0 As New PictureBox
    Public PortB As Byte = 0
    Public PortD As Byte = 0
    Public PortA As Byte = 0
    Public Receive_Completed As Boolean = False
    Public UdpScanRunning As Boolean = False
    Dim err As Boolean = False
    Dim serialTimeOut As Integer = 4000                       'how long it will take serial port to time out in msec
    Dim udpTimeOut As Integer = 10000                         'how long it will take udpscan to time out in msec
    Public Const UdpMsgSize As Integer = ((16 * 7) - 1)       'size of udp message parsed (unit # * 8) - 1 (-1 for 0)
    Public sp As New SerialPort
    Const mdPort As UShort = 502

    Enum port As Integer
        baudrate = 9600
        databits = 8
        stopbits = 1
        parity = 0
    End Enum

    <Flags()> Enum Commands As Byte
        Read = &H1
        Write = &H2
        uCResponseOk = &H3
        uCResponseBad = &H83
    End Enum

    Enum uPnumber As Byte
        uP1 = &H1       'Line1 to Line 16
        uP2 = &H2       'Tie 1 to Tie 15
        uP3 = &H3       'Unit 1 to 8 counter output 
        uP4 = &H4       'Unit 9 to 16 counter output
        'uP5 = &H5       'All uICs
    End Enum

#End Region

#Region " Flagged Breakers ... Not In Use "
    '<Flags()> Enum breakers As UInteger
    '    alloff = 0
    '    line1 = 2 ^ 0
    '    line2 = 2 ^ 1
    '    line3 = 2 ^ 2
    '    line4 = 2 ^ 3
    '    line5 = 2 ^ 4
    '    line6 = 2 ^ 5
    '    line7 = 2 ^ 6
    '    line8 = 2 ^ 7
    '    line9 = 2 ^ 8
    '    line10 = 2 ^ 9
    '    line11 = 2 ^ 10
    '    line12 = 2 ^ 11
    '    line13 = 2 ^ 12
    '    line14 = 2 ^ 13
    '    line15 = 2 ^ 14
    '    line16 = 2 ^ 15
    '    tie1 = 2 ^ 16
    '    tie2 = 2 ^ 17
    '    tie3 = 2 ^ 18
    '    tie4 = 2 ^ 19
    '    tie5 = 2 ^ 20
    '    tie6 = 2 ^ 21
    '    tie7 = 2 ^ 22
    '    tie8 = 2 ^ 23
    '    tie9 = 2 ^ 24
    '    tie10 = 2 ^ 25
    '    tie11 = 2 ^ 26
    '    tie12 = 2 ^ 27
    '    tie13 = 2 ^ 28
    '    tie14 = 2 ^ 29
    '    tie15 = 2 ^ 30
    '    allon = (2 ^ 31) - 1
    'End Enum
#End Region

#Region " Form Load & Close ... etc "

    Private Sub Form1_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        CheckedControls(AutoControl, True)
        My.Settings.MyLocation = Me.Location
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '************************************************************
        '*  Verify the form is in visible area                      *
        '*  If NOT reset location to 0,0                            *
        '************************************************************
        Dim x As Integer = My.Computer.Screen.WorkingArea.Left
        Dim y As Integer = My.Computer.Screen.WorkingArea.Right
        If My.Settings.MyLocation.X < x Or My.Settings.MyLocation.X > y Then
            Me.DesktopLocation = New System.Drawing.Point(0, 0)
        Else
            Me.Location = My.Settings.MyLocation
        End If
        '************************************************************
        raise = New PictureBox() {raise1, raise2, raise3, raise4, raise5, raise6, raise7, raise8,
                                  raise9, raise10, raise11, raise12, raise13, raise14, raise15, raise16}
        lower = New PictureBox() {lower1, lower2, lower3, lower4, lower5, lower6, lower7, lower8,
                                  lower9, lower10, lower11, lower12, lower13, lower14, lower15, lower16}
        alarms = New PictureBox() {alarm1, alarm2, alarm3, alarm4, alarm5, alarm6, alarm7, alarm8,
                                  alarm9, alarm10, alarm11, alarm12, alarm13, alarm14, alarm15, alarm16}
        Dim copyright As String = My.Application.Info.Copyright
        Dim versionFormat As String = " (V{0}.{1})" '.{2})" '.{3})"
        'Dim version As String = System.String.Format(versionFormat,
        '                                                My.Application.Info.Version.Major,
        '                                                    My.Application.Info.Version.Minor,
        '                                                        My.Application.Info.Version.Build,
        '                                                            My.Application.Info.Version.Revision)
        Dim version As String = System.String.Format(versionFormat,
                                                        My.Application.Info.Version.Major,
                                                           My.Application.Info.Version.Revision)
        Me.Text = My.Application.Info.Title & " By " & copyright & " " & version
        ComboBox1.Items.AddRange(SerialPort.GetPortNames)
        ComboBox1.SelectedIndex = -1
        Led_Status()
        GetNICS()
    End Sub

    Private Sub GetNICS()
        Dim nics() As NetworkInterface = NetworkInterface.GetAllNetworkInterfaces

        If Not nics Is Nothing OrElse Not nics.Length < 1 Then
            For Each adapter In nics
                Dim properties As IPInterfaceProperties = adapter.GetIPProperties()

                For Each unicast As IPAddressInformation In properties.UnicastAddresses
                    If unicast.Address.AddressFamily = AddressFamily.InterNetwork And Not (adapter.Description.ToLower().Contains("loopback")) Then
                        If OSVersion() = "Microsoft Windows 7 Professional " Then
                            If (NetworkAdaptersAvailable.DropDownWidth / 5) < (adapter.Description.Length + adapter.GetIPProperties.UnicastAddresses.Item(1).Address.ToString.Length) Then
                                NetworkAdaptersAvailable.DropDownWidth = (adapter.Description.Length + unicast.Address.ToString.Length) * 6
                            End If
                        End If
                        NetworkAdaptersAvailable.Items.Add(unicast.Address.ToString() & " - " & adapter.Description)
                        NetworkAdaptersConnected.SetValue(unicast.Address, NetworkNumber)
                    End If
                Next
                NetworkNumber += 1
            Next
        End If
        NetworkNumber = 0
    End Sub
#End Region

#Region " LED Status "
    Private Sub Led_Status()

        SetBreakers("000000000000000000000000", &H1)
        SetBreakers("000000000000000000000000", &H2)

    End Sub
#End Region

#Region " ComboBoxes "
    Private Sub ComboBox1_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles ComboBox1.MouseClick, ComboBox1.MouseDown, ComboBox1.Click
        ComboBox1.Items.Clear()
        ComboBox1.Items.AddRange(SerialPort.GetPortNames)
        ComboBox1.SelectedIndex = -1
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If Not SerialPort1.IsOpen AndAlso Not ComboBox1.SelectedIndex = -1 Then
            SerialPort1.PortName = ComboBox1.SelectedItem.ToString
            ButtonConnect.Enabled = True
        End If
    End Sub

    Private Sub NetworkAdaptersAvailable_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles NetworkAdaptersAvailable.MouseClick, NetworkAdaptersAvailable.MouseDown, NetworkAdaptersAvailable.Click
        NetworkAdaptersAvailable.Items.Clear()
        GetNICS()
    End Sub

    Private Sub NetworkAdaptersAvailable_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles NetworkAdaptersAvailable.SelectedIndexChanged
        If Not NetworkAdaptersAvailable.SelectedIndex = -1 Then
            ConnectAdapter.Enabled = True
        End If
    End Sub
#End Region

#Region " Data Receive Handler "
    Private Sub Receiver(ByVal sender As System.Object, ByVal e As  _
                            System.IO.Ports.SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived

        Dim i As Integer = 0
        Dim ReceivingBuffer() As Byte = New Byte(SerialPort1.BytesToRead) {}
        If Not ReceivingBuffer.Length < 6 Then
            Try
                Do
                    SerialPort1.Read(ReceivingBuffer, 0, SerialPort1.BytesToRead)
                Loop Until SerialPort1.BytesToRead = 0

                Dim func As Byte = ReceivingBuffer.GetValue(0)
                Dim address As Byte = ReceivingBuffer.GetValue(1)
                Dim PortBReceiveData As Byte = ReceivingBuffer.GetValue(2)
                Dim PortDReceiveData As Byte = ReceivingBuffer.GetValue(3)
                Dim PortAReceiveData As Byte = ReceivingBuffer.GetValue(4)
                Dim breakerStatus As String = IntToBin(ReceivingBuffer.GetValue(2)) & IntToBin(ReceivingBuffer.GetValue(3)) & IntToBin(ReceivingBuffer.GetValue(4))
                Dim LRC_Received As Byte = ReceivingBuffer.GetValue(5)
                Dim LRC_Calculated As Byte = LRCCalculator(func, address, PortBReceiveData, PortDReceiveData, PortAReceiveData)
               
                ' If LRC_Received = LRC_Calculated Then
                Select Case func
                    Case &H3   'Good response
                        Receive_Completed = True
                        SetBreakers(breakerStatus, address)
                    Case &H83   'Bad Response
                        ''Should never get this message. Processors will ignore errors and clear their receiver buffer
                        SetTextMsgCenter(ConnectionStatusLabel, " Corrupted serial data ")
                End Select
                'Else
                'SetTextMsgCenter(ConnectionStatusLabel, " Serial data errors ")
                'End If
            Catch ex As Exception
                SetTextMsgCenter(ConnectionStatusLabel, " Bad data ")
            End Try
        End If
    End Sub
#End Region

#Region " Wait for receiving completed "
    Private Function WaitForReceivingCompleted() ' As Boolean

        SetTextMsgCenter(ConnectionStatusLabel, " Connecting ... ")
        Dim result As Boolean = True
        ValueProgressBar(UdpProgressBar, 0)
        MaxValuePBar(UdpProgressBar, serialTimeOut)

        Dim watch As New Stopwatch()
        watch.Start()
        Try
            While watch.IsRunning
                ValueProgressBar(UdpProgressBar, watch.ElapsedMilliseconds)
                If watch.ElapsedMilliseconds >= serialTimeOut Then
                    watch.Stop()
                    Throw New Exception
                End If
                If Receive_Completed Then
                    Exit While
                End If
            End While
            SetTextMsgCenter(ConnectionStatusLabel, " Connected ")
            SetText(Label119, " ")
        Catch ex As Exception
            result = False
            SetTextMsgCenter(ConnectionStatusLabel, " Not Connected ")
            SetText(Label119, " No response from serial port ...")
        End Try
        watch.Reset()
        ValueProgressBar(UdpProgressBar, 0)
        Return result
    End Function
#End Region

#Region " Integer to Binary "
    Function IntToBin(ByVal IntegerNumber As Byte) As String

        Dim IntNum As UInteger = IntegerNumber
        Dim TempValue As UInteger = 0
        Dim BinValue As String = ""
        Dim n As Integer = 0       '3 byte
        Do
            'Use the Mod operator to get the current binary digit from the
            'Integer number
            TempValue = IntNum Mod 2
            BinValue = CStr(TempValue) + BinValue

            n += 1      'for leading zeros

            'Divide the current number by 2 and get the integer result
            IntNum = IntNum \ 2
            'Loop Until IntNum = 0
        Loop Until n = 8

        IntToBin = BinValue

    End Function
#End Region

#Region " Binary to Integer "
    Function BinToInt(ByVal BinaryNumber As String)

        'Get the length of the binary string
        Dim Length As Integer = Len(BinaryNumber)
        Dim TempValue As Integer
        'Convert each binary digit to its corresponding integer value
        'and add the value to the previous sum
        'The string is parsed from the right (LSB - Least Significant Bit)
        'to the left (MSB - Most Significant Bit)
        For x = 1 To Length
            TempValue = TempValue + Val(Mid(BinaryNumber, Length - x + 1, 1)) * 2 ^ (x - 1)
        Next

        BinToInt = TempValue

    End Function

#End Region

#Region " Set Breakers Displays per actual reading from processors "
    Private Sub SetBreakers(ByVal BreakersStatus As String, ByVal address As Byte)
        Dim n As Integer = 0
        Select Case address '(address And &HF8)
            Case &H0, &H1
                Dim uPLineControls() As System.Object =
                {Me.Unit1LineBreaker, Me.Unit2LineBreaker, Me.Unit3LineBreaker, Me.Unit4LineBreaker, Me.Unit5LineBreaker, Me.Unit6LineBreaker, Me.Unit7LineBreaker, Me.Unit8LineBreaker,
                 Me.dummybit7, Me.Unit9LineBreaker, Me.Unit10LineBreaker, Me.Unit11LineBreaker, Me.Unit12LineBreaker, Me.Unit13LineBreaker, Me.dummybit1, Me.dummybit0,
                 Me.dummybit7, Me.dummybit6, Me.dummybit5, Me.dummybit4, Me.dummybit3, Me.Unit14LineBreaker, Me.Unit15LineBreaker, Me.Unit16LineBreaker}

                For Each linebreaker In uPLineControls
                    Select Case BreakersStatus.Substring(n, 1)
                        Case "1"
                            linebreaker.image = My.Resources.Closed_Line
                            linebreaker.tag = 1
                        Case Else
                            linebreaker.image = My.Resources.Open_Line
                            linebreaker.tag = 0
                    End Select
                    M_Breakers.SetValue(linebreaker.tag.ToString, n)
                    'M_Breakers.SetValue(linebreaker.name, n)
                    n += 1
                Next
                n = 0

                Dim LedStatus() As System.Object =
                {Me.Led1, Me.Led2, Me.Led3, Me.Led4, Me.Led5, Me.Led6, Me.Led7, Me.Led8,
                Me.dummybit7, Me.Led9, Me.Led10, Me.Led11, Me.Led12, Me.Led13, Me.dummybit1, Me.dummybit0,
                Me.dummybit7, Me.dummybit6, Me.dummybit5, Me.dummybit4, Me.dummybit3, Me.Led14, Me.Led15, Me.Led16}

                For Each led In LedStatus
                    Select Case BreakersStatus.Substring(n, 1)
                        Case "1"
                            led.backcolor = Color.Blue
                            'linebreaker.tag = 1
                        Case Else
                            led.backcolor = Color.LightGray
                    End Select
                    n += 1
                Next
                'MessageBox.Show("ID = 1 " & BreakersStatus)
            Case &H0, &H2
                Dim uPLineControls_2() As System.Object =
                {Me.Unit1TieBreaker, Me.Unit2TieBreaker, Me.Unit3TieBreaker, Me.Unit4TieBreaker, Me.Unit5TieBreaker, Me.Unit6TieBreaker, Me.Unit7TieBreaker, Me.Unit8TieBreaker,
                 Me.dummybit7, Me.Unit9TieBreaker, Me.Unit10TieBreaker, Me.Unit11TieBreaker, Me.Unit12TieBreaker, Me.Unit13TieBreaker, Me.dummybit1, Me.dummybit0,
                 Me.dummybit7, Me.dummybit6, Me.dummybit5, Me.dummybit4, Me.dummybit3, Me.Unit14TieBreaker, Me.Unit15TieBreaker, Me.dummybit0}


                For Each linebreaker In uPLineControls_2
                    Select Case BreakersStatus.Substring(n, 1)
                        Case "1"
                            linebreaker.image = My.Resources.Closed_Tie
                            linebreaker.tag = 1
                        Case Else
                            linebreaker.image = My.Resources.Open_Tie
                            linebreaker.tag = 0
                    End Select
                    M_Breakers.SetValue(linebreaker.tag.ToString, n + 24)
                    'M_Breakers.SetValue(linebreaker.name, n + 24)
                    n += 1
                Next
                n = 0

                Dim LedStatus_2() As System.Object =
                {Me.Led17, Me.Led18, Me.Led19, Me.Led20, Me.Led21, Me.Led22, Me.Led23, Me.Led24,
                 Me.dummybit7, Me.Led25, Me.Led26, Me.Led27, Me.Led28, Me.Led29, Me.dummybit1, Me.dummybit0,
                 Me.dummybit7, Me.dummybit6, Me.dummybit5, Me.dummybit4, Me.dummybit3, Me.Led30, Me.Led31, Me.dummybit0}
                For Each led In LedStatus_2
                    Select Case BreakersStatus.Substring(n, 1)
                        Case "1"
                            led.backcolor = Color.Blue
                            'linebreaker.tag = 1
                        Case Else
                            led.backcolor = Color.LightGray
                    End Select
                    n += 1
                Next
        End Select
    End Sub
#End Region

#Region " Transfer To Microprocessor "
    Private Sub Write2Processor()
        If ButtonConnect.Enabled Then
            Try
                Dim messageitems As String = ""
                SerialPort1.DiscardInBuffer()
                SerialPort1.DiscardOutBuffer()
                Receive_Completed = False
                Dim offset As Integer
                For offset = 0 To message2send.Length - 1
                    SerialPort1.Write(message2send, offset, 1)
                    Console.Write(String.Format("{0:X2} ", message2send(offset)))
                Next
                Console.WriteLine("")
                'Timer1.Enabled = True
                'WaitForReceivingCompleted()   '(Nothing, Nothing)
            Catch ex As Exception
                'MessageBox.Show("Error in comms")
                SetTextMsgCenter(ConnectionStatusLabel, " Errors ")
            End Try
        End If
    End Sub

    Private Sub Write2Processor(ByVal ParamArray _message() As Byte)
        If ButtonConnect.Enabled Then
            Try
                SerialPort1.DiscardInBuffer()
                SerialPort1.DiscardOutBuffer()
                Receive_Completed = False
                Dim offset As Integer
                For offset = 0 To _message.Length - 1
                    SerialPort1.Write(_message, offset, 1)
                Next
                'Timer1.Enabled = True
                'WaitForReceivingCompleted()   '(Nothing, Nothing)
            Catch ex As Exception
                'MessageBox.Show("Error in comms")
                SetTextMsgCenter(ConnectionStatusLabel, " Errors ")
            End Try
        End If
    End Sub
#End Region

#Region " Write to Outputs "
    Private Sub Write2Outputs(ByVal sender As Object)
        Dim address As Byte = 0
        Dim changedata As String = ""
        Dim data2SendPortB As Byte = 0
        Dim data2SendPortD As Byte = 0
        Dim data2SendPortA As Byte = 0
        Dim bit2change As Integer = CInt(sender.AccessibleName) '- 1     'compensate bit 0 vs accessiblename 1

        M_Breakers.SetValue(sender.tag.ToString, bit2change)

        For Each bitvalue In M_Breakers
            changedata &= bitvalue
        Next

        Select Case bit2change
            Case 0 To 23
                'changeaddress = BinToInt(changedata.Substring(0, 4))
                data2SendPortB = BinToInt(changedata.Substring(0, 8))
                data2SendPortD = BinToInt(changedata.Substring(8, 8))
                data2SendPortA = BinToInt(changedata.Substring(16, 8))
                address = &H1
                ' MessageBox.Show("ID 1 = " & Hex(changeaddress) & " " & Hex(data2send))
                message2send = {Commands.Write, address, data2SendPortB, data2SendPortD, data2SendPortA,
                                LRCCalculator(Commands.Write, address, data2SendPortB, data2SendPortD, data2SendPortA)}
                Receive_Completed = False
                Write2Processor()
                WaitForReceivingCompleted() '(Nothing, Nothing)
            Case 24 To 47
                'changeaddress = BinToInt(changedata.Substring(12, 4))
                data2SendPortB = BinToInt(changedata.Substring(24, 8))
                data2SendPortD = BinToInt(changedata.Substring(32, 8))
                data2SendPortA = BinToInt(changedata.Substring(40, 8))
                address = &H2
                message2send = {Commands.Write, address, data2SendPortB, data2SendPortD, data2SendPortA,
                                LRCCalculator(Commands.Write, address, data2SendPortB, data2SendPortD, data2SendPortA)}
                Receive_Completed = False
                Write2Processor()
                WaitForReceivingCompleted() '(Nothing, Nothing)
        End Select
    End Sub
#End Region

#Region " First Communication "
    Private Function Inquire() As Boolean
        Dim inquiry_1() As Byte =
        {Commands.Read, uPnumber.uP1, PortB, PortD, PortA, LRCCalculator(Commands.Read, uPnumber.uP1, PortB, PortD, PortA)}
        Dim inquiry_2() As Byte =
        {Commands.Read, uPnumber.uP2, PortB, PortD, PortA, LRCCalculator(Commands.Read, uPnumber.uP2, PortB, PortD, PortA)}

        If Not SerialPort1.IsOpen Then
            Try
                AddHandler SerialPort1.DataReceived, AddressOf Receiver
                With SerialPort1
                    .BaudRate = port.baudrate
                    .DataBits = port.databits
                    .StopBits = port.stopbits
                    .Parity = Parity.None
                End With
                SerialPort1.Open()

                Dim offset As Integer
                For offset = 0 To inquiry_1.Length - 1
                    SerialPort1.Write(inquiry_1, offset, 1)
                Next

                Receive_Completed = False
                err = WaitForReceivingCompleted()

                If SerialPort1.IsOpen Then
                    For offset = 0 To inquiry_2.Length - 1
                        SerialPort1.Write(inquiry_2, offset, 1)
                    Next
                    Receive_Completed = False
                    err = WaitForReceivingCompleted() Or err

                Else
                    err = True
                End If
            Catch ex As Exception
                SetTextMsgCenter(ConnectionStatusLabel, " Failed to open ")
                err = True
            End Try
        End If
        Return err
    End Function
#End Region

#Region " Line Breaker Status Update "
    Event GetLineBreakerStatus(ByVal sender As System.Object)

    Private Sub UpdateLineBreakerStatus(ByVal sender As System.Object) Handles Me.GetLineBreakerStatus
        Select Case sender.tag
            Case 0
                sender.image = My.Resources.Closed_Line
                sender.tag = 1
                ' button_all_off.Checked = False
            Case 1
                sender.image = My.Resources.Open_Line
                sender.tag = 0
                'button_all_on.Checked = False
        End Select
        Receive_Completed = False
        Write2Outputs(sender)
        WaitForReceivingCompleted() '(Nothing, Nothing)
        'aTimer.Start()
    End Sub
#End Region

#Region " Tie Breakers Status Update "
    Event GetTieBreakerStatus(ByVal sender As System.Object)

    Private Sub UpdateTieBreakerStatus(ByVal sender As System.Object) Handles Me.GetTieBreakerStatus
        Select Case sender.tag
            Case 0
                sender.image = My.Resources.Closed_Tie
                sender.tag = 1
                'button_all_off.Checked = False
            Case 1
                sender.image = My.Resources.Open_Tie
                sender.tag = 0
                'button_all_on.Checked = False
        End Select
        Receive_Completed = False
        Write2Outputs(sender)
        WaitForReceivingCompleted() '(Nothing, Nothing)
        'aTimer.Start()
    End Sub
#End Region

#Region " Line Breakers "
    Private Sub LineBreakers(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Unit1LineBreaker.MouseUp,
                                            Unit2LineBreaker.MouseUp, Unit3LineBreaker.MouseUp, Unit4LineBreaker.MouseUp, Unit5LineBreaker.MouseUp,
                                            Unit6LineBreaker.MouseUp, Unit7LineBreaker.MouseUp, Unit8LineBreaker.MouseUp, Unit9LineBreaker.MouseUp,
                                            Unit10LineBreaker.MouseUp, Unit11LineBreaker.MouseUp, Unit12LineBreaker.MouseUp, Unit13LineBreaker.MouseUp,
                                            Unit14LineBreaker.MouseUp, Unit15LineBreaker.MouseUp, Unit16LineBreaker.MouseUp
        If ButtonConnect.Enabled Then
            'aTimer.Stop()
            RaiseEvent GetLineBreakerStatus(sender)
        End If
    End Sub
#End Region

#Region " Tie Breakers "
    Private Sub TieBreakers(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Unit1TieBreaker.MouseUp,
                                            Unit2TieBreaker.MouseUp, Unit3TieBreaker.MouseUp, Unit4TieBreaker.MouseUp, Unit5TieBreaker.MouseUp,
                                            Unit6TieBreaker.MouseUp, Unit7TieBreaker.MouseUp, Unit8TieBreaker.MouseUp, Unit9TieBreaker.MouseUp,
                                            Unit10TieBreaker.MouseUp, Unit11TieBreaker.MouseUp, Unit12TieBreaker.MouseUp, Unit13TieBreaker.MouseUp,
                                            Unit14TieBreaker.MouseUp, Unit15TieBreaker.MouseUp
        If ButtonConnect.Enabled Then
            'aTimer.Stop()
            RaiseEvent GetTieBreakerStatus(sender)
        End If
    End Sub
#End Region

#Region " Clear Labels "
    Dim labels() As Label

    Private Sub ClearLabels()
        labels = New Label() {Label98, Label99, Label100, Label101,
                             Label102, Label103, Label104, Label105,
                             Label106, Label107, Label108, Label109,
                             Label110, Label111, Label112, Label113}
        For Each lab In labels
            SetText(lab, "")
        Next
    End Sub
#End Region

#Region " Delegates "
    Private Delegate Sub EnableControlsDelegate(ByVal myControl As Control, ByVal enabled As Boolean)
    Private Delegate Sub VisibleControlsDelegate(ByVal myControl As Object, ByVal visible As Boolean)
    Private Delegate Sub SetTextDelegate(ByVal myControl As Control, ByVal itsText As String)
    Private Delegate Sub SetTextMsgCenterDelegate(ByVal myControl As Windows.Forms.ToolStripStatusLabel, ByVal itsText As String)
    Private Delegate Sub ValueProgressBarDelegate(ByVal myControl As Windows.Forms.ToolStripProgressBar, ByVal itsValue As Integer)
    Private Delegate Sub MaxValuePBarDelegate(ByVal mycontrol As ToolStripProgressBar, ByVal itsMaxValue As Integer)
    Private Delegate Sub CheckedControlsDelegate(ByVal myControl As RadioButton, ByVal checked As Boolean)

    Private Sub CheckedControls(ByVal myControl As RadioButton, ByVal checked As Boolean)
        If Me.InvokeRequired Then
            Dim del As New CheckedControlsDelegate(AddressOf CheckedControls)
            Me.Invoke(del, New Object() {myControl, checked})
        Else
            myControl.Checked = checked
        End If
    End Sub

    Private Sub MaxValuePBar(ByVal myControl As ToolStripProgressBar, ByVal itsMaxValue As Integer)
        If Me.StatusStrip1.InvokeRequired Then
            Dim del As New MaxValuePBarDelegate(AddressOf MaxValuePBar)
            Me.StatusStrip1.Invoke(del, New Object() {myControl, itsMaxValue})
        Else
            myControl.Maximum = itsMaxValue
        End If
    End Sub

    Private Sub EnableControls(ByVal myControl As Control, ByVal enabled As Boolean)
        If Me.InvokeRequired Then
            Dim del As New EnableControlsDelegate(AddressOf EnableControls)
            Me.Invoke(del, New Object() {myControl, enabled})
        Else
            myControl.Enabled = enabled
        End If
    End Sub

    Private Sub VisibilityControls(ByVal myControl As Object, ByVal visible As Boolean)
        If Me.InvokeRequired Then
            Dim del As New VisibleControlsDelegate(AddressOf VisibilityControls)
            Me.Invoke(del, New Object() {myControl, visible})
        Else
            myControl.Visible = visible
        End If
    End Sub

    Public Sub SetText(ByVal myControl As Control, ByVal itsText As String)
        If myControl.InvokeRequired Then
            Dim del As New SetTextDelegate(AddressOf SetText)
            myControl.Invoke(del, New Object() {myControl, itsText})
        Else
            myControl.Text = itsText
        End If
    End Sub

    Private Sub ValueProgressBar(ByVal myControl As Windows.Forms.ToolStripProgressBar, ByVal itsValue As Integer)
        If Me.StatusStrip1.InvokeRequired Then
            Dim del As New ValueProgressBarDelegate(AddressOf ValueProgressBar)
            Me.StatusStrip1.Invoke(del, New Object() {myControl, itsValue})
        Else
            If itsValue > myControl.Maximum Then
                itsValue = myControl.Maximum
            End If
            myControl.Value = itsValue
        End If
    End Sub

    Public Sub SetTextMsgCenter(ByVal myControl As Windows.Forms.ToolStripStatusLabel, ByVal itsText As String)
        If Me.StatusStrip1.InvokeRequired Then
            Dim del As New SetTextMsgCenterDelegate(AddressOf SetTextMsgCenter)
            Me.StatusStrip1.Invoke(del, New Object() {myControl, itsText})
            'Me.StatusStrip1.Refresh()
        Else
            myControl.Text = itsText
            Me.StatusStrip1.Refresh()
        End If
    End Sub
#End Region

#Region " Timers "
    Dim UdpScanRate As Integer                            'how often will scan udp devices in msec
    Dim aTimer As System.Timers.Timer = New System.Timers.Timer
    Dim counter As Integer = 0

    Private Async Sub OnTimedEvent(ByVal source As Object, ByVal e As ElapsedEventArgs)
        ' Private Sub OnTimedEvent(ByVal source As Object, ByVal e As ElapsedEventArgs)
        aTimer.Interval = (UdpRefreshRate.Value * 1000) + 1
        If ConnectAdapter.Enabled Then
            Await TaskEx.Run(Sub() TapPositions())
            counter += 1
            If counter = CInt(2000 / aTimer.Interval) Then        'every 10 seconds udp status update
                Await TaskEx.Run(Sub() NetworkDevices_Click(Nothing, Nothing))
                counter = 0
            End If
        End If
    End Sub

    Private Sub TimerStart()
        If Not UdpScanRunning Then
            aTimer = New System.Timers.Timer(UdpScanRate)
            AddHandler aTimer.Elapsed, AddressOf OnTimedEvent
            aTimer.Enabled = True
            aTimer.AutoReset = True
            'GC.KeepAlive(aTimer)
            UdpScanRunning = True
        End If
    End Sub
#End Region

#Region " Dis/connect buttons "
    Private Async Sub ButtonConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonConnect.Click

        Dim commResult As Boolean
        Select Case ButtonConnect.Text
            Case "Connect"
                commResult = (Await TaskEx.Run(Function() Inquire()))
                If commResult Then
                    SetTextMsgCenter(ConnectionStatusLabel, " Connected ")
                    EnableControls(ComboBox1, False)
                    SetText(ButtonConnect, "Disconnect")
                    button_all_on.Enabled = True
                    button_all_off.Enabled = True
                    ConnectionStatusLabel.BackColor = Color.Green
                    LedBlock.Visible = True
                    MainBlock.Visible = True
                Else
                    SerialPort1.Close()
                    ComboBox1.Enabled = True
                    ComboBox1.SelectedIndex = -1
                    ButtonConnect.Enabled = False
                    button_all_on.Enabled = False
                    button_all_off.Enabled = False
                    ConnectionStatusLabel.BackColor = Color.Red
                    ConnectionStatusLabel.Text = " Disconnected "
                End If

            Case "Disconnect"
                SerialPort1.Close()
                ButtonConnect.Text = "Connect"
                ComboBox1.Enabled = True
                ComboBox1.SelectedIndex = -1
                Led_Status()
                ConnectionStatusLabel.BackColor = Color.Red
                ConnectionStatusLabel.Text = " Disconnected "
                ButtonConnect.Enabled = False
                button_all_on.Enabled = False
                button_all_off.Enabled = False
                LedBlock.Visible = False
        End Select
    End Sub

    Private Async Sub ConnectAdapter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConnectAdapter.Click
        Select Case ConnectAdapter.Text
            Case "Connect"
                modbus.IPAddresses = Nothing
                SetText(ConnectAdapter, "Disconnect")
                EnableControls(NetworkAdaptersAvailable, False)
                EnableControls(ConnectAdapter, True)
                EnableControls(UdpRefreshRate, False)
                NetworkNumber = NetworkAdaptersAvailable.SelectedIndex
                Await TaskEx.Run(Sub() NetworkDevices_Click(Nothing, Nothing))
                MainBlock.Visible = True
            Case "Disconnect"
                Array.Clear(udp_msg_list, 0, udp_msg_list.Length)
                Array.Clear(masters, 0, masters.Length)
                Array.Clear(UDPMessageInOrder, 0, UDPMessageInOrder.Length)
                Array.Clear(testip, 0, testip.Length)
                SetText(ConnectAdapter, "Connect")
                EnableControls(ConnectAdapter, False)
                EnableControls(NetworkAdaptersAvailable, True)
                EnableControls(UdpRefreshRate, True)
                NetworkAdaptersAvailable.SelectedIndex = -1
                aTimer.Stop()
                aTimer.Close()
                UdpScanRunning = False
                MainBlock.Visible = False
                'Array.Clear(server, 0, server.Length)       'reset ip addresses to scan
        End Select

    End Sub
#End Region

#Region " LRC Calculator "
    ''' <summary>
    ''' Checksum calculated here
    ''' </summary>
    ''' <param name="command">can be read, write</param>
    ''' <param name="address">can be only 1 and 2. There is no communication for 3 and 4</param>
    ''' <returns>LRC total checksum</returns>
    Public Function LRCCalculator(ByVal command As Byte,
        ByVal address As Byte, ByVal PortBData As Byte,
        ByVal PortDData As Byte, ByVal PortAData As Byte) As Byte
        Dim value As UInt64 = 0
        Dim LRCTotal As Byte = 0

        value = CInt(command) + CInt(address) + CInt(PortBData) + CInt(PortDData) + CInt(PortAData)

        LRCTotal = CByte(value And CInt(&HFF))
        Return LRCTotal
    End Function
#End Region

#Region " All on/off"
    Private Sub AllBreakers(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_all_on.MouseClick,
                                                                                                button_all_off.MouseClick
        If SerialPort1.IsOpen Then
            Select Case sender.text
                Case button_all_on.Text
                    message2send = {&H2, &H1, &HFF, &H7C, &H7, &H85}
                    Write2Processor()
                    WaitForReceivingCompleted() '(Nothing, Nothing)
                    Receive_Completed = False
                    message2send = {&H2, &H2, &HFF, &H7C, &H6, &H85}
                    Write2Processor()
                    WaitForReceivingCompleted() '(Nothing, Nothing)
                    Receive_Completed = False
                Case button_all_off.Text
                    'MsgBox(sender.text)
                    message2send = {&H2, &H1, &H0, &H0, &H0, &H3}
                    Write2Processor()
                    WaitForReceivingCompleted() '(Nothing, Nothing)
                    Receive_Completed = False
                    message2send = {&H2, &H2, &H0, &H0, &H0, &H4}
                    Write2Processor()
                    WaitForReceivingCompleted() '(Nothing, Nothing)
                    Receive_Completed = False
            End Select
        End If
        button_all_on.Checked = False
        button_all_off.Checked = False
    End Sub
#End Region

#Region " LED Click Support "
    Private Sub Led_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Led1.Click, Led2.Click, Led3.Click,
                                                Led4.Click, Led5.Click, Led6.Click, Led7.Click, Led9.Click, Led8.Click, Led31.Click,
                                                Led30.Click, Led29.Click, Led28.Click, Led27.Click, Led26.Click, Led25.Click, Led24.Click,
                                                Led23.Click, Led22.Click, Led21.Click, Led20.Click, Led19.Click, Led18.Click, Led17.Click,
                                                Led16.Click, Led15.Click, Led14.Click, Led13.Click, Led12.Click, Led11.Click, Led10.Click

        If ButtonConnect.Enabled Then
            Dim _senders() As Object = New Object() {Nothing, Unit1LineBreaker, Unit2LineBreaker, Unit3LineBreaker, Unit4LineBreaker,
                                                     Unit5LineBreaker, Unit6LineBreaker, Unit7LineBreaker, Unit8LineBreaker,
                                                     Unit9LineBreaker, Unit10LineBreaker, Unit11LineBreaker, Unit12LineBreaker,
                                                     Unit13LineBreaker, Unit14LineBreaker, Unit15LineBreaker, Unit16LineBreaker,
                                                     Unit1TieBreaker, Unit2TieBreaker, Unit3TieBreaker, Unit4TieBreaker, Unit5TieBreaker,
                                                     Unit6TieBreaker, Unit7TieBreaker, Unit8TieBreaker, Unit9TieBreaker, Unit10TieBreaker,
                                                     Unit11TieBreaker, Unit12TieBreaker, Unit13TieBreaker, Unit14TieBreaker, Unit15TieBreaker}

            Dim _breaker As Integer = CInt(sender.name.ToString.Substring(3))
            MsgBox("Item clicked: " & _breaker & vbCrLf & "Sending: " & _senders.GetValue(_breaker).name.ToString)
            RaiseEvent GetLineBreakerStatus(_senders.GetValue(_breaker))
        End If
    End Sub
#End Region

#Region " Udp Message Parser "
    Public udp_msg_list() As String = New String(UdpMsgSize) {}
    Public Shared UDPMessageInOrder() As String = New String(15) {}
    Public followers() As String = New String(15) {}
    Dim udpRunning As Boolean = False

    Private Sub Text_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles udp_message_textbox.TextChanged
        receivedUDPMessage = udp_message_textbox.Text
    End Sub

    Private Sub NetworkDevices_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NetworkDevices.Click
        Dim DiscoverNetworkDevicesThread As New Thread(AddressOf DiscoverDevices)
        receivedUDPMessage = Nothing
        If Not DiscoverNetworkDevicesThread.IsAlive Then
            DiscoverNetworkDevicesThread.Start()
        End If

        EnableControls(NetworkDevices, False)
        ValueProgressBar(UdpProgressBar, 0)
        VisibilityControls(UdpProgressBar, True)
        Dim aStopWatch As New Stopwatch
        aStopWatch.Start()
        Try
            While aStopWatch.IsRunning
                ValueProgressBar(UdpProgressBar, aStopWatch.ElapsedMilliseconds)
                'ValueProgressBar(ProgressBar1, aStopWatch.ElapsedMilliseconds)
                If aStopWatch.ElapsedMilliseconds > udpTimeOut Then
                    aStopWatch.Stop()
                    Throw New Exception
                End If
                If Not String.IsNullOrEmpty(receivedUDPMessage) Then
                    Exit While
                End If
            End While
        Catch ex As Exception
            'SetTextMsgCenter(ConnectionStatusLabel, " Time out ")
            'SetText(UdpProgressBarLabel, " Timed out ")
        End Try
        Try
            If ButtonConnect.Enabled Or ConnectAdapter.Enabled Then
                'SetTextMsgCenter(ConnectionStatusLabel, " Updated ")
                'SetText(UdpProgressBarLabel, " Updated ")
                'SetProgressBarText(ProgressBar1, " Updated ", ProgressBarTextLocation.Centered, Color.Black) ', System.Drawing.SystemFonts.DefaultFont)
            End If
            ValueProgressBar(UdpProgressBar, 0)
            'VisibilityControls(UdpProgressBar, False)


            udp_msg_list = receivedUDPMessage.Split(",")
            Array.Resize(udp_msg_list, udp_msg_list.Length - 1)

            If Not udp_msg_list.Contains(Nothing) Then
                ClearLabels()
                Dim v As Integer = 0
                Dim d As Integer = 0
                For Each item In udp_msg_list
                    Select Case item
                        Case "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                                "11", "12", "13", "14", "15", "16"
                            d = CInt(item) - 1

                            Dim newLabelText As String = udp_msg_list.GetValue(v + 2) & vbCrLf &
                                                         "S/N: " &
                                                         udp_msg_list.GetValue(v + 6).ToString.Substring(1, udp_msg_list.GetValue(v + 6).ToString.Length - 1) & vbCrLf &
                                                         udp_msg_list.GetValue(v + 1)

                            If ButtonConnect.Enabled Or ConnectAdapter.Enabled Then
                                SetText(labels.GetValue(d), newLabelText)
                                UDPMessageInOrder.SetValue(udp_msg_list.GetValue(v + 3).ToString.Substring(1), d)
                                Dim MF, MTO As String
                                Dim picStatus As Boolean = False
                                Dim alarmstatus As Boolean

                                If udp_msg_list.GetValue(v + 4) = "Alarm" Then
                                    alarmstatus = True
                                Else
                                    alarmstatus = False
                                End If

                                If udp_msg_list.GetValue(v + 2) = "Master" Then
                                    MF = "1"
                                    MTO = udp_msg_list.GetValue(v + 5)
                                Else
                                    MF = "0"
                                    MTO = "0"
                                End If
                                IPAddresses.SetValue(System.Net.IPAddress.Parse(udp_msg_list.GetValue(v + 1)), d)
                                If testip.Contains(Nothing) Then
                                    testip.SetValue(System.Net.IPAddress.Parse(udp_msg_list.GetValue(v + 1)), d)
                                End If
                                alarms(d).Tag = udp_msg_list.GetValue(v + 1).ToString
                                raise(d).Tag = udp_msg_list.GetValue(v + 1).ToString
                                lower(d).Tag = udp_msg_list.GetValue(v + 1).ToString
                                masters.SetValue(MF, d)
                                VisibilityControls(alarms(d), alarmstatus)
                            End If
                    End Select
                    v += 1
                Next
                SetTextMsgCenter(ConnectionStatusLabel, " Connected ")
            Else
                SetTextMsgCenter(ConnectionStatusLabel, " Received no data ")
            End If
        Catch ex As Exception
            SetTextMsgCenter(ConnectionStatusLabel, " Errors ")
        End Try
        Array.Clear(udp_msg_list, 0, udp_msg_list.Length)
        If DiscoverNetworkDevicesThread.IsAlive Then
            If Not DiscoverNetworkDevicesThread.Join(100) Then
                DiscoverNetworkDevicesThread.Abort()
                udpRunning = False
            End If
        End If
        If Not UdpScanRunning Then
            UdpScanRate = UdpRefreshRate.Value ' * 10000
            TimerStart()
            aTimer.Start()
        End If
    End Sub

    Private Sub DiscoverDevices()
        Dim m_broadcast As New CMFBroadcast()
        m_broadcast.Send(NetworkAdaptersConnected.GetValue(NetworkNumber), 55000)
    End Sub
#End Region

#Region " TCP/IP Comm Stuff "

    Dim modbus As tcpmodbus.modbus = New tcpmodbus.modbus
    Dim raise(), lower(), alarms() As PictureBox
    Dim testip() As IPAddress = New IPAddress(15) {}

    Private Sub TapPositions()
        Dim tapPositionlabels() As Label = New Label() {lt1, lt2, lt3, lt4, lt5, lt6, lt7, lt8,
                                                        lt9, lt10, lt11, lt12, lt13, lt14, lt15, lt16}
        Dim o() As Label = New Label() {out1, out2, out3, out4, out5, out6, out7, out8,
                                         out9, out10, out11, out12, out13, out14, out15, out16}
        Dim i As Integer = 0
        Dim tapP() As String = New String(testip.Length - 1) {}
        Dim alm() As String = New String(testip.Length - 1) {}
        Dim out As String = ""

        modbus.IPAddresses = testip
        modbus.port = mdPort

        Dim tapPosition() As UShort = New UShort(testip.Length - 1) {}
        tapPosition = modbus.CommunicateMultipleUnit(1706, _modbusfunc.read, 1)
        Dim outputstatus() As UShort = New UShort(testip.Length - 1) {}
        outputstatus = modbus.CommunicateMultipleUnit(1721, _modbusfunc.read, 1)
        'Dim alarms() As UShort = New UShort(testip.Length - 1) {}
        'alarms = modbus.CommunicateMultipleUnit(1721, _modbusfunc.read, 1)

        For Each tp In tapPosition
            Select Case (tp And &HFF)
                Case Is = 0
                    tapP(i) = "TapPos 0"
                Case Is < 125
                    tapP(i) = "TapPos " & tp.ToString & "R"
                Case Is > 125
                    tp = CUShort((&HFF - (tp And &HFF)) + 1)
                    tapP(i) = "TapPos " & tp.ToString & "L"
                Case Else
                    tapP(i) = "?"
            End Select
            i += 1
        Next
        i = 0

        For Each status In outputstatus
            Select Case status '(outputstatus And &HFF)
                Case 0
                    'out = (i + 1).ToString
                    out = CStr(i + 1)
                Case 1, 5, 9, 13
                    out = "R"
                Case 2, 6, 10, 14
                    out = "L"
                Case 3, 5, 6, 12, 13, 14
                    out = "S"
                Case 8 To 15
                    out = "A"
                    alm.SetValue("A-Active", i)
                Case Else
                    out = "--"      'Illegal
            End Select
            SetText(o.GetValue(i), out)
            SetText(tapPositionlabels.GetValue(i), tapP.GetValue(i) & vbCrLf & alm.GetValue(i))
            i += 1
        Next
        
        Dim alarm_active As Boolean = Array.Find(outputstatus, AddressOf AlarmActive)

        If alarm_active Then            '128 is 2^8 due to alarm uses bit 8
            VisibilityControls(AlarmReset, True)
        Else
            VisibilityControls(AlarmReset, False)
        End If
    End Sub

    Private Function AlarmActive(ByVal status As UShort) As Boolean
        If status < 8 Then
            Return False
        Else
            Return True
        End If
    End Function

    Event GetManualEvent(ByVal sender As Object, ByVal e As System.EventArgs)

    Private Async Sub ManualEvent(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.GetManualEvent
        'Dim CounterLocations() As UShort = New UShort() {&H4, &H2, &H1, &H400, &H800, &H1000, &H2000, &H4000,
        '                                                 &H4, &H2, &H1, &H400, &H800, &H1000, &H2000, &H4000}
        Dim ip2talk As IPAddress = System.Net.IPAddress.Parse(sender.tag)

        If sender.name.substring(0, 5) = "alarm" Then
            Await TaskEx.Run(Function() modbus.CommunicateSingleUnit(ip2talk, mdPort, 5053, _modbusfunc.write, 1))
            'Await TaskEx.Run(Sub() NetworkDevices_Click(sender, e))
        Else
            'Await TaskEx.Run(Function() modbus.CommunicateSingleUnit(ip2talk, mdPort, 7702, _modbusfunc.write, 1))
            Select Case sender.name.substring(0, 5)
                Case "raise"
                    Await TaskEx.Run(Function() modbus.CommunicateSingleUnit(ip2talk, mdPort, 7700, _modbusfunc.write, 1))
                Case "lower"
                    Await TaskEx.Run(Function() modbus.CommunicateSingleUnit(ip2talk, mdPort, 7701, _modbusfunc.write, 1))
                    'Case "count"
                    '    'chkManualCounterInputs_CheckedChanged(sender, e)
                    '    If CInt(sender.tag) <= 7 Then
                    '        ManualControlMessages(uPnumber.uP3, CounterLocations.GetValue(CInt(sender.tag)), Manual.Block)
                    '    Else
                    '        ManualControlMessages(uPnumber.uP4, CounterLocations.GetValue(CInt(sender.tag)), Manual.Block)
                    '    End If

            End Select
            'delayCommBlock(ip2talk)
        End If
    End Sub

    Private Sub Counter_Click(sender As System.Object, e As System.EventArgs) Handles counter9.Click, counter8.Click,
                                              counter7.Click, counter6.Click, counter5.Click, counter4.Click, counter3.Click,
                                              counter2.Click, counter16.Click, counter15.Click, counter14.Click, counter13.Click,
                                              counter12.Click, counter11.Click, counter10.Click, counter1.Click
        RaiseEvent GetManualEvent(sender, e)

    End Sub

    Private Sub Raise_Click(sender As System.Object, e As System.EventArgs) Handles raise16.Click, raise1.Click,
                                             raise11.Click, raise10.Click, raise9.Click, raise8.Click, raise7.Click,
                                             raise6.Click, raise5.Click, raise4.Click, raise3.Click, raise2.Click,
                                             raise15.Click, raise14.Click, raise13.Click, raise12.Click
        RaiseEvent GetManualEvent(sender, e)
        'modbus.Communicate(7702, _modbusfunc.write, 1)
        'modbus.Communicate(7700, _modbusfunc.write, 1)
        'delayCommBlock(sender)
    End Sub

    Private Sub Lower_Click(sender As System.Object, e As System.EventArgs) Handles lower16.Click, lower9.Click, lower8.Click,
                                                lower7.Click, lower6.Click, lower5.Click, lower4.Click, lower3.Click, lower2.Click,
                                                lower15.Click, lower14.Click, lower13.Click, lower12.Click, lower11.Click,
                                                lower10.Click, lower1.Click
        RaiseEvent GetManualEvent(sender, e)
        'modbus.Communicate(7702, _modbusfunc.write, 1)
        'modbus.Communicate(7701, _modbusfunc.write, 1)
        'delayCommBlock(sender)
    End Sub

    Private Sub Alarm_Reset(sender As System.Object, e As System.EventArgs) Handles alarm9.Click, alarm8.Click, alarm7.Click,
                                                alarm6.Click, alarm5.Click, alarm4.Click, alarm3.Click, alarm2.Click,
                                                alarm16.Click, alarm15.Click, alarm14.Click, alarm13.Click, alarm12.Click,
                                                alarm11.Click, alarm10.Click, alarm1.Click
        RaiseEvent GetManualEvent(sender, e)
        'modbus.Communicate(5053, _modbusfunc.write, 1)
        'NetworkDevices_Click(sender, e)
    End Sub

    'Private Async Sub delayCommBlock(ip2talk As IPAddress)
    '    Dim aStopWatch As New Stopwatch
    '    aStopWatch.Start()
    '    While aStopWatch.IsRunning
    '        If aStopWatch.ElapsedMilliseconds > 1000 Then
    '            aStopWatch.Stop()
    '            Await TaskEx.Run(Function() modbus.CommunicateSingleUnit(ip2talk, mdPort, 7702, _modbusfunc.write, 0))
    '        End If
    '    End While
    'End Sub
#End Region

#Region " Manual Control "
    Private Async Sub CommBlock_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CommBlock.CheckedChanged
        Dim Pict() As PictureBox = New PictureBox() {raise1, raise2, raise3, raise4, raise5, raise6, raise7, raise8,
                                raise9, raise10, raise11, raise12, raise13, raise14, raise15, raise16, lower1, lower2,
                                lower3, lower4, lower5, lower6, lower7, lower8, lower9, lower10, lower11, lower12,
                                lower13, lower14, lower15, lower16}

        For Each pic In Pict
            VisibilityControls(pic, CommBlock.Checked)
        Next
        modbus.IPAddresses = testip
        modbus.port = mdPort
        If CommBlock.Checked Then
            Await TaskEx.Run(Function() modbus.CommunicateMultipleUnit(7702, _modbusfunc.write, 1))
        Else
            Await TaskEx.Run(Function() modbus.CommunicateMultipleUnit(7702, _modbusfunc.write, 0))
        End If
    End Sub

    Private Async Sub ResetAllAlarms(sender As System.Object, e As System.EventArgs) Handles AlarmReset.CheckedChanged
        If AlarmReset.Checked Then
            Await TaskEx.Run(Function() modbus.CommunicateMultipleUnit(5053, _modbusfunc.write, 1))
            AlarmReset.Checked = False
        End If
    End Sub

    'Private Sub EnableManualCounterInputs(ByVal i As Integer, ByVal visibility As Boolean)
    '    Dim Pict() As PictureBox = New PictureBox() {counter1, counter2, counter3, counter4, counter5, counter6, counter7,
    '                                                 counter8, counter9, counter10, counter11, counter12, counter13, counter14,
    '                                                 counter15, counter16}
    '    If AlarmReset.Checked Then
    '        VisibilityControls(Pict(i), visibility)
    '    End If
    'End Sub

    'Enum Manual As Byte
    '    Block = &H1
    '    Unblock = &H0
    'End Enum

    'Private Sub ManualControlMessages(Optional ByVal unitID As Byte = &H5, Optional ByVal value As UShort = 0,
    '                                  Optional ByVal BlockStatus As Byte = Manual.Unblock)
    '    Dim _message() As Byte = New Byte(5) {}
    '    'Dim unitID As Byte = &H3
    '    PortD = value / 256
    '    PortA = value Mod 256

    '    Dim sp As SerialPort = New SerialPort("COM4", port.baudrate, Parity.None, port.databits, port.stopbits)

    '    _message = {Commands.Write, unitID, BlockStatus, PortD, PortA, LRCCalculator(Commands.Write, unitID, BlockStatus, PortD, PortA)}
    '    Try
    '        If Not sp.IsOpen Then
    '            sp.Open()
    '        End If
    '        Dim _data As Integer
    '        For _data = 0 To _message.Length - 1
    '            sp.Write(_message, _data, 1)
    '        Next
    '        ' sp.Close()
    '    Catch ex As Exception

    '    End Try

    'End Sub
#End Region

#Region " Menu Items "
    Private Sub ExitToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        About.ShowDialog()
    End Sub

    Private Sub ContentsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ContentsToolStripMenuItem.Click
        Dim helpfile As ExtIOHelp.Form1 = New ExtIOHelp.Form1
        helpfile.ShowDialog()
    End Sub
#End Region

End Class
