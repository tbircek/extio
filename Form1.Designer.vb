﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim PictureBox16 As System.Windows.Forms.PictureBox
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim PictureBox15 As System.Windows.Forms.PictureBox
        Dim PictureBox14 As System.Windows.Forms.PictureBox
        Dim PictureBox13 As System.Windows.Forms.PictureBox
        Dim PictureBox12 As System.Windows.Forms.PictureBox
        Dim PictureBox11 As System.Windows.Forms.PictureBox
        Dim PictureBox10 As System.Windows.Forms.PictureBox
        Dim PictureBox9 As System.Windows.Forms.PictureBox
        Dim PictureBox8 As System.Windows.Forms.PictureBox
        Dim PictureBox7 As System.Windows.Forms.PictureBox
        Dim PictureBox5 As System.Windows.Forms.PictureBox
        Dim PictureBox4 As System.Windows.Forms.PictureBox
        Dim PictureBox3 As System.Windows.Forms.PictureBox
        Dim PictureBox2 As System.Windows.Forms.PictureBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SerialPanel = New System.Windows.Forms.Panel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ButtonConnect = New System.Windows.Forms.Button()
        Me.NetworkPanel = New System.Windows.Forms.Panel()
        Me.ConnectAdapter = New System.Windows.Forms.Button()
        Me.NetworkAdaptersAvailable = New System.Windows.Forms.ComboBox()
        Me.SettingsBlock = New System.Windows.Forms.Panel()
        Me.CommBlock = New System.Windows.Forms.RadioButton()
        Me.AutoControl = New System.Windows.Forms.RadioButton()
        Me.AlarmReset = New System.Windows.Forms.CheckBox()
        Me.UdpRefreshRate = New System.Windows.Forms.NumericUpDown()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.LedBlock = New System.Windows.Forms.Panel()
        Me.grpAllBreakers = New System.Windows.Forms.GroupBox()
        Me.button_all_on = New System.Windows.Forms.RadioButton()
        Me.button_all_off = New System.Windows.Forms.RadioButton()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Led31 = New System.Windows.Forms.PictureBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Led30 = New System.Windows.Forms.PictureBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Led29 = New System.Windows.Forms.PictureBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Led28 = New System.Windows.Forms.PictureBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Led27 = New System.Windows.Forms.PictureBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Led26 = New System.Windows.Forms.PictureBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Led25 = New System.Windows.Forms.PictureBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Led24 = New System.Windows.Forms.PictureBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Led23 = New System.Windows.Forms.PictureBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Led22 = New System.Windows.Forms.PictureBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Led21 = New System.Windows.Forms.PictureBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Led20 = New System.Windows.Forms.PictureBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Led19 = New System.Windows.Forms.PictureBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Led18 = New System.Windows.Forms.PictureBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Led17 = New System.Windows.Forms.PictureBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Led16 = New System.Windows.Forms.PictureBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Led15 = New System.Windows.Forms.PictureBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Led14 = New System.Windows.Forms.PictureBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Led13 = New System.Windows.Forms.PictureBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Led12 = New System.Windows.Forms.PictureBox()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Led11 = New System.Windows.Forms.PictureBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Led10 = New System.Windows.Forms.PictureBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Led9 = New System.Windows.Forms.PictureBox()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Led8 = New System.Windows.Forms.PictureBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Led7 = New System.Windows.Forms.PictureBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Led6 = New System.Windows.Forms.PictureBox()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Led5 = New System.Windows.Forms.PictureBox()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Led4 = New System.Windows.Forms.PictureBox()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Led3 = New System.Windows.Forms.PictureBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Led2 = New System.Windows.Forms.PictureBox()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Led1 = New System.Windows.Forms.PictureBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.NetworkDevices = New System.Windows.Forms.Button()
        Me.DelayAll = New System.Windows.Forms.CheckBox()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MainBlock = New System.Windows.Forms.Panel()
        Me.counter16 = New System.Windows.Forms.PictureBox()
        Me.counter15 = New System.Windows.Forms.PictureBox()
        Me.counter14 = New System.Windows.Forms.PictureBox()
        Me.counter13 = New System.Windows.Forms.PictureBox()
        Me.counter12 = New System.Windows.Forms.PictureBox()
        Me.counter11 = New System.Windows.Forms.PictureBox()
        Me.counter10 = New System.Windows.Forms.PictureBox()
        Me.counter9 = New System.Windows.Forms.PictureBox()
        Me.counter8 = New System.Windows.Forms.PictureBox()
        Me.counter7 = New System.Windows.Forms.PictureBox()
        Me.counter6 = New System.Windows.Forms.PictureBox()
        Me.counter5 = New System.Windows.Forms.PictureBox()
        Me.counter4 = New System.Windows.Forms.PictureBox()
        Me.counter3 = New System.Windows.Forms.PictureBox()
        Me.counter2 = New System.Windows.Forms.PictureBox()
        Me.counter1 = New System.Windows.Forms.PictureBox()
        Me.raise9 = New System.Windows.Forms.PictureBox()
        Me.raise10 = New System.Windows.Forms.PictureBox()
        Me.lower9 = New System.Windows.Forms.PictureBox()
        Me.alarm9 = New System.Windows.Forms.PictureBox()
        Me.raise11 = New System.Windows.Forms.PictureBox()
        Me.lower10 = New System.Windows.Forms.PictureBox()
        Me.alarm10 = New System.Windows.Forms.PictureBox()
        Me.raise12 = New System.Windows.Forms.PictureBox()
        Me.lower11 = New System.Windows.Forms.PictureBox()
        Me.alarm11 = New System.Windows.Forms.PictureBox()
        Me.raise13 = New System.Windows.Forms.PictureBox()
        Me.lower12 = New System.Windows.Forms.PictureBox()
        Me.alarm12 = New System.Windows.Forms.PictureBox()
        Me.raise14 = New System.Windows.Forms.PictureBox()
        Me.lower13 = New System.Windows.Forms.PictureBox()
        Me.raise15 = New System.Windows.Forms.PictureBox()
        Me.alarm13 = New System.Windows.Forms.PictureBox()
        Me.lower14 = New System.Windows.Forms.PictureBox()
        Me.alarm14 = New System.Windows.Forms.PictureBox()
        Me.raise16 = New System.Windows.Forms.PictureBox()
        Me.lower15 = New System.Windows.Forms.PictureBox()
        Me.alarm15 = New System.Windows.Forms.PictureBox()
        Me.raise8 = New System.Windows.Forms.PictureBox()
        Me.lower16 = New System.Windows.Forms.PictureBox()
        Me.alarm16 = New System.Windows.Forms.PictureBox()
        Me.raise7 = New System.Windows.Forms.PictureBox()
        Me.lower8 = New System.Windows.Forms.PictureBox()
        Me.alarm8 = New System.Windows.Forms.PictureBox()
        Me.raise6 = New System.Windows.Forms.PictureBox()
        Me.lower7 = New System.Windows.Forms.PictureBox()
        Me.alarm7 = New System.Windows.Forms.PictureBox()
        Me.raise5 = New System.Windows.Forms.PictureBox()
        Me.lower6 = New System.Windows.Forms.PictureBox()
        Me.alarm6 = New System.Windows.Forms.PictureBox()
        Me.raise4 = New System.Windows.Forms.PictureBox()
        Me.lower5 = New System.Windows.Forms.PictureBox()
        Me.alarm5 = New System.Windows.Forms.PictureBox()
        Me.raise3 = New System.Windows.Forms.PictureBox()
        Me.lower4 = New System.Windows.Forms.PictureBox()
        Me.alarm4 = New System.Windows.Forms.PictureBox()
        Me.raise2 = New System.Windows.Forms.PictureBox()
        Me.lower3 = New System.Windows.Forms.PictureBox()
        Me.alarm3 = New System.Windows.Forms.PictureBox()
        Me.raise1 = New System.Windows.Forms.PictureBox()
        Me.lower2 = New System.Windows.Forms.PictureBox()
        Me.alarm2 = New System.Windows.Forms.PictureBox()
        Me.lt16 = New System.Windows.Forms.Label()
        Me.lower1 = New System.Windows.Forms.PictureBox()
        Me.lt15 = New System.Windows.Forms.Label()
        Me.alarm1 = New System.Windows.Forms.PictureBox()
        Me.lt14 = New System.Windows.Forms.Label()
        Me.lt13 = New System.Windows.Forms.Label()
        Me.lt12 = New System.Windows.Forms.Label()
        Me.lt11 = New System.Windows.Forms.Label()
        Me.lt10 = New System.Windows.Forms.Label()
        Me.lt9 = New System.Windows.Forms.Label()
        Me.lt8 = New System.Windows.Forms.Label()
        Me.lt7 = New System.Windows.Forms.Label()
        Me.lt6 = New System.Windows.Forms.Label()
        Me.lt5 = New System.Windows.Forms.Label()
        Me.lt4 = New System.Windows.Forms.Label()
        Me.lt3 = New System.Windows.Forms.Label()
        Me.lt2 = New System.Windows.Forms.Label()
        Me.lt1 = New System.Windows.Forms.Label()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.NumericUpDown15 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown14 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown13 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown12 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown11 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown10 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown9 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown8 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown7 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown6 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown0 = New System.Windows.Forms.NumericUpDown()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Unit8TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit15TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit14TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit13TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit12TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit11TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit10TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit9TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit7TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit6TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit5TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit3TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit2TieBreaker = New System.Windows.Forms.PictureBox()
        Me.out16 = New System.Windows.Forms.Label()
        Me.out15 = New System.Windows.Forms.Label()
        Me.out14 = New System.Windows.Forms.Label()
        Me.out13 = New System.Windows.Forms.Label()
        Me.out12 = New System.Windows.Forms.Label()
        Me.out11 = New System.Windows.Forms.Label()
        Me.out10 = New System.Windows.Forms.Label()
        Me.out9 = New System.Windows.Forms.Label()
        Me.out8 = New System.Windows.Forms.Label()
        Me.out7 = New System.Windows.Forms.Label()
        Me.out6 = New System.Windows.Forms.Label()
        Me.out5 = New System.Windows.Forms.Label()
        Me.out4 = New System.Windows.Forms.Label()
        Me.out3 = New System.Windows.Forms.Label()
        Me.out2 = New System.Windows.Forms.Label()
        Me.out1 = New System.Windows.Forms.Label()
        Me.Unit1TieBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit16LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit15LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit14LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit13LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit12LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit11LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit10LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit9LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit8LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit7LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit6LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit5LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit4LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit3LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit2LineBreaker = New System.Windows.Forms.PictureBox()
        Me.Unit1LineBreaker = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Unit4TieBreaker = New System.Windows.Forms.PictureBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ConnectionStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.UdpProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        PictureBox16 = New System.Windows.Forms.PictureBox()
        PictureBox15 = New System.Windows.Forms.PictureBox()
        PictureBox14 = New System.Windows.Forms.PictureBox()
        PictureBox13 = New System.Windows.Forms.PictureBox()
        PictureBox12 = New System.Windows.Forms.PictureBox()
        PictureBox11 = New System.Windows.Forms.PictureBox()
        PictureBox10 = New System.Windows.Forms.PictureBox()
        PictureBox9 = New System.Windows.Forms.PictureBox()
        PictureBox8 = New System.Windows.Forms.PictureBox()
        PictureBox7 = New System.Windows.Forms.PictureBox()
        PictureBox5 = New System.Windows.Forms.PictureBox()
        PictureBox4 = New System.Windows.Forms.PictureBox()
        PictureBox3 = New System.Windows.Forms.PictureBox()
        PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SerialPanel.SuspendLayout()
        Me.NetworkPanel.SuspendLayout()
        Me.SettingsBlock.SuspendLayout()
        CType(Me.UdpRefreshRate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LedBlock.SuspendLayout()
        Me.grpAllBreakers.SuspendLayout()
        CType(Me.Led31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainBlock.SuspendLayout()
        CType(Me.counter16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.counter1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.raise1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lower1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.alarm1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit8TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit15TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit14TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit13TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit12TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit11TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit10TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit9TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit7TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit6TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit5TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit3TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit2TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit1TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit16LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit15LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit14LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit13LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit12LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit11LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit10LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit9LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit8LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit7LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit6LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit5LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit4LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit3LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit2LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit1LineBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Unit4TieBreaker, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox16
        '
        PictureBox16.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox16.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox16.InitialImage = CType(resources.GetObject("PictureBox16.InitialImage"), System.Drawing.Image)
        PictureBox16.Location = New System.Drawing.Point(334, 223)
        PictureBox16.Name = "PictureBox16"
        PictureBox16.Size = New System.Drawing.Size(35, 57)
        PictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox16.TabIndex = 180
        PictureBox16.TabStop = False
        '
        'PictureBox15
        '
        PictureBox15.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox15.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox15.InitialImage = CType(resources.GetObject("PictureBox15.InitialImage"), System.Drawing.Image)
        PictureBox15.Location = New System.Drawing.Point(226, 223)
        PictureBox15.Name = "PictureBox15"
        PictureBox15.Size = New System.Drawing.Size(35, 57)
        PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox15.TabIndex = 179
        PictureBox15.TabStop = False
        '
        'PictureBox14
        '
        PictureBox14.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox14.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox14.InitialImage = CType(resources.GetObject("PictureBox14.InitialImage"), System.Drawing.Image)
        PictureBox14.Location = New System.Drawing.Point(118, 223)
        PictureBox14.Name = "PictureBox14"
        PictureBox14.Size = New System.Drawing.Size(35, 57)
        PictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox14.TabIndex = 178
        PictureBox14.TabStop = False
        '
        'PictureBox13
        '
        PictureBox13.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox13.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox13.InitialImage = CType(resources.GetObject("PictureBox13.InitialImage"), System.Drawing.Image)
        PictureBox13.Location = New System.Drawing.Point(442, 223)
        PictureBox13.Name = "PictureBox13"
        PictureBox13.Size = New System.Drawing.Size(35, 57)
        PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox13.TabIndex = 177
        PictureBox13.TabStop = False
        '
        'PictureBox12
        '
        PictureBox12.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox12.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox12.InitialImage = CType(resources.GetObject("PictureBox12.InitialImage"), System.Drawing.Image)
        PictureBox12.Location = New System.Drawing.Point(766, 223)
        PictureBox12.Name = "PictureBox12"
        PictureBox12.Size = New System.Drawing.Size(35, 57)
        PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox12.TabIndex = 176
        PictureBox12.TabStop = False
        '
        'PictureBox11
        '
        PictureBox11.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox11.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox11.InitialImage = CType(resources.GetObject("PictureBox11.InitialImage"), System.Drawing.Image)
        PictureBox11.Location = New System.Drawing.Point(658, 223)
        PictureBox11.Name = "PictureBox11"
        PictureBox11.Size = New System.Drawing.Size(35, 57)
        PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox11.TabIndex = 175
        PictureBox11.TabStop = False
        '
        'PictureBox10
        '
        PictureBox10.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox10.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox10.InitialImage = CType(resources.GetObject("PictureBox10.InitialImage"), System.Drawing.Image)
        PictureBox10.Location = New System.Drawing.Point(550, 223)
        PictureBox10.Name = "PictureBox10"
        PictureBox10.Size = New System.Drawing.Size(35, 57)
        PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox10.TabIndex = 174
        PictureBox10.TabStop = False
        '
        'PictureBox9
        '
        PictureBox9.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox9.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox9.InitialImage = CType(resources.GetObject("PictureBox9.InitialImage"), System.Drawing.Image)
        PictureBox9.Location = New System.Drawing.Point(10, 223)
        PictureBox9.Name = "PictureBox9"
        PictureBox9.Size = New System.Drawing.Size(35, 57)
        PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox9.TabIndex = 173
        PictureBox9.TabStop = False
        '
        'PictureBox8
        '
        PictureBox8.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox8.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox8.InitialImage = CType(resources.GetObject("PictureBox8.InitialImage"), System.Drawing.Image)
        PictureBox8.Location = New System.Drawing.Point(334, 44)
        PictureBox8.Name = "PictureBox8"
        PictureBox8.Size = New System.Drawing.Size(35, 57)
        PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox8.TabIndex = 172
        PictureBox8.TabStop = False
        '
        'PictureBox7
        '
        PictureBox7.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox7.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox7.InitialImage = CType(resources.GetObject("PictureBox7.InitialImage"), System.Drawing.Image)
        PictureBox7.Location = New System.Drawing.Point(226, 44)
        PictureBox7.Name = "PictureBox7"
        PictureBox7.Size = New System.Drawing.Size(35, 57)
        PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox7.TabIndex = 171
        PictureBox7.TabStop = False
        '
        'PictureBox5
        '
        PictureBox5.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox5.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox5.InitialImage = CType(resources.GetObject("PictureBox5.InitialImage"), System.Drawing.Image)
        PictureBox5.Location = New System.Drawing.Point(442, 44)
        PictureBox5.Name = "PictureBox5"
        PictureBox5.Size = New System.Drawing.Size(35, 57)
        PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox5.TabIndex = 169
        PictureBox5.TabStop = False
        '
        'PictureBox4
        '
        PictureBox4.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox4.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox4.InitialImage = CType(resources.GetObject("PictureBox4.InitialImage"), System.Drawing.Image)
        PictureBox4.Location = New System.Drawing.Point(766, 44)
        PictureBox4.Name = "PictureBox4"
        PictureBox4.Size = New System.Drawing.Size(35, 57)
        PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox4.TabIndex = 168
        PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        PictureBox3.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox3.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox3.InitialImage = CType(resources.GetObject("PictureBox3.InitialImage"), System.Drawing.Image)
        PictureBox3.Location = New System.Drawing.Point(658, 44)
        PictureBox3.Name = "PictureBox3"
        PictureBox3.Size = New System.Drawing.Size(35, 57)
        PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox3.TabIndex = 167
        PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        PictureBox2.Cursor = System.Windows.Forms.Cursors.Hand
        PictureBox2.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        PictureBox2.InitialImage = CType(resources.GetObject("PictureBox2.InitialImage"), System.Drawing.Image)
        PictureBox2.Location = New System.Drawing.Point(550, 44)
        PictureBox2.Name = "PictureBox2"
        PictureBox2.Size = New System.Drawing.Size(35, 57)
        PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox2.TabIndex = 166
        PictureBox2.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SerialPanel)
        Me.GroupBox1.Controls.Add(Me.NetworkPanel)
        Me.GroupBox1.Controls.Add(Me.SettingsBlock)
        Me.GroupBox1.Controls.Add(Me.LedBlock)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(897, 123)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Please select a comport"
        '
        'SerialPanel
        '
        Me.SerialPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SerialPanel.Controls.Add(Me.ComboBox1)
        Me.SerialPanel.Controls.Add(Me.ButtonConnect)
        Me.SerialPanel.Location = New System.Drawing.Point(7, 19)
        Me.SerialPanel.Name = "SerialPanel"
        Me.SerialPanel.Size = New System.Drawing.Size(245, 30)
        Me.SerialPanel.TabIndex = 130
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(3, 3)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(123, 21)
        Me.ComboBox1.Sorted = True
        Me.ComboBox1.TabIndex = 0
        '
        'ButtonConnect
        '
        Me.ButtonConnect.Enabled = False
        Me.ButtonConnect.Location = New System.Drawing.Point(132, 3)
        Me.ButtonConnect.Name = "ButtonConnect"
        Me.ButtonConnect.Size = New System.Drawing.Size(75, 23)
        Me.ButtonConnect.TabIndex = 2
        Me.ButtonConnect.Text = "Connect"
        Me.ButtonConnect.UseVisualStyleBackColor = True
        '
        'NetworkPanel
        '
        Me.NetworkPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.NetworkPanel.Controls.Add(Me.ConnectAdapter)
        Me.NetworkPanel.Controls.Add(Me.NetworkAdaptersAvailable)
        Me.NetworkPanel.Location = New System.Drawing.Point(7, 55)
        Me.NetworkPanel.Name = "NetworkPanel"
        Me.NetworkPanel.Size = New System.Drawing.Size(245, 30)
        Me.NetworkPanel.TabIndex = 129
        '
        'ConnectAdapter
        '
        Me.ConnectAdapter.Enabled = False
        Me.ConnectAdapter.Location = New System.Drawing.Point(154, 2)
        Me.ConnectAdapter.Name = "ConnectAdapter"
        Me.ConnectAdapter.Size = New System.Drawing.Size(75, 23)
        Me.ConnectAdapter.TabIndex = 1
        Me.ConnectAdapter.Text = "Connect"
        Me.ConnectAdapter.UseVisualStyleBackColor = True
        '
        'NetworkAdaptersAvailable
        '
        Me.NetworkAdaptersAvailable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.NetworkAdaptersAvailable.DropDownWidth = 360
        Me.NetworkAdaptersAvailable.FormattingEnabled = True
        Me.NetworkAdaptersAvailable.Location = New System.Drawing.Point(3, 3)
        Me.NetworkAdaptersAvailable.Name = "NetworkAdaptersAvailable"
        Me.NetworkAdaptersAvailable.Size = New System.Drawing.Size(141, 21)
        Me.NetworkAdaptersAvailable.TabIndex = 0
        '
        'SettingsBlock
        '
        Me.SettingsBlock.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SettingsBlock.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SettingsBlock.Controls.Add(Me.CommBlock)
        Me.SettingsBlock.Controls.Add(Me.AutoControl)
        Me.SettingsBlock.Controls.Add(Me.AlarmReset)
        Me.SettingsBlock.Controls.Add(Me.UdpRefreshRate)
        Me.SettingsBlock.Controls.Add(Me.Label117)
        Me.SettingsBlock.Controls.Add(Me.Label114)
        Me.SettingsBlock.Location = New System.Drawing.Point(6, 88)
        Me.SettingsBlock.Name = "SettingsBlock"
        Me.SettingsBlock.Size = New System.Drawing.Size(885, 28)
        Me.SettingsBlock.TabIndex = 128
        '
        'CommBlock
        '
        Me.CommBlock.AutoSize = True
        Me.CommBlock.Location = New System.Drawing.Point(327, 4)
        Me.CommBlock.Name = "CommBlock"
        Me.CommBlock.Size = New System.Drawing.Size(84, 17)
        Me.CommBlock.TabIndex = 129
        Me.CommBlock.Text = "Comm Block"
        Me.CommBlock.UseVisualStyleBackColor = True
        '
        'AutoControl
        '
        Me.AutoControl.AutoSize = True
        Me.AutoControl.Checked = True
        Me.AutoControl.Location = New System.Drawing.Point(228, 4)
        Me.AutoControl.Name = "AutoControl"
        Me.AutoControl.Size = New System.Drawing.Size(83, 17)
        Me.AutoControl.TabIndex = 128
        Me.AutoControl.TabStop = True
        Me.AutoControl.Text = "Auto Control"
        Me.AutoControl.UseVisualStyleBackColor = True
        '
        'AlarmReset
        '
        Me.AlarmReset.AutoSize = True
        Me.AlarmReset.Location = New System.Drawing.Point(427, 4)
        Me.AlarmReset.Name = "AlarmReset"
        Me.AlarmReset.Size = New System.Drawing.Size(132, 17)
        Me.AlarmReset.TabIndex = 127
        Me.AlarmReset.Text = "Reset all active alarms"
        Me.AlarmReset.UseVisualStyleBackColor = True
        Me.AlarmReset.Visible = False
        '
        'UdpRefreshRate
        '
        Me.UdpRefreshRate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.UdpRefreshRate.DecimalPlaces = 1
        Me.UdpRefreshRate.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.UdpRefreshRate.Location = New System.Drawing.Point(128, 3)
        Me.UdpRefreshRate.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.UdpRefreshRate.Minimum = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.UdpRefreshRate.Name = "UdpRefreshRate"
        Me.UdpRefreshRate.Size = New System.Drawing.Size(40, 20)
        Me.UdpRefreshRate.TabIndex = 126
        Me.UdpRefreshRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.UdpRefreshRate.ThousandsSeparator = True
        Me.UdpRefreshRate.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label117
        '
        Me.Label117.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label117.AutoSize = True
        Me.Label117.Location = New System.Drawing.Point(4, 7)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(124, 13)
        Me.Label117.TabIndex = 125
        Me.Label117.Text = "Refresh devices in every"
        '
        'Label114
        '
        Me.Label114.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label114.AutoSize = True
        Me.Label114.Location = New System.Drawing.Point(168, 7)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(50, 13)
        Me.Label114.TabIndex = 121
        Me.Label114.Text = "seconds."
        Me.Label114.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LedBlock
        '
        Me.LedBlock.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LedBlock.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.LedBlock.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LedBlock.Controls.Add(Me.grpAllBreakers)
        Me.LedBlock.Controls.Add(Me.Label54)
        Me.LedBlock.Controls.Add(Me.Led31)
        Me.LedBlock.Controls.Add(Me.Label49)
        Me.LedBlock.Controls.Add(Me.Led30)
        Me.LedBlock.Controls.Add(Me.Label50)
        Me.LedBlock.Controls.Add(Me.Led29)
        Me.LedBlock.Controls.Add(Me.Label51)
        Me.LedBlock.Controls.Add(Me.Led28)
        Me.LedBlock.Controls.Add(Me.Label52)
        Me.LedBlock.Controls.Add(Me.Led27)
        Me.LedBlock.Controls.Add(Me.Label53)
        Me.LedBlock.Controls.Add(Me.Led26)
        Me.LedBlock.Controls.Add(Me.Label55)
        Me.LedBlock.Controls.Add(Me.Led25)
        Me.LedBlock.Controls.Add(Me.Label56)
        Me.LedBlock.Controls.Add(Me.Led24)
        Me.LedBlock.Controls.Add(Me.Label57)
        Me.LedBlock.Controls.Add(Me.Led23)
        Me.LedBlock.Controls.Add(Me.Label58)
        Me.LedBlock.Controls.Add(Me.Led22)
        Me.LedBlock.Controls.Add(Me.Label59)
        Me.LedBlock.Controls.Add(Me.Led21)
        Me.LedBlock.Controls.Add(Me.Label60)
        Me.LedBlock.Controls.Add(Me.Led20)
        Me.LedBlock.Controls.Add(Me.Label61)
        Me.LedBlock.Controls.Add(Me.Led19)
        Me.LedBlock.Controls.Add(Me.Label62)
        Me.LedBlock.Controls.Add(Me.Led18)
        Me.LedBlock.Controls.Add(Me.Label63)
        Me.LedBlock.Controls.Add(Me.Led17)
        Me.LedBlock.Controls.Add(Me.Label64)
        Me.LedBlock.Controls.Add(Me.Led16)
        Me.LedBlock.Controls.Add(Me.Label65)
        Me.LedBlock.Controls.Add(Me.Led15)
        Me.LedBlock.Controls.Add(Me.Label66)
        Me.LedBlock.Controls.Add(Me.Led14)
        Me.LedBlock.Controls.Add(Me.Label67)
        Me.LedBlock.Controls.Add(Me.Led13)
        Me.LedBlock.Controls.Add(Me.Label81)
        Me.LedBlock.Controls.Add(Me.Led12)
        Me.LedBlock.Controls.Add(Me.Label80)
        Me.LedBlock.Controls.Add(Me.Led11)
        Me.LedBlock.Controls.Add(Me.Label79)
        Me.LedBlock.Controls.Add(Me.Led10)
        Me.LedBlock.Controls.Add(Me.Label78)
        Me.LedBlock.Controls.Add(Me.Led9)
        Me.LedBlock.Controls.Add(Me.Label77)
        Me.LedBlock.Controls.Add(Me.Led8)
        Me.LedBlock.Controls.Add(Me.Label76)
        Me.LedBlock.Controls.Add(Me.Led7)
        Me.LedBlock.Controls.Add(Me.Label75)
        Me.LedBlock.Controls.Add(Me.Led6)
        Me.LedBlock.Controls.Add(Me.Label74)
        Me.LedBlock.Controls.Add(Me.Led5)
        Me.LedBlock.Controls.Add(Me.Label73)
        Me.LedBlock.Controls.Add(Me.Led4)
        Me.LedBlock.Controls.Add(Me.Label72)
        Me.LedBlock.Controls.Add(Me.Led3)
        Me.LedBlock.Controls.Add(Me.Label71)
        Me.LedBlock.Controls.Add(Me.Led2)
        Me.LedBlock.Controls.Add(Me.Label70)
        Me.LedBlock.Controls.Add(Me.Led1)
        Me.LedBlock.Controls.Add(Me.Label69)
        Me.LedBlock.Controls.Add(Me.Label68)
        Me.LedBlock.Location = New System.Drawing.Point(257, 10)
        Me.LedBlock.Name = "LedBlock"
        Me.LedBlock.Size = New System.Drawing.Size(634, 75)
        Me.LedBlock.TabIndex = 3
        Me.LedBlock.Visible = False
        '
        'grpAllBreakers
        '
        Me.grpAllBreakers.Controls.Add(Me.button_all_on)
        Me.grpAllBreakers.Controls.Add(Me.button_all_off)
        Me.grpAllBreakers.Location = New System.Drawing.Point(544, 7)
        Me.grpAllBreakers.Name = "grpAllBreakers"
        Me.grpAllBreakers.Size = New System.Drawing.Size(84, 60)
        Me.grpAllBreakers.TabIndex = 343
        Me.grpAllBreakers.TabStop = False
        Me.grpAllBreakers.Text = "All Breakers"
        '
        'button_all_on
        '
        Me.button_all_on.AutoSize = True
        Me.button_all_on.Enabled = False
        Me.button_all_on.Location = New System.Drawing.Point(6, 19)
        Me.button_all_on.Name = "button_all_on"
        Me.button_all_on.Size = New System.Drawing.Size(51, 17)
        Me.button_all_on.TabIndex = 28
        Me.button_all_on.TabStop = True
        Me.button_all_on.Text = "Close"
        Me.button_all_on.UseVisualStyleBackColor = True
        '
        'button_all_off
        '
        Me.button_all_off.AutoSize = True
        Me.button_all_off.Enabled = False
        Me.button_all_off.Location = New System.Drawing.Point(6, 37)
        Me.button_all_off.Name = "button_all_off"
        Me.button_all_off.Size = New System.Drawing.Size(51, 17)
        Me.button_all_off.TabIndex = 114
        Me.button_all_off.TabStop = True
        Me.button_all_off.Text = "Open"
        Me.button_all_off.UseVisualStyleBackColor = True
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(141, 1)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(13, 13)
        Me.Label54.TabIndex = 9
        Me.Label54.Text = "3"
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led31
        '
        Me.Led31.BackColor = System.Drawing.Color.Blue
        Me.Led31.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led31.Location = New System.Drawing.Point(501, 49)
        Me.Led31.Name = "Led31"
        Me.Led31.Size = New System.Drawing.Size(16, 16)
        Me.Led31.TabIndex = 67
        Me.Led31.TabStop = False
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(5, 18)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(72, 13)
        Me.Label49.TabIndex = 4
        Me.Label49.Text = "Line Breakers"
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led30
        '
        Me.Led30.BackColor = System.Drawing.Color.Blue
        Me.Led30.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led30.Location = New System.Drawing.Point(472, 49)
        Me.Led30.Name = "Led30"
        Me.Led30.Size = New System.Drawing.Size(16, 16)
        Me.Led30.TabIndex = 66
        Me.Led30.TabStop = False
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(5, 52)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(67, 13)
        Me.Label50.TabIndex = 5
        Me.Label50.Text = "Tie Breakers"
        '
        'Led29
        '
        Me.Led29.BackColor = System.Drawing.Color.Blue
        Me.Led29.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led29.Location = New System.Drawing.Point(443, 49)
        Me.Led29.Name = "Led29"
        Me.Led29.Size = New System.Drawing.Size(16, 16)
        Me.Led29.TabIndex = 65
        Me.Led29.TabStop = False
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.Location = New System.Drawing.Point(83, 1)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(13, 13)
        Me.Label51.TabIndex = 6
        Me.Label51.Text = "1"
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led28
        '
        Me.Led28.BackColor = System.Drawing.Color.Blue
        Me.Led28.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led28.Location = New System.Drawing.Point(414, 49)
        Me.Led28.Name = "Led28"
        Me.Led28.Size = New System.Drawing.Size(16, 16)
        Me.Led28.TabIndex = 64
        Me.Led28.TabStop = False
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.Location = New System.Drawing.Point(96, 36)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(13, 13)
        Me.Label52.TabIndex = 7
        Me.Label52.Text = "1"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led27
        '
        Me.Led27.BackColor = System.Drawing.Color.Blue
        Me.Led27.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led27.Location = New System.Drawing.Point(384, 49)
        Me.Led27.Name = "Led27"
        Me.Led27.Size = New System.Drawing.Size(16, 16)
        Me.Led27.TabIndex = 63
        Me.Led27.TabStop = False
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(170, 1)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(13, 13)
        Me.Label53.TabIndex = 8
        Me.Label53.Text = "4"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led26
        '
        Me.Led26.BackColor = System.Drawing.Color.Blue
        Me.Led26.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led26.Location = New System.Drawing.Point(355, 49)
        Me.Led26.Name = "Led26"
        Me.Led26.Size = New System.Drawing.Size(16, 16)
        Me.Led26.TabIndex = 62
        Me.Led26.TabStop = False
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(112, 1)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(13, 13)
        Me.Label55.TabIndex = 10
        Me.Label55.Text = "2"
        Me.Label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led25
        '
        Me.Led25.BackColor = System.Drawing.Color.Blue
        Me.Led25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led25.Location = New System.Drawing.Point(325, 49)
        Me.Led25.Name = "Led25"
        Me.Led25.Size = New System.Drawing.Size(16, 16)
        Me.Led25.TabIndex = 61
        Me.Led25.TabStop = False
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(199, 1)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(13, 13)
        Me.Label56.TabIndex = 11
        Me.Label56.Text = "5"
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led24
        '
        Me.Led24.BackColor = System.Drawing.Color.Blue
        Me.Led24.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led24.Location = New System.Drawing.Point(296, 49)
        Me.Led24.Name = "Led24"
        Me.Led24.Size = New System.Drawing.Size(16, 16)
        Me.Led24.TabIndex = 60
        Me.Led24.TabStop = False
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.Location = New System.Drawing.Point(228, 1)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(13, 13)
        Me.Label57.TabIndex = 12
        Me.Label57.Text = "6"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led23
        '
        Me.Led23.BackColor = System.Drawing.Color.Blue
        Me.Led23.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led23.Location = New System.Drawing.Point(267, 49)
        Me.Led23.Name = "Led23"
        Me.Led23.Size = New System.Drawing.Size(16, 16)
        Me.Led23.TabIndex = 59
        Me.Led23.TabStop = False
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.Location = New System.Drawing.Point(257, 1)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(13, 13)
        Me.Label58.TabIndex = 13
        Me.Label58.Text = "7"
        Me.Label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led22
        '
        Me.Led22.BackColor = System.Drawing.Color.Blue
        Me.Led22.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led22.Location = New System.Drawing.Point(238, 49)
        Me.Led22.Name = "Led22"
        Me.Led22.Size = New System.Drawing.Size(16, 16)
        Me.Led22.TabIndex = 58
        Me.Led22.TabStop = False
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(286, 1)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(13, 13)
        Me.Label59.TabIndex = 14
        Me.Label59.Text = "8"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led21
        '
        Me.Led21.BackColor = System.Drawing.Color.Blue
        Me.Led21.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led21.Location = New System.Drawing.Point(209, 49)
        Me.Led21.Name = "Led21"
        Me.Led21.Size = New System.Drawing.Size(16, 16)
        Me.Led21.TabIndex = 57
        Me.Led21.TabStop = False
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(315, 1)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(13, 13)
        Me.Label60.TabIndex = 15
        Me.Label60.Text = "9"
        Me.Label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led20
        '
        Me.Led20.BackColor = System.Drawing.Color.Blue
        Me.Led20.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led20.Location = New System.Drawing.Point(180, 49)
        Me.Led20.Name = "Led20"
        Me.Led20.Size = New System.Drawing.Size(16, 16)
        Me.Led20.TabIndex = 56
        Me.Led20.TabStop = False
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.Location = New System.Drawing.Point(341, 1)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(19, 13)
        Me.Label61.TabIndex = 16
        Me.Label61.Text = "10"
        Me.Label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led19
        '
        Me.Led19.BackColor = System.Drawing.Color.Blue
        Me.Led19.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led19.Location = New System.Drawing.Point(151, 49)
        Me.Led19.Name = "Led19"
        Me.Led19.Size = New System.Drawing.Size(16, 16)
        Me.Led19.TabIndex = 55
        Me.Led19.TabStop = False
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(371, 1)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(19, 13)
        Me.Label62.TabIndex = 17
        Me.Label62.Text = "11"
        Me.Label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led18
        '
        Me.Led18.BackColor = System.Drawing.Color.Blue
        Me.Led18.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led18.Location = New System.Drawing.Point(122, 49)
        Me.Led18.Name = "Led18"
        Me.Led18.Size = New System.Drawing.Size(16, 16)
        Me.Led18.TabIndex = 54
        Me.Led18.TabStop = False
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.Location = New System.Drawing.Point(401, 1)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(19, 13)
        Me.Label63.TabIndex = 18
        Me.Label63.Text = "12"
        Me.Label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led17
        '
        Me.Led17.BackColor = System.Drawing.Color.Blue
        Me.Led17.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led17.Location = New System.Drawing.Point(93, 49)
        Me.Led17.Name = "Led17"
        Me.Led17.Size = New System.Drawing.Size(16, 16)
        Me.Led17.TabIndex = 53
        Me.Led17.TabStop = False
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.Location = New System.Drawing.Point(430, 1)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(19, 13)
        Me.Label64.TabIndex = 19
        Me.Label64.Text = "13"
        Me.Label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led16
        '
        Me.Led16.BackColor = System.Drawing.Color.Blue
        Me.Led16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led16.Location = New System.Drawing.Point(518, 19)
        Me.Led16.Name = "Led16"
        Me.Led16.Size = New System.Drawing.Size(16, 16)
        Me.Led16.TabIndex = 52
        Me.Led16.TabStop = False
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.Location = New System.Drawing.Point(517, 1)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(19, 13)
        Me.Label65.TabIndex = 20
        Me.Label65.Text = "16"
        Me.Label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led15
        '
        Me.Led15.BackColor = System.Drawing.Color.Blue
        Me.Led15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led15.Location = New System.Drawing.Point(489, 19)
        Me.Led15.Name = "Led15"
        Me.Led15.Size = New System.Drawing.Size(16, 16)
        Me.Led15.TabIndex = 51
        Me.Led15.TabStop = False
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.Location = New System.Drawing.Point(487, 1)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(19, 13)
        Me.Label66.TabIndex = 21
        Me.Label66.Text = "15"
        Me.Label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led14
        '
        Me.Led14.BackColor = System.Drawing.Color.Blue
        Me.Led14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led14.Location = New System.Drawing.Point(460, 19)
        Me.Led14.Name = "Led14"
        Me.Led14.Size = New System.Drawing.Size(16, 16)
        Me.Led14.TabIndex = 50
        Me.Led14.TabStop = False
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(458, 1)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(19, 13)
        Me.Label67.TabIndex = 22
        Me.Label67.Text = "14"
        Me.Label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led13
        '
        Me.Led13.BackColor = System.Drawing.Color.Blue
        Me.Led13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led13.Location = New System.Drawing.Point(431, 19)
        Me.Led13.Name = "Led13"
        Me.Led13.Size = New System.Drawing.Size(16, 16)
        Me.Led13.TabIndex = 49
        Me.Led13.TabStop = False
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label81.Location = New System.Drawing.Point(183, 36)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(13, 13)
        Me.Label81.TabIndex = 23
        Me.Label81.Text = "4"
        Me.Label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led12
        '
        Me.Led12.BackColor = System.Drawing.Color.Blue
        Me.Led12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led12.Location = New System.Drawing.Point(402, 19)
        Me.Led12.Name = "Led12"
        Me.Led12.Size = New System.Drawing.Size(16, 16)
        Me.Led12.TabIndex = 48
        Me.Led12.TabStop = False
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.Location = New System.Drawing.Point(154, 36)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(13, 13)
        Me.Label80.TabIndex = 24
        Me.Label80.Text = "3"
        Me.Label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led11
        '
        Me.Led11.BackColor = System.Drawing.Color.Blue
        Me.Led11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led11.Location = New System.Drawing.Point(372, 19)
        Me.Led11.Name = "Led11"
        Me.Led11.Size = New System.Drawing.Size(16, 16)
        Me.Led11.TabIndex = 47
        Me.Led11.TabStop = False
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.Location = New System.Drawing.Point(125, 36)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(13, 13)
        Me.Label79.TabIndex = 25
        Me.Label79.Text = "2"
        Me.Label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led10
        '
        Me.Led10.BackColor = System.Drawing.Color.Blue
        Me.Led10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led10.Location = New System.Drawing.Point(343, 19)
        Me.Led10.Name = "Led10"
        Me.Led10.Size = New System.Drawing.Size(16, 16)
        Me.Led10.TabIndex = 46
        Me.Led10.TabStop = False
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label78.Location = New System.Drawing.Point(212, 36)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(13, 13)
        Me.Label78.TabIndex = 26
        Me.Label78.Text = "5"
        Me.Label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led9
        '
        Me.Led9.BackColor = System.Drawing.Color.Blue
        Me.Led9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led9.Location = New System.Drawing.Point(313, 19)
        Me.Led9.Name = "Led9"
        Me.Led9.Size = New System.Drawing.Size(16, 16)
        Me.Led9.TabIndex = 45
        Me.Led9.TabStop = False
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label77.Location = New System.Drawing.Point(241, 36)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(13, 13)
        Me.Label77.TabIndex = 27
        Me.Label77.Text = "6"
        Me.Label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led8
        '
        Me.Led8.BackColor = System.Drawing.Color.Blue
        Me.Led8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led8.Location = New System.Drawing.Point(284, 19)
        Me.Led8.Name = "Led8"
        Me.Led8.Size = New System.Drawing.Size(16, 16)
        Me.Led8.TabIndex = 44
        Me.Led8.TabStop = False
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label76.Location = New System.Drawing.Point(270, 36)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(13, 13)
        Me.Label76.TabIndex = 28
        Me.Label76.Text = "7"
        Me.Label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led7
        '
        Me.Led7.BackColor = System.Drawing.Color.Blue
        Me.Led7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led7.Location = New System.Drawing.Point(255, 19)
        Me.Led7.Name = "Led7"
        Me.Led7.Size = New System.Drawing.Size(16, 16)
        Me.Led7.TabIndex = 43
        Me.Led7.TabStop = False
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.Location = New System.Drawing.Point(299, 36)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(13, 13)
        Me.Label75.TabIndex = 29
        Me.Label75.Text = "8"
        Me.Label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led6
        '
        Me.Led6.BackColor = System.Drawing.Color.Blue
        Me.Led6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led6.Location = New System.Drawing.Point(226, 19)
        Me.Led6.Name = "Led6"
        Me.Led6.Size = New System.Drawing.Size(16, 16)
        Me.Led6.TabIndex = 42
        Me.Led6.TabStop = False
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label74.Location = New System.Drawing.Point(328, 36)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(13, 13)
        Me.Label74.TabIndex = 30
        Me.Label74.Text = "9"
        Me.Label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led5
        '
        Me.Led5.BackColor = System.Drawing.Color.Blue
        Me.Led5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led5.Location = New System.Drawing.Point(197, 19)
        Me.Led5.Name = "Led5"
        Me.Led5.Size = New System.Drawing.Size(16, 16)
        Me.Led5.TabIndex = 41
        Me.Led5.TabStop = False
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.Location = New System.Drawing.Point(353, 36)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(19, 13)
        Me.Label73.TabIndex = 31
        Me.Label73.Text = "10"
        Me.Label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led4
        '
        Me.Led4.BackColor = System.Drawing.Color.Blue
        Me.Led4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led4.Location = New System.Drawing.Point(168, 19)
        Me.Led4.Name = "Led4"
        Me.Led4.Size = New System.Drawing.Size(16, 16)
        Me.Led4.TabIndex = 40
        Me.Led4.TabStop = False
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label72.Location = New System.Drawing.Point(383, 36)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(19, 13)
        Me.Label72.TabIndex = 32
        Me.Label72.Text = "11"
        Me.Label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led3
        '
        Me.Led3.BackColor = System.Drawing.Color.Blue
        Me.Led3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led3.Location = New System.Drawing.Point(139, 19)
        Me.Led3.Name = "Led3"
        Me.Led3.Size = New System.Drawing.Size(16, 16)
        Me.Led3.TabIndex = 39
        Me.Led3.TabStop = False
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.Location = New System.Drawing.Point(411, 36)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(19, 13)
        Me.Label71.TabIndex = 33
        Me.Label71.Text = "12"
        Me.Label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led2
        '
        Me.Led2.BackColor = System.Drawing.Color.Blue
        Me.Led2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led2.Location = New System.Drawing.Point(110, 19)
        Me.Led2.Name = "Led2"
        Me.Led2.Size = New System.Drawing.Size(16, 16)
        Me.Led2.TabIndex = 38
        Me.Led2.TabStop = False
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.Location = New System.Drawing.Point(440, 36)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(19, 13)
        Me.Label70.TabIndex = 34
        Me.Label70.Text = "13"
        Me.Label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Led1
        '
        Me.Led1.BackColor = System.Drawing.Color.Blue
        Me.Led1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Led1.Location = New System.Drawing.Point(81, 19)
        Me.Led1.Name = "Led1"
        Me.Led1.Size = New System.Drawing.Size(16, 16)
        Me.Led1.TabIndex = 37
        Me.Led1.TabStop = False
        Me.Led1.Tag = "Unit1LineBreaker"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(499, 36)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(19, 13)
        Me.Label69.TabIndex = 35
        Me.Label69.Text = "15"
        Me.Label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.Location = New System.Drawing.Point(469, 36)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(19, 13)
        Me.Label68.TabIndex = 36
        Me.Label68.Text = "14"
        Me.Label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NetworkDevices
        '
        Me.NetworkDevices.Location = New System.Drawing.Point(810, 332)
        Me.NetworkDevices.Name = "NetworkDevices"
        Me.NetworkDevices.Size = New System.Drawing.Size(20, 23)
        Me.NetworkDevices.TabIndex = 120
        Me.NetworkDevices.Text = "Refresh Device Information"
        Me.NetworkDevices.UseVisualStyleBackColor = True
        Me.NetworkDevices.Visible = False
        '
        'DelayAll
        '
        Me.DelayAll.Checked = True
        Me.DelayAll.CheckState = System.Windows.Forms.CheckState.Checked
        Me.DelayAll.Enabled = False
        Me.DelayAll.Location = New System.Drawing.Point(807, 312)
        Me.DelayAll.Name = "DelayAll"
        Me.DelayAll.Size = New System.Drawing.Size(60, 17)
        Me.DelayAll.TabIndex = 115
        Me.DelayAll.Text = "Delay same for all the units"
        Me.DelayAll.UseVisualStyleBackColor = True
        Me.DelayAll.Visible = False
        '
        'SerialPort1
        '
        Me.SerialPort1.ReadTimeout = 5000
        Me.SerialPort1.WriteBufferSize = 4096
        Me.SerialPort1.WriteTimeout = 5000
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'MainBlock
        '
        Me.MainBlock.Controls.Add(Me.counter16)
        Me.MainBlock.Controls.Add(Me.counter15)
        Me.MainBlock.Controls.Add(Me.counter14)
        Me.MainBlock.Controls.Add(Me.counter13)
        Me.MainBlock.Controls.Add(Me.counter12)
        Me.MainBlock.Controls.Add(Me.counter11)
        Me.MainBlock.Controls.Add(Me.counter10)
        Me.MainBlock.Controls.Add(Me.counter9)
        Me.MainBlock.Controls.Add(Me.counter8)
        Me.MainBlock.Controls.Add(Me.counter7)
        Me.MainBlock.Controls.Add(Me.counter6)
        Me.MainBlock.Controls.Add(Me.counter5)
        Me.MainBlock.Controls.Add(Me.counter4)
        Me.MainBlock.Controls.Add(Me.counter3)
        Me.MainBlock.Controls.Add(Me.counter2)
        Me.MainBlock.Controls.Add(Me.counter1)
        Me.MainBlock.Controls.Add(Me.raise9)
        Me.MainBlock.Controls.Add(Me.raise10)
        Me.MainBlock.Controls.Add(Me.lower9)
        Me.MainBlock.Controls.Add(Me.alarm9)
        Me.MainBlock.Controls.Add(Me.raise11)
        Me.MainBlock.Controls.Add(Me.lower10)
        Me.MainBlock.Controls.Add(Me.alarm10)
        Me.MainBlock.Controls.Add(Me.raise12)
        Me.MainBlock.Controls.Add(Me.lower11)
        Me.MainBlock.Controls.Add(Me.alarm11)
        Me.MainBlock.Controls.Add(Me.raise13)
        Me.MainBlock.Controls.Add(Me.lower12)
        Me.MainBlock.Controls.Add(Me.alarm12)
        Me.MainBlock.Controls.Add(Me.raise14)
        Me.MainBlock.Controls.Add(Me.lower13)
        Me.MainBlock.Controls.Add(Me.raise15)
        Me.MainBlock.Controls.Add(Me.alarm13)
        Me.MainBlock.Controls.Add(Me.lower14)
        Me.MainBlock.Controls.Add(Me.alarm14)
        Me.MainBlock.Controls.Add(Me.raise16)
        Me.MainBlock.Controls.Add(Me.lower15)
        Me.MainBlock.Controls.Add(Me.alarm15)
        Me.MainBlock.Controls.Add(Me.raise8)
        Me.MainBlock.Controls.Add(Me.lower16)
        Me.MainBlock.Controls.Add(Me.alarm16)
        Me.MainBlock.Controls.Add(Me.raise7)
        Me.MainBlock.Controls.Add(Me.lower8)
        Me.MainBlock.Controls.Add(Me.alarm8)
        Me.MainBlock.Controls.Add(Me.raise6)
        Me.MainBlock.Controls.Add(Me.lower7)
        Me.MainBlock.Controls.Add(Me.alarm7)
        Me.MainBlock.Controls.Add(Me.raise5)
        Me.MainBlock.Controls.Add(Me.lower6)
        Me.MainBlock.Controls.Add(Me.alarm6)
        Me.MainBlock.Controls.Add(Me.raise4)
        Me.MainBlock.Controls.Add(Me.lower5)
        Me.MainBlock.Controls.Add(Me.alarm5)
        Me.MainBlock.Controls.Add(Me.raise3)
        Me.MainBlock.Controls.Add(Me.lower4)
        Me.MainBlock.Controls.Add(Me.alarm4)
        Me.MainBlock.Controls.Add(Me.raise2)
        Me.MainBlock.Controls.Add(Me.lower3)
        Me.MainBlock.Controls.Add(Me.alarm3)
        Me.MainBlock.Controls.Add(Me.raise1)
        Me.MainBlock.Controls.Add(Me.lower2)
        Me.MainBlock.Controls.Add(Me.alarm2)
        Me.MainBlock.Controls.Add(Me.lt16)
        Me.MainBlock.Controls.Add(Me.lower1)
        Me.MainBlock.Controls.Add(Me.lt15)
        Me.MainBlock.Controls.Add(Me.alarm1)
        Me.MainBlock.Controls.Add(Me.lt14)
        Me.MainBlock.Controls.Add(Me.lt13)
        Me.MainBlock.Controls.Add(Me.lt12)
        Me.MainBlock.Controls.Add(Me.lt11)
        Me.MainBlock.Controls.Add(Me.lt10)
        Me.MainBlock.Controls.Add(Me.lt9)
        Me.MainBlock.Controls.Add(Me.lt8)
        Me.MainBlock.Controls.Add(Me.lt7)
        Me.MainBlock.Controls.Add(Me.lt6)
        Me.MainBlock.Controls.Add(Me.lt5)
        Me.MainBlock.Controls.Add(Me.lt4)
        Me.MainBlock.Controls.Add(Me.lt3)
        Me.MainBlock.Controls.Add(Me.lt2)
        Me.MainBlock.Controls.Add(Me.lt1)
        Me.MainBlock.Controls.Add(Me.DelayAll)
        Me.MainBlock.Controls.Add(Me.NetworkDevices)
        Me.MainBlock.Controls.Add(Me.Label113)
        Me.MainBlock.Controls.Add(Me.Label112)
        Me.MainBlock.Controls.Add(Me.Label111)
        Me.MainBlock.Controls.Add(Me.Label110)
        Me.MainBlock.Controls.Add(Me.Label109)
        Me.MainBlock.Controls.Add(Me.Label108)
        Me.MainBlock.Controls.Add(Me.Label107)
        Me.MainBlock.Controls.Add(Me.Label106)
        Me.MainBlock.Controls.Add(Me.Label105)
        Me.MainBlock.Controls.Add(Me.Label104)
        Me.MainBlock.Controls.Add(Me.Label103)
        Me.MainBlock.Controls.Add(Me.Label102)
        Me.MainBlock.Controls.Add(Me.Label101)
        Me.MainBlock.Controls.Add(Me.Label100)
        Me.MainBlock.Controls.Add(Me.Label99)
        Me.MainBlock.Controls.Add(Me.Label98)
        Me.MainBlock.Controls.Add(Me.Label97)
        Me.MainBlock.Controls.Add(Me.Label96)
        Me.MainBlock.Controls.Add(Me.Label95)
        Me.MainBlock.Controls.Add(Me.Label94)
        Me.MainBlock.Controls.Add(Me.Label93)
        Me.MainBlock.Controls.Add(Me.Label92)
        Me.MainBlock.Controls.Add(Me.Label91)
        Me.MainBlock.Controls.Add(Me.Label90)
        Me.MainBlock.Controls.Add(Me.Label89)
        Me.MainBlock.Controls.Add(Me.Label88)
        Me.MainBlock.Controls.Add(Me.Label87)
        Me.MainBlock.Controls.Add(Me.Label86)
        Me.MainBlock.Controls.Add(Me.Label85)
        Me.MainBlock.Controls.Add(Me.Label84)
        Me.MainBlock.Controls.Add(Me.Label83)
        Me.MainBlock.Controls.Add(Me.Label82)
        Me.MainBlock.Controls.Add(Me.NumericUpDown15)
        Me.MainBlock.Controls.Add(Me.NumericUpDown14)
        Me.MainBlock.Controls.Add(Me.NumericUpDown13)
        Me.MainBlock.Controls.Add(Me.NumericUpDown12)
        Me.MainBlock.Controls.Add(Me.NumericUpDown11)
        Me.MainBlock.Controls.Add(Me.NumericUpDown10)
        Me.MainBlock.Controls.Add(Me.NumericUpDown9)
        Me.MainBlock.Controls.Add(Me.NumericUpDown8)
        Me.MainBlock.Controls.Add(Me.NumericUpDown7)
        Me.MainBlock.Controls.Add(Me.NumericUpDown6)
        Me.MainBlock.Controls.Add(Me.NumericUpDown5)
        Me.MainBlock.Controls.Add(Me.NumericUpDown4)
        Me.MainBlock.Controls.Add(Me.NumericUpDown3)
        Me.MainBlock.Controls.Add(Me.NumericUpDown2)
        Me.MainBlock.Controls.Add(Me.NumericUpDown1)
        Me.MainBlock.Controls.Add(Me.NumericUpDown0)
        Me.MainBlock.Controls.Add(Me.Label48)
        Me.MainBlock.Controls.Add(Me.Label47)
        Me.MainBlock.Controls.Add(Me.Label46)
        Me.MainBlock.Controls.Add(Me.Label45)
        Me.MainBlock.Controls.Add(Me.Label44)
        Me.MainBlock.Controls.Add(Me.Label43)
        Me.MainBlock.Controls.Add(Me.Label42)
        Me.MainBlock.Controls.Add(Me.Label41)
        Me.MainBlock.Controls.Add(Me.Label40)
        Me.MainBlock.Controls.Add(Me.Label39)
        Me.MainBlock.Controls.Add(Me.Label38)
        Me.MainBlock.Controls.Add(Me.Label37)
        Me.MainBlock.Controls.Add(Me.Label36)
        Me.MainBlock.Controls.Add(Me.Label35)
        Me.MainBlock.Controls.Add(Me.Label34)
        Me.MainBlock.Controls.Add(Me.Label33)
        Me.MainBlock.Controls.Add(Me.Label32)
        Me.MainBlock.Controls.Add(Me.Label31)
        Me.MainBlock.Controls.Add(Me.Label30)
        Me.MainBlock.Controls.Add(Me.Label29)
        Me.MainBlock.Controls.Add(Me.Label28)
        Me.MainBlock.Controls.Add(Me.Label27)
        Me.MainBlock.Controls.Add(Me.Label26)
        Me.MainBlock.Controls.Add(Me.Label25)
        Me.MainBlock.Controls.Add(Me.Label24)
        Me.MainBlock.Controls.Add(Me.Label23)
        Me.MainBlock.Controls.Add(Me.Label22)
        Me.MainBlock.Controls.Add(Me.Label20)
        Me.MainBlock.Controls.Add(Me.Label19)
        Me.MainBlock.Controls.Add(Me.Label18)
        Me.MainBlock.Controls.Add(Me.Unit8TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit15TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit14TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit13TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit12TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit11TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit10TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit9TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit7TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit6TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit5TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit3TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit2TieBreaker)
        Me.MainBlock.Controls.Add(Me.out16)
        Me.MainBlock.Controls.Add(Me.out15)
        Me.MainBlock.Controls.Add(Me.out14)
        Me.MainBlock.Controls.Add(Me.out13)
        Me.MainBlock.Controls.Add(Me.out12)
        Me.MainBlock.Controls.Add(Me.out11)
        Me.MainBlock.Controls.Add(Me.out10)
        Me.MainBlock.Controls.Add(Me.out9)
        Me.MainBlock.Controls.Add(Me.out8)
        Me.MainBlock.Controls.Add(Me.out7)
        Me.MainBlock.Controls.Add(Me.out6)
        Me.MainBlock.Controls.Add(Me.out5)
        Me.MainBlock.Controls.Add(Me.out4)
        Me.MainBlock.Controls.Add(Me.out3)
        Me.MainBlock.Controls.Add(Me.out2)
        Me.MainBlock.Controls.Add(Me.out1)
        Me.MainBlock.Controls.Add(Me.Unit1TieBreaker)
        Me.MainBlock.Controls.Add(Me.Unit16LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit15LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit14LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit13LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit12LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit11LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit10LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit9LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit8LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit7LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit6LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit5LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit4LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit3LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit2LineBreaker)
        Me.MainBlock.Controls.Add(Me.Unit1LineBreaker)
        Me.MainBlock.Controls.Add(PictureBox16)
        Me.MainBlock.Controls.Add(PictureBox15)
        Me.MainBlock.Controls.Add(PictureBox14)
        Me.MainBlock.Controls.Add(PictureBox13)
        Me.MainBlock.Controls.Add(PictureBox12)
        Me.MainBlock.Controls.Add(PictureBox11)
        Me.MainBlock.Controls.Add(PictureBox10)
        Me.MainBlock.Controls.Add(PictureBox9)
        Me.MainBlock.Controls.Add(PictureBox8)
        Me.MainBlock.Controls.Add(PictureBox7)
        Me.MainBlock.Controls.Add(Me.PictureBox6)
        Me.MainBlock.Controls.Add(PictureBox5)
        Me.MainBlock.Controls.Add(PictureBox4)
        Me.MainBlock.Controls.Add(PictureBox3)
        Me.MainBlock.Controls.Add(PictureBox2)
        Me.MainBlock.Controls.Add(Me.PictureBox1)
        Me.MainBlock.Controls.Add(Me.Label21)
        Me.MainBlock.Controls.Add(Me.Unit4TieBreaker)
        Me.MainBlock.Location = New System.Drawing.Point(0, 147)
        Me.MainBlock.Name = "MainBlock"
        Me.MainBlock.Size = New System.Drawing.Size(897, 360)
        Me.MainBlock.TabIndex = 165
        Me.MainBlock.Visible = False
        '
        'counter16
        '
        Me.counter16.BackColor = System.Drawing.Color.LightGray
        Me.counter16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter16.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter16.Location = New System.Drawing.Point(809, 183)
        Me.counter16.Name = "counter16"
        Me.counter16.Size = New System.Drawing.Size(16, 16)
        Me.counter16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter16.TabIndex = 362
        Me.counter16.TabStop = False
        Me.counter16.Tag = "15"
        Me.counter16.Visible = False
        '
        'counter15
        '
        Me.counter15.BackColor = System.Drawing.Color.LightGray
        Me.counter15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter15.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter15.Location = New System.Drawing.Point(699, 183)
        Me.counter15.Name = "counter15"
        Me.counter15.Size = New System.Drawing.Size(16, 16)
        Me.counter15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter15.TabIndex = 361
        Me.counter15.TabStop = False
        Me.counter15.Tag = "14"
        Me.counter15.Visible = False
        '
        'counter14
        '
        Me.counter14.BackColor = System.Drawing.Color.LightGray
        Me.counter14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter14.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter14.Location = New System.Drawing.Point(593, 183)
        Me.counter14.Name = "counter14"
        Me.counter14.Size = New System.Drawing.Size(16, 16)
        Me.counter14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter14.TabIndex = 360
        Me.counter14.TabStop = False
        Me.counter14.Tag = "13"
        Me.counter14.Visible = False
        '
        'counter13
        '
        Me.counter13.BackColor = System.Drawing.Color.LightGray
        Me.counter13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter13.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter13.Location = New System.Drawing.Point(483, 183)
        Me.counter13.Name = "counter13"
        Me.counter13.Size = New System.Drawing.Size(16, 16)
        Me.counter13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter13.TabIndex = 359
        Me.counter13.TabStop = False
        Me.counter13.Tag = "12"
        Me.counter13.Visible = False
        '
        'counter12
        '
        Me.counter12.BackColor = System.Drawing.Color.LightGray
        Me.counter12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter12.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter12.Location = New System.Drawing.Point(375, 183)
        Me.counter12.Name = "counter12"
        Me.counter12.Size = New System.Drawing.Size(16, 16)
        Me.counter12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter12.TabIndex = 358
        Me.counter12.TabStop = False
        Me.counter12.Tag = "11"
        Me.counter12.Visible = False
        '
        'counter11
        '
        Me.counter11.BackColor = System.Drawing.Color.LightGray
        Me.counter11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter11.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter11.Location = New System.Drawing.Point(267, 183)
        Me.counter11.Name = "counter11"
        Me.counter11.Size = New System.Drawing.Size(16, 16)
        Me.counter11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter11.TabIndex = 352
        Me.counter11.TabStop = False
        Me.counter11.Tag = "10"
        Me.counter11.Visible = False
        '
        'counter10
        '
        Me.counter10.BackColor = System.Drawing.Color.LightGray
        Me.counter10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter10.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter10.Location = New System.Drawing.Point(159, 183)
        Me.counter10.Name = "counter10"
        Me.counter10.Size = New System.Drawing.Size(16, 16)
        Me.counter10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter10.TabIndex = 351
        Me.counter10.TabStop = False
        Me.counter10.Tag = "9"
        Me.counter10.Visible = False
        '
        'counter9
        '
        Me.counter9.BackColor = System.Drawing.Color.LightGray
        Me.counter9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter9.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter9.Location = New System.Drawing.Point(52, 183)
        Me.counter9.Name = "counter9"
        Me.counter9.Size = New System.Drawing.Size(16, 16)
        Me.counter9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter9.TabIndex = 350
        Me.counter9.TabStop = False
        Me.counter9.Tag = "8"
        Me.counter9.Visible = False
        '
        'counter8
        '
        Me.counter8.BackColor = System.Drawing.Color.LightGray
        Me.counter8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter8.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter8.Location = New System.Drawing.Point(809, 5)
        Me.counter8.Name = "counter8"
        Me.counter8.Size = New System.Drawing.Size(16, 16)
        Me.counter8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter8.TabIndex = 349
        Me.counter8.TabStop = False
        Me.counter8.Tag = "7"
        Me.counter8.Visible = False
        '
        'counter7
        '
        Me.counter7.BackColor = System.Drawing.Color.LightGray
        Me.counter7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter7.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter7.Location = New System.Drawing.Point(699, 5)
        Me.counter7.Name = "counter7"
        Me.counter7.Size = New System.Drawing.Size(16, 16)
        Me.counter7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter7.TabIndex = 348
        Me.counter7.TabStop = False
        Me.counter7.Tag = "6"
        Me.counter7.Visible = False
        '
        'counter6
        '
        Me.counter6.BackColor = System.Drawing.Color.LightGray
        Me.counter6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter6.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter6.Location = New System.Drawing.Point(591, 5)
        Me.counter6.Name = "counter6"
        Me.counter6.Size = New System.Drawing.Size(16, 16)
        Me.counter6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter6.TabIndex = 347
        Me.counter6.TabStop = False
        Me.counter6.Tag = "5"
        Me.counter6.Visible = False
        '
        'counter5
        '
        Me.counter5.BackColor = System.Drawing.Color.LightGray
        Me.counter5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter5.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter5.Location = New System.Drawing.Point(483, 5)
        Me.counter5.Name = "counter5"
        Me.counter5.Size = New System.Drawing.Size(16, 16)
        Me.counter5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter5.TabIndex = 346
        Me.counter5.TabStop = False
        Me.counter5.Tag = "4"
        Me.counter5.Visible = False
        '
        'counter4
        '
        Me.counter4.BackColor = System.Drawing.Color.LightGray
        Me.counter4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter4.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter4.Location = New System.Drawing.Point(375, 5)
        Me.counter4.Name = "counter4"
        Me.counter4.Size = New System.Drawing.Size(16, 16)
        Me.counter4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter4.TabIndex = 344
        Me.counter4.TabStop = False
        Me.counter4.Tag = "3"
        Me.counter4.Visible = False
        '
        'counter3
        '
        Me.counter3.BackColor = System.Drawing.Color.LightGray
        Me.counter3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter3.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter3.Location = New System.Drawing.Point(267, 5)
        Me.counter3.Name = "counter3"
        Me.counter3.Size = New System.Drawing.Size(16, 16)
        Me.counter3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter3.TabIndex = 345
        Me.counter3.TabStop = False
        Me.counter3.Tag = "2"
        Me.counter3.Visible = False
        '
        'counter2
        '
        Me.counter2.BackColor = System.Drawing.Color.LightGray
        Me.counter2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter2.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter2.Location = New System.Drawing.Point(159, 5)
        Me.counter2.Name = "counter2"
        Me.counter2.Size = New System.Drawing.Size(16, 16)
        Me.counter2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter2.TabIndex = 344
        Me.counter2.TabStop = False
        Me.counter2.Tag = "1"
        Me.counter2.Visible = False
        '
        'counter1
        '
        Me.counter1.BackColor = System.Drawing.Color.LightGray
        Me.counter1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.counter1.Image = Global.ExtIO.My.Resources.Resources.Counter
        Me.counter1.Location = New System.Drawing.Point(52, 5)
        Me.counter1.Name = "counter1"
        Me.counter1.Size = New System.Drawing.Size(16, 16)
        Me.counter1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.counter1.TabIndex = 343
        Me.counter1.TabStop = False
        Me.counter1.Tag = "0"
        Me.counter1.Visible = False
        '
        'raise9
        '
        Me.raise9.BackColor = System.Drawing.Color.LightGray
        Me.raise9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise9.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise9.Location = New System.Drawing.Point(4, 183)
        Me.raise9.Name = "raise9"
        Me.raise9.Size = New System.Drawing.Size(16, 16)
        Me.raise9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise9.TabIndex = 307
        Me.raise9.TabStop = False
        Me.raise9.Tag = "8"
        Me.raise9.Visible = False
        '
        'raise10
        '
        Me.raise10.BackColor = System.Drawing.Color.LightGray
        Me.raise10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise10.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise10.Location = New System.Drawing.Point(111, 183)
        Me.raise10.Name = "raise10"
        Me.raise10.Size = New System.Drawing.Size(16, 16)
        Me.raise10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise10.TabIndex = 307
        Me.raise10.TabStop = False
        Me.raise10.Tag = "9"
        Me.raise10.Visible = False
        '
        'lower9
        '
        Me.lower9.BackColor = System.Drawing.Color.LightGray
        Me.lower9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower9.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower9.Location = New System.Drawing.Point(20, 183)
        Me.lower9.Name = "lower9"
        Me.lower9.Size = New System.Drawing.Size(16, 16)
        Me.lower9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower9.TabIndex = 308
        Me.lower9.TabStop = False
        Me.lower9.Tag = "8"
        Me.lower9.Visible = False
        '
        'alarm9
        '
        Me.alarm9.BackColor = System.Drawing.Color.LightGray
        Me.alarm9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm9.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm9.Location = New System.Drawing.Point(36, 183)
        Me.alarm9.Name = "alarm9"
        Me.alarm9.Size = New System.Drawing.Size(16, 16)
        Me.alarm9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm9.TabIndex = 309
        Me.alarm9.TabStop = False
        Me.alarm9.Tag = "8"
        Me.alarm9.Visible = False
        '
        'raise11
        '
        Me.raise11.BackColor = System.Drawing.Color.LightGray
        Me.raise11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise11.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise11.Location = New System.Drawing.Point(219, 183)
        Me.raise11.Name = "raise11"
        Me.raise11.Size = New System.Drawing.Size(16, 16)
        Me.raise11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise11.TabIndex = 307
        Me.raise11.TabStop = False
        Me.raise11.Tag = "10"
        Me.raise11.Visible = False
        '
        'lower10
        '
        Me.lower10.BackColor = System.Drawing.Color.LightGray
        Me.lower10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower10.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower10.Location = New System.Drawing.Point(127, 183)
        Me.lower10.Name = "lower10"
        Me.lower10.Size = New System.Drawing.Size(16, 16)
        Me.lower10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower10.TabIndex = 308
        Me.lower10.TabStop = False
        Me.lower10.Tag = "9"
        Me.lower10.Visible = False
        '
        'alarm10
        '
        Me.alarm10.BackColor = System.Drawing.Color.LightGray
        Me.alarm10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm10.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm10.Location = New System.Drawing.Point(143, 183)
        Me.alarm10.Name = "alarm10"
        Me.alarm10.Size = New System.Drawing.Size(16, 16)
        Me.alarm10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm10.TabIndex = 309
        Me.alarm10.TabStop = False
        Me.alarm10.Tag = "9"
        Me.alarm10.Visible = False
        '
        'raise12
        '
        Me.raise12.BackColor = System.Drawing.Color.LightGray
        Me.raise12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise12.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise12.Location = New System.Drawing.Point(327, 183)
        Me.raise12.Name = "raise12"
        Me.raise12.Size = New System.Drawing.Size(16, 16)
        Me.raise12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise12.TabIndex = 307
        Me.raise12.TabStop = False
        Me.raise12.Tag = "11"
        Me.raise12.Visible = False
        '
        'lower11
        '
        Me.lower11.BackColor = System.Drawing.Color.LightGray
        Me.lower11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower11.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower11.Location = New System.Drawing.Point(235, 183)
        Me.lower11.Name = "lower11"
        Me.lower11.Size = New System.Drawing.Size(16, 16)
        Me.lower11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower11.TabIndex = 308
        Me.lower11.TabStop = False
        Me.lower11.Tag = "10"
        Me.lower11.Visible = False
        '
        'alarm11
        '
        Me.alarm11.BackColor = System.Drawing.Color.LightGray
        Me.alarm11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm11.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm11.Location = New System.Drawing.Point(251, 183)
        Me.alarm11.Name = "alarm11"
        Me.alarm11.Size = New System.Drawing.Size(16, 16)
        Me.alarm11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm11.TabIndex = 309
        Me.alarm11.TabStop = False
        Me.alarm11.Tag = "10"
        Me.alarm11.Visible = False
        '
        'raise13
        '
        Me.raise13.BackColor = System.Drawing.Color.LightGray
        Me.raise13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise13.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise13.Location = New System.Drawing.Point(435, 183)
        Me.raise13.Name = "raise13"
        Me.raise13.Size = New System.Drawing.Size(16, 16)
        Me.raise13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise13.TabIndex = 307
        Me.raise13.TabStop = False
        Me.raise13.Tag = "12"
        Me.raise13.Visible = False
        '
        'lower12
        '
        Me.lower12.BackColor = System.Drawing.Color.LightGray
        Me.lower12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower12.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower12.Location = New System.Drawing.Point(343, 183)
        Me.lower12.Name = "lower12"
        Me.lower12.Size = New System.Drawing.Size(16, 16)
        Me.lower12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower12.TabIndex = 308
        Me.lower12.TabStop = False
        Me.lower12.Tag = "11"
        Me.lower12.Visible = False
        '
        'alarm12
        '
        Me.alarm12.BackColor = System.Drawing.Color.LightGray
        Me.alarm12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm12.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm12.Location = New System.Drawing.Point(359, 183)
        Me.alarm12.Name = "alarm12"
        Me.alarm12.Size = New System.Drawing.Size(16, 16)
        Me.alarm12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm12.TabIndex = 309
        Me.alarm12.TabStop = False
        Me.alarm12.Tag = "11"
        Me.alarm12.Visible = False
        '
        'raise14
        '
        Me.raise14.BackColor = System.Drawing.Color.LightGray
        Me.raise14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise14.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise14.Location = New System.Drawing.Point(545, 183)
        Me.raise14.Name = "raise14"
        Me.raise14.Size = New System.Drawing.Size(16, 16)
        Me.raise14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise14.TabIndex = 307
        Me.raise14.TabStop = False
        Me.raise14.Tag = "13"
        Me.raise14.Visible = False
        '
        'lower13
        '
        Me.lower13.BackColor = System.Drawing.Color.LightGray
        Me.lower13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower13.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower13.Location = New System.Drawing.Point(451, 183)
        Me.lower13.Name = "lower13"
        Me.lower13.Size = New System.Drawing.Size(16, 16)
        Me.lower13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower13.TabIndex = 308
        Me.lower13.TabStop = False
        Me.lower13.Tag = "12"
        Me.lower13.Visible = False
        '
        'raise15
        '
        Me.raise15.BackColor = System.Drawing.Color.LightGray
        Me.raise15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise15.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise15.Location = New System.Drawing.Point(651, 183)
        Me.raise15.Name = "raise15"
        Me.raise15.Size = New System.Drawing.Size(16, 16)
        Me.raise15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise15.TabIndex = 307
        Me.raise15.TabStop = False
        Me.raise15.Tag = "14"
        Me.raise15.Visible = False
        '
        'alarm13
        '
        Me.alarm13.BackColor = System.Drawing.Color.LightGray
        Me.alarm13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm13.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm13.Location = New System.Drawing.Point(467, 183)
        Me.alarm13.Name = "alarm13"
        Me.alarm13.Size = New System.Drawing.Size(16, 16)
        Me.alarm13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm13.TabIndex = 309
        Me.alarm13.TabStop = False
        Me.alarm13.Tag = "12"
        Me.alarm13.Visible = False
        '
        'lower14
        '
        Me.lower14.BackColor = System.Drawing.Color.LightGray
        Me.lower14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower14.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower14.Location = New System.Drawing.Point(561, 183)
        Me.lower14.Name = "lower14"
        Me.lower14.Size = New System.Drawing.Size(16, 16)
        Me.lower14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower14.TabIndex = 308
        Me.lower14.TabStop = False
        Me.lower14.Tag = "13"
        Me.lower14.Visible = False
        '
        'alarm14
        '
        Me.alarm14.BackColor = System.Drawing.Color.LightGray
        Me.alarm14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm14.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm14.Location = New System.Drawing.Point(577, 183)
        Me.alarm14.Name = "alarm14"
        Me.alarm14.Size = New System.Drawing.Size(16, 16)
        Me.alarm14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm14.TabIndex = 309
        Me.alarm14.TabStop = False
        Me.alarm14.Tag = "13"
        Me.alarm14.Visible = False
        '
        'raise16
        '
        Me.raise16.BackColor = System.Drawing.Color.LightGray
        Me.raise16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise16.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise16.Location = New System.Drawing.Point(761, 183)
        Me.raise16.Name = "raise16"
        Me.raise16.Size = New System.Drawing.Size(16, 16)
        Me.raise16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise16.TabIndex = 307
        Me.raise16.TabStop = False
        Me.raise16.Tag = "15"
        Me.raise16.Visible = False
        '
        'lower15
        '
        Me.lower15.BackColor = System.Drawing.Color.LightGray
        Me.lower15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower15.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower15.Location = New System.Drawing.Point(667, 183)
        Me.lower15.Name = "lower15"
        Me.lower15.Size = New System.Drawing.Size(16, 16)
        Me.lower15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower15.TabIndex = 308
        Me.lower15.TabStop = False
        Me.lower15.Tag = "14"
        Me.lower15.Visible = False
        '
        'alarm15
        '
        Me.alarm15.BackColor = System.Drawing.Color.LightGray
        Me.alarm15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm15.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm15.Location = New System.Drawing.Point(683, 183)
        Me.alarm15.Name = "alarm15"
        Me.alarm15.Size = New System.Drawing.Size(16, 16)
        Me.alarm15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm15.TabIndex = 309
        Me.alarm15.TabStop = False
        Me.alarm15.Tag = "14"
        Me.alarm15.Visible = False
        '
        'raise8
        '
        Me.raise8.BackColor = System.Drawing.Color.LightGray
        Me.raise8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise8.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise8.Location = New System.Drawing.Point(761, 5)
        Me.raise8.Name = "raise8"
        Me.raise8.Size = New System.Drawing.Size(16, 16)
        Me.raise8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise8.TabIndex = 307
        Me.raise8.TabStop = False
        Me.raise8.Tag = "7"
        Me.raise8.Visible = False
        '
        'lower16
        '
        Me.lower16.BackColor = System.Drawing.Color.LightGray
        Me.lower16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower16.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower16.Location = New System.Drawing.Point(777, 183)
        Me.lower16.Name = "lower16"
        Me.lower16.Size = New System.Drawing.Size(16, 16)
        Me.lower16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower16.TabIndex = 308
        Me.lower16.TabStop = False
        Me.lower16.Tag = "15"
        Me.lower16.Visible = False
        '
        'alarm16
        '
        Me.alarm16.BackColor = System.Drawing.Color.LightGray
        Me.alarm16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm16.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm16.Location = New System.Drawing.Point(793, 183)
        Me.alarm16.Name = "alarm16"
        Me.alarm16.Size = New System.Drawing.Size(16, 16)
        Me.alarm16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm16.TabIndex = 309
        Me.alarm16.TabStop = False
        Me.alarm16.Tag = "15"
        Me.alarm16.Visible = False
        '
        'raise7
        '
        Me.raise7.BackColor = System.Drawing.Color.LightGray
        Me.raise7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise7.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise7.Location = New System.Drawing.Point(651, 5)
        Me.raise7.Name = "raise7"
        Me.raise7.Size = New System.Drawing.Size(16, 16)
        Me.raise7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise7.TabIndex = 307
        Me.raise7.TabStop = False
        Me.raise7.Tag = "6"
        Me.raise7.Visible = False
        '
        'lower8
        '
        Me.lower8.BackColor = System.Drawing.Color.LightGray
        Me.lower8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower8.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower8.Location = New System.Drawing.Point(777, 5)
        Me.lower8.Name = "lower8"
        Me.lower8.Size = New System.Drawing.Size(16, 16)
        Me.lower8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower8.TabIndex = 308
        Me.lower8.TabStop = False
        Me.lower8.Tag = "7"
        Me.lower8.Visible = False
        '
        'alarm8
        '
        Me.alarm8.BackColor = System.Drawing.Color.LightGray
        Me.alarm8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm8.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm8.Location = New System.Drawing.Point(793, 5)
        Me.alarm8.Name = "alarm8"
        Me.alarm8.Size = New System.Drawing.Size(16, 16)
        Me.alarm8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm8.TabIndex = 309
        Me.alarm8.TabStop = False
        Me.alarm8.Tag = "7"
        Me.alarm8.Visible = False
        '
        'raise6
        '
        Me.raise6.BackColor = System.Drawing.Color.LightGray
        Me.raise6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise6.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise6.Location = New System.Drawing.Point(543, 5)
        Me.raise6.Name = "raise6"
        Me.raise6.Size = New System.Drawing.Size(16, 16)
        Me.raise6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise6.TabIndex = 307
        Me.raise6.TabStop = False
        Me.raise6.Tag = "5"
        Me.raise6.Visible = False
        '
        'lower7
        '
        Me.lower7.BackColor = System.Drawing.Color.LightGray
        Me.lower7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower7.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower7.Location = New System.Drawing.Point(667, 5)
        Me.lower7.Name = "lower7"
        Me.lower7.Size = New System.Drawing.Size(16, 16)
        Me.lower7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower7.TabIndex = 308
        Me.lower7.TabStop = False
        Me.lower7.Tag = "6"
        Me.lower7.Visible = False
        '
        'alarm7
        '
        Me.alarm7.BackColor = System.Drawing.Color.LightGray
        Me.alarm7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm7.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm7.Location = New System.Drawing.Point(683, 5)
        Me.alarm7.Name = "alarm7"
        Me.alarm7.Size = New System.Drawing.Size(16, 16)
        Me.alarm7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm7.TabIndex = 309
        Me.alarm7.TabStop = False
        Me.alarm7.Tag = "6"
        Me.alarm7.Visible = False
        '
        'raise5
        '
        Me.raise5.BackColor = System.Drawing.Color.LightGray
        Me.raise5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise5.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise5.Location = New System.Drawing.Point(435, 5)
        Me.raise5.Name = "raise5"
        Me.raise5.Size = New System.Drawing.Size(16, 16)
        Me.raise5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise5.TabIndex = 307
        Me.raise5.TabStop = False
        Me.raise5.Tag = "4"
        Me.raise5.Visible = False
        '
        'lower6
        '
        Me.lower6.BackColor = System.Drawing.Color.LightGray
        Me.lower6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower6.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower6.Location = New System.Drawing.Point(559, 5)
        Me.lower6.Name = "lower6"
        Me.lower6.Size = New System.Drawing.Size(16, 16)
        Me.lower6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower6.TabIndex = 308
        Me.lower6.TabStop = False
        Me.lower6.Tag = "5"
        Me.lower6.Visible = False
        '
        'alarm6
        '
        Me.alarm6.BackColor = System.Drawing.Color.LightGray
        Me.alarm6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm6.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm6.Location = New System.Drawing.Point(575, 5)
        Me.alarm6.Name = "alarm6"
        Me.alarm6.Size = New System.Drawing.Size(16, 16)
        Me.alarm6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm6.TabIndex = 309
        Me.alarm6.TabStop = False
        Me.alarm6.Tag = "5"
        Me.alarm6.Visible = False
        '
        'raise4
        '
        Me.raise4.BackColor = System.Drawing.Color.LightGray
        Me.raise4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise4.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise4.Location = New System.Drawing.Point(327, 5)
        Me.raise4.Name = "raise4"
        Me.raise4.Size = New System.Drawing.Size(16, 16)
        Me.raise4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise4.TabIndex = 307
        Me.raise4.TabStop = False
        Me.raise4.Tag = "3"
        Me.raise4.Visible = False
        '
        'lower5
        '
        Me.lower5.BackColor = System.Drawing.Color.LightGray
        Me.lower5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower5.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower5.Location = New System.Drawing.Point(451, 5)
        Me.lower5.Name = "lower5"
        Me.lower5.Size = New System.Drawing.Size(16, 16)
        Me.lower5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower5.TabIndex = 308
        Me.lower5.TabStop = False
        Me.lower5.Tag = "4"
        Me.lower5.Visible = False
        '
        'alarm5
        '
        Me.alarm5.BackColor = System.Drawing.Color.LightGray
        Me.alarm5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm5.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm5.Location = New System.Drawing.Point(467, 5)
        Me.alarm5.Name = "alarm5"
        Me.alarm5.Size = New System.Drawing.Size(16, 16)
        Me.alarm5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm5.TabIndex = 309
        Me.alarm5.TabStop = False
        Me.alarm5.Tag = "4"
        Me.alarm5.Visible = False
        '
        'raise3
        '
        Me.raise3.BackColor = System.Drawing.Color.LightGray
        Me.raise3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise3.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise3.Location = New System.Drawing.Point(219, 5)
        Me.raise3.Name = "raise3"
        Me.raise3.Size = New System.Drawing.Size(16, 16)
        Me.raise3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise3.TabIndex = 307
        Me.raise3.TabStop = False
        Me.raise3.Tag = "2"
        Me.raise3.Visible = False
        '
        'lower4
        '
        Me.lower4.BackColor = System.Drawing.Color.LightGray
        Me.lower4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower4.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower4.Location = New System.Drawing.Point(343, 5)
        Me.lower4.Name = "lower4"
        Me.lower4.Size = New System.Drawing.Size(16, 16)
        Me.lower4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower4.TabIndex = 308
        Me.lower4.TabStop = False
        Me.lower4.Tag = "3"
        Me.lower4.Visible = False
        '
        'alarm4
        '
        Me.alarm4.BackColor = System.Drawing.Color.LightGray
        Me.alarm4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm4.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm4.Location = New System.Drawing.Point(359, 5)
        Me.alarm4.Name = "alarm4"
        Me.alarm4.Size = New System.Drawing.Size(16, 16)
        Me.alarm4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm4.TabIndex = 309
        Me.alarm4.TabStop = False
        Me.alarm4.Tag = "3"
        Me.alarm4.Visible = False
        '
        'raise2
        '
        Me.raise2.BackColor = System.Drawing.Color.LightGray
        Me.raise2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise2.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise2.Location = New System.Drawing.Point(111, 5)
        Me.raise2.Name = "raise2"
        Me.raise2.Size = New System.Drawing.Size(16, 16)
        Me.raise2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise2.TabIndex = 307
        Me.raise2.TabStop = False
        Me.raise2.Tag = "1"
        Me.raise2.Visible = False
        '
        'lower3
        '
        Me.lower3.BackColor = System.Drawing.Color.LightGray
        Me.lower3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower3.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower3.Location = New System.Drawing.Point(235, 5)
        Me.lower3.Name = "lower3"
        Me.lower3.Size = New System.Drawing.Size(16, 16)
        Me.lower3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower3.TabIndex = 308
        Me.lower3.TabStop = False
        Me.lower3.Tag = "2"
        Me.lower3.Visible = False
        '
        'alarm3
        '
        Me.alarm3.BackColor = System.Drawing.Color.LightGray
        Me.alarm3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm3.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm3.Location = New System.Drawing.Point(251, 5)
        Me.alarm3.Name = "alarm3"
        Me.alarm3.Size = New System.Drawing.Size(16, 16)
        Me.alarm3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm3.TabIndex = 309
        Me.alarm3.TabStop = False
        Me.alarm3.Tag = "2"
        Me.alarm3.Visible = False
        '
        'raise1
        '
        Me.raise1.BackColor = System.Drawing.Color.LightGray
        Me.raise1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.raise1.Image = Global.ExtIO.My.Resources.Resources.Raise
        Me.raise1.Location = New System.Drawing.Point(3, 5)
        Me.raise1.Name = "raise1"
        Me.raise1.Size = New System.Drawing.Size(16, 16)
        Me.raise1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.raise1.TabIndex = 307
        Me.raise1.TabStop = False
        Me.raise1.Tag = "0"
        Me.raise1.Visible = False
        '
        'lower2
        '
        Me.lower2.BackColor = System.Drawing.Color.LightGray
        Me.lower2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower2.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower2.Location = New System.Drawing.Point(127, 5)
        Me.lower2.Name = "lower2"
        Me.lower2.Size = New System.Drawing.Size(16, 16)
        Me.lower2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower2.TabIndex = 308
        Me.lower2.TabStop = False
        Me.lower2.Tag = "1"
        Me.lower2.Visible = False
        '
        'alarm2
        '
        Me.alarm2.BackColor = System.Drawing.Color.LightGray
        Me.alarm2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm2.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm2.Location = New System.Drawing.Point(143, 5)
        Me.alarm2.Name = "alarm2"
        Me.alarm2.Size = New System.Drawing.Size(16, 16)
        Me.alarm2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm2.TabIndex = 309
        Me.alarm2.TabStop = False
        Me.alarm2.Tag = "1"
        Me.alarm2.Visible = False
        '
        'lt16
        '
        Me.lt16.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt16.Location = New System.Drawing.Point(800, 249)
        Me.lt16.Name = "lt16"
        Me.lt16.Size = New System.Drawing.Size(69, 59)
        Me.lt16.TabIndex = 342
        Me.lt16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lower1
        '
        Me.lower1.BackColor = System.Drawing.Color.LightGray
        Me.lower1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lower1.Image = Global.ExtIO.My.Resources.Resources.Lower
        Me.lower1.Location = New System.Drawing.Point(20, 5)
        Me.lower1.Name = "lower1"
        Me.lower1.Size = New System.Drawing.Size(16, 16)
        Me.lower1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lower1.TabIndex = 308
        Me.lower1.TabStop = False
        Me.lower1.Tag = "0"
        Me.lower1.Visible = False
        '
        'lt15
        '
        Me.lt15.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt15.Location = New System.Drawing.Point(692, 249)
        Me.lt15.Name = "lt15"
        Me.lt15.Size = New System.Drawing.Size(69, 59)
        Me.lt15.TabIndex = 341
        Me.lt15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'alarm1
        '
        Me.alarm1.BackColor = System.Drawing.Color.LightGray
        Me.alarm1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.alarm1.Image = Global.ExtIO.My.Resources.Resources.Alarm
        Me.alarm1.Location = New System.Drawing.Point(36, 5)
        Me.alarm1.Name = "alarm1"
        Me.alarm1.Size = New System.Drawing.Size(16, 16)
        Me.alarm1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.alarm1.TabIndex = 309
        Me.alarm1.TabStop = False
        Me.alarm1.Tag = "0"
        Me.alarm1.Visible = False
        '
        'lt14
        '
        Me.lt14.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt14.Location = New System.Drawing.Point(584, 249)
        Me.lt14.Name = "lt14"
        Me.lt14.Size = New System.Drawing.Size(69, 59)
        Me.lt14.TabIndex = 340
        Me.lt14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt13
        '
        Me.lt13.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt13.Location = New System.Drawing.Point(476, 249)
        Me.lt13.Name = "lt13"
        Me.lt13.Size = New System.Drawing.Size(69, 59)
        Me.lt13.TabIndex = 339
        Me.lt13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt12
        '
        Me.lt12.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt12.Location = New System.Drawing.Point(368, 249)
        Me.lt12.Name = "lt12"
        Me.lt12.Size = New System.Drawing.Size(69, 59)
        Me.lt12.TabIndex = 338
        Me.lt12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt11
        '
        Me.lt11.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt11.Location = New System.Drawing.Point(260, 249)
        Me.lt11.Name = "lt11"
        Me.lt11.Size = New System.Drawing.Size(69, 59)
        Me.lt11.TabIndex = 337
        Me.lt11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt10
        '
        Me.lt10.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt10.Location = New System.Drawing.Point(152, 249)
        Me.lt10.Name = "lt10"
        Me.lt10.Size = New System.Drawing.Size(69, 59)
        Me.lt10.TabIndex = 336
        Me.lt10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt9
        '
        Me.lt9.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt9.Location = New System.Drawing.Point(44, 249)
        Me.lt9.Name = "lt9"
        Me.lt9.Size = New System.Drawing.Size(69, 59)
        Me.lt9.TabIndex = 335
        Me.lt9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt8
        '
        Me.lt8.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt8.Location = New System.Drawing.Point(803, 70)
        Me.lt8.Name = "lt8"
        Me.lt8.Size = New System.Drawing.Size(69, 59)
        Me.lt8.TabIndex = 334
        Me.lt8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt7
        '
        Me.lt7.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt7.Location = New System.Drawing.Point(695, 70)
        Me.lt7.Name = "lt7"
        Me.lt7.Size = New System.Drawing.Size(69, 59)
        Me.lt7.TabIndex = 333
        Me.lt7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt6
        '
        Me.lt6.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt6.Location = New System.Drawing.Point(587, 70)
        Me.lt6.Name = "lt6"
        Me.lt6.Size = New System.Drawing.Size(69, 59)
        Me.lt6.TabIndex = 332
        Me.lt6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt5
        '
        Me.lt5.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt5.Location = New System.Drawing.Point(479, 70)
        Me.lt5.Name = "lt5"
        Me.lt5.Size = New System.Drawing.Size(69, 59)
        Me.lt5.TabIndex = 331
        Me.lt5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt4
        '
        Me.lt4.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt4.Location = New System.Drawing.Point(371, 70)
        Me.lt4.Name = "lt4"
        Me.lt4.Size = New System.Drawing.Size(69, 59)
        Me.lt4.TabIndex = 330
        Me.lt4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt3
        '
        Me.lt3.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt3.Location = New System.Drawing.Point(263, 70)
        Me.lt3.Name = "lt3"
        Me.lt3.Size = New System.Drawing.Size(69, 59)
        Me.lt3.TabIndex = 329
        Me.lt3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt2
        '
        Me.lt2.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt2.Location = New System.Drawing.Point(155, 70)
        Me.lt2.Name = "lt2"
        Me.lt2.Size = New System.Drawing.Size(69, 59)
        Me.lt2.TabIndex = 328
        Me.lt2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lt1
        '
        Me.lt1.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lt1.Location = New System.Drawing.Point(46, 70)
        Me.lt1.Name = "lt1"
        Me.lt1.Size = New System.Drawing.Size(69, 59)
        Me.lt1.TabIndex = 327
        Me.lt1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label113
        '
        Me.Label113.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label113.Location = New System.Drawing.Point(801, 207)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(67, 42)
        Me.Label113.TabIndex = 306
        Me.Label113.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label112
        '
        Me.Label112.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label112.Location = New System.Drawing.Point(693, 207)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(67, 42)
        Me.Label112.TabIndex = 305
        Me.Label112.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label111
        '
        Me.Label111.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label111.Location = New System.Drawing.Point(585, 207)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(67, 42)
        Me.Label111.TabIndex = 304
        Me.Label111.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label110
        '
        Me.Label110.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label110.Location = New System.Drawing.Point(477, 207)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(67, 42)
        Me.Label110.TabIndex = 303
        Me.Label110.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label109
        '
        Me.Label109.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label109.Location = New System.Drawing.Point(369, 207)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(67, 42)
        Me.Label109.TabIndex = 302
        Me.Label109.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label108
        '
        Me.Label108.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label108.Location = New System.Drawing.Point(261, 207)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(67, 42)
        Me.Label108.TabIndex = 301
        Me.Label108.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label107
        '
        Me.Label107.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label107.Location = New System.Drawing.Point(153, 207)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(67, 42)
        Me.Label107.TabIndex = 300
        Me.Label107.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label106
        '
        Me.Label106.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label106.Location = New System.Drawing.Point(45, 207)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(67, 42)
        Me.Label106.TabIndex = 299
        Me.Label106.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label105
        '
        Me.Label105.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label105.Location = New System.Drawing.Point(804, 28)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(67, 42)
        Me.Label105.TabIndex = 298
        Me.Label105.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label104
        '
        Me.Label104.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label104.Location = New System.Drawing.Point(696, 28)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(67, 42)
        Me.Label104.TabIndex = 297
        Me.Label104.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label103
        '
        Me.Label103.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label103.Location = New System.Drawing.Point(588, 28)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(67, 42)
        Me.Label103.TabIndex = 296
        Me.Label103.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label102
        '
        Me.Label102.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label102.Location = New System.Drawing.Point(480, 28)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(67, 42)
        Me.Label102.TabIndex = 295
        Me.Label102.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label101
        '
        Me.Label101.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label101.Location = New System.Drawing.Point(372, 28)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(67, 42)
        Me.Label101.TabIndex = 294
        Me.Label101.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label100
        '
        Me.Label100.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label100.Location = New System.Drawing.Point(264, 28)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(67, 42)
        Me.Label100.TabIndex = 293
        Me.Label100.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label99
        '
        Me.Label99.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label99.Location = New System.Drawing.Point(156, 28)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(67, 42)
        Me.Label99.TabIndex = 292
        Me.Label99.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label98
        '
        Me.Label98.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label98.Location = New System.Drawing.Point(47, 28)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(67, 42)
        Me.Label98.TabIndex = 291
        Me.Label98.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Location = New System.Drawing.Point(1114, 258)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(56, 13)
        Me.Label97.TabIndex = 290
        Me.Label97.Text = "Delay (ms)"
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.Location = New System.Drawing.Point(1114, 183)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(56, 13)
        Me.Label96.TabIndex = 289
        Me.Label96.Text = "Delay (ms)"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Location = New System.Drawing.Point(1114, 199)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(56, 13)
        Me.Label95.TabIndex = 288
        Me.Label95.Text = "Delay (ms)"
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.Location = New System.Drawing.Point(1114, 212)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(56, 13)
        Me.Label94.TabIndex = 287
        Me.Label94.Text = "Delay (ms)"
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(1114, 229)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(56, 13)
        Me.Label93.TabIndex = 286
        Me.Label93.Text = "Delay (ms)"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(1114, 242)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(56, 13)
        Me.Label92.TabIndex = 285
        Me.Label92.Text = "Delay (ms)"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Location = New System.Drawing.Point(1114, 271)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(56, 13)
        Me.Label91.TabIndex = 284
        Me.Label91.Text = "Delay (ms)"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(1114, 288)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(56, 13)
        Me.Label90.TabIndex = 283
        Me.Label90.Text = "Delay (ms)"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(1114, 164)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(56, 13)
        Me.Label89.TabIndex = 282
        Me.Label89.Text = "Delay (ms)"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(1114, 143)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(56, 13)
        Me.Label88.TabIndex = 281
        Me.Label88.Text = "Delay (ms)"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Location = New System.Drawing.Point(1114, 129)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(56, 13)
        Me.Label87.TabIndex = 280
        Me.Label87.Text = "Delay (ms)"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(1114, 108)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(56, 13)
        Me.Label86.TabIndex = 279
        Me.Label86.Text = "Delay (ms)"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(1114, 88)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(56, 13)
        Me.Label85.TabIndex = 278
        Me.Label85.Text = "Delay (ms)"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(1114, 66)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(56, 13)
        Me.Label84.TabIndex = 277
        Me.Label84.Text = "Delay (ms)"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(1114, 44)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(56, 13)
        Me.Label83.TabIndex = 276
        Me.Label83.Text = "Delay (ms)"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(1114, 21)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(56, 13)
        Me.Label82.TabIndex = 275
        Me.Label82.Text = "Delay (ms)"
        '
        'NumericUpDown15
        '
        Me.NumericUpDown15.AutoSize = True
        Me.NumericUpDown15.Location = New System.Drawing.Point(1185, 332)
        Me.NumericUpDown15.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown15.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown15.Name = "NumericUpDown15"
        Me.NumericUpDown15.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown15.TabIndex = 274
        Me.NumericUpDown15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown15.ThousandsSeparator = True
        Me.NumericUpDown15.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown15.Visible = False
        '
        'NumericUpDown14
        '
        Me.NumericUpDown14.AutoSize = True
        Me.NumericUpDown14.Location = New System.Drawing.Point(1185, 307)
        Me.NumericUpDown14.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown14.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown14.Name = "NumericUpDown14"
        Me.NumericUpDown14.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown14.TabIndex = 273
        Me.NumericUpDown14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown14.ThousandsSeparator = True
        Me.NumericUpDown14.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown14.Visible = False
        '
        'NumericUpDown13
        '
        Me.NumericUpDown13.AutoSize = True
        Me.NumericUpDown13.Location = New System.Drawing.Point(1185, 286)
        Me.NumericUpDown13.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown13.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown13.Name = "NumericUpDown13"
        Me.NumericUpDown13.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown13.TabIndex = 272
        Me.NumericUpDown13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown13.ThousandsSeparator = True
        Me.NumericUpDown13.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown13.Visible = False
        '
        'NumericUpDown12
        '
        Me.NumericUpDown12.AutoSize = True
        Me.NumericUpDown12.Location = New System.Drawing.Point(1185, 264)
        Me.NumericUpDown12.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown12.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown12.Name = "NumericUpDown12"
        Me.NumericUpDown12.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown12.TabIndex = 271
        Me.NumericUpDown12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown12.ThousandsSeparator = True
        Me.NumericUpDown12.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown12.Visible = False
        '
        'NumericUpDown11
        '
        Me.NumericUpDown11.AutoSize = True
        Me.NumericUpDown11.Location = New System.Drawing.Point(1176, 242)
        Me.NumericUpDown11.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown11.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown11.Name = "NumericUpDown11"
        Me.NumericUpDown11.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown11.TabIndex = 270
        Me.NumericUpDown11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown11.ThousandsSeparator = True
        Me.NumericUpDown11.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown11.Visible = False
        '
        'NumericUpDown10
        '
        Me.NumericUpDown10.AutoSize = True
        Me.NumericUpDown10.Location = New System.Drawing.Point(1185, 227)
        Me.NumericUpDown10.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown10.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown10.Name = "NumericUpDown10"
        Me.NumericUpDown10.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown10.TabIndex = 269
        Me.NumericUpDown10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown10.ThousandsSeparator = True
        Me.NumericUpDown10.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown10.Visible = False
        '
        'NumericUpDown9
        '
        Me.NumericUpDown9.AutoSize = True
        Me.NumericUpDown9.Location = New System.Drawing.Point(1185, 205)
        Me.NumericUpDown9.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown9.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown9.Name = "NumericUpDown9"
        Me.NumericUpDown9.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown9.TabIndex = 268
        Me.NumericUpDown9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown9.ThousandsSeparator = True
        Me.NumericUpDown9.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown9.Visible = False
        '
        'NumericUpDown8
        '
        Me.NumericUpDown8.AutoSize = True
        Me.NumericUpDown8.Location = New System.Drawing.Point(1185, 183)
        Me.NumericUpDown8.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown8.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown8.Name = "NumericUpDown8"
        Me.NumericUpDown8.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown8.TabIndex = 267
        Me.NumericUpDown8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown8.ThousandsSeparator = True
        Me.NumericUpDown8.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown8.Visible = False
        '
        'NumericUpDown7
        '
        Me.NumericUpDown7.AutoSize = True
        Me.NumericUpDown7.Location = New System.Drawing.Point(1185, 164)
        Me.NumericUpDown7.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown7.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown7.Name = "NumericUpDown7"
        Me.NumericUpDown7.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown7.TabIndex = 266
        Me.NumericUpDown7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown7.ThousandsSeparator = True
        Me.NumericUpDown7.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown7.Visible = False
        '
        'NumericUpDown6
        '
        Me.NumericUpDown6.AutoSize = True
        Me.NumericUpDown6.Location = New System.Drawing.Point(1185, 143)
        Me.NumericUpDown6.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown6.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown6.Name = "NumericUpDown6"
        Me.NumericUpDown6.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown6.TabIndex = 265
        Me.NumericUpDown6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown6.ThousandsSeparator = True
        Me.NumericUpDown6.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown6.Visible = False
        '
        'NumericUpDown5
        '
        Me.NumericUpDown5.AutoSize = True
        Me.NumericUpDown5.Location = New System.Drawing.Point(1185, 127)
        Me.NumericUpDown5.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown5.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown5.Name = "NumericUpDown5"
        Me.NumericUpDown5.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown5.TabIndex = 264
        Me.NumericUpDown5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown5.ThousandsSeparator = True
        Me.NumericUpDown5.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown5.Visible = False
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.AutoSize = True
        Me.NumericUpDown4.Location = New System.Drawing.Point(1185, 108)
        Me.NumericUpDown4.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown4.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown4.TabIndex = 263
        Me.NumericUpDown4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown4.ThousandsSeparator = True
        Me.NumericUpDown4.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown4.Visible = False
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.AutoSize = True
        Me.NumericUpDown3.Location = New System.Drawing.Point(1185, 86)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown3.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown3.TabIndex = 262
        Me.NumericUpDown3.Tag = "4"
        Me.NumericUpDown3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown3.ThousandsSeparator = True
        Me.NumericUpDown3.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown3.Visible = False
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.AutoSize = True
        Me.NumericUpDown2.Location = New System.Drawing.Point(1185, 64)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown2.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown2.TabIndex = 261
        Me.NumericUpDown2.Tag = "3"
        Me.NumericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown2.ThousandsSeparator = True
        Me.NumericUpDown2.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown2.Visible = False
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.AutoSize = True
        Me.NumericUpDown1.Location = New System.Drawing.Point(1185, 42)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown1.TabIndex = 260
        Me.NumericUpDown1.Tag = "2"
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown1.ThousandsSeparator = True
        Me.NumericUpDown1.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown1.Visible = False
        '
        'NumericUpDown0
        '
        Me.NumericUpDown0.AutoSize = True
        Me.NumericUpDown0.Location = New System.Drawing.Point(1185, 19)
        Me.NumericUpDown0.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.NumericUpDown0.Minimum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.NumericUpDown0.Name = "NumericUpDown0"
        Me.NumericUpDown0.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDown0.TabIndex = 259
        Me.NumericUpDown0.Tag = "1"
        Me.NumericUpDown0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown0.ThousandsSeparator = True
        Me.NumericUpDown0.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown0.Visible = False
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(711, 307)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(37, 13)
        Me.Label48.TabIndex = 258
        Me.Label48.Text = "Tie 15"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(603, 307)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(37, 13)
        Me.Label47.TabIndex = 257
        Me.Label47.Text = "Tie 14"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(495, 307)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(37, 13)
        Me.Label46.TabIndex = 256
        Me.Label46.Text = "Tie 13"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(387, 307)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(37, 13)
        Me.Label45.TabIndex = 255
        Me.Label45.Text = "Tie 12"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(279, 307)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(37, 13)
        Me.Label44.TabIndex = 254
        Me.Label44.Text = "Tie 11"
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(171, 307)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(37, 13)
        Me.Label43.TabIndex = 253
        Me.Label43.Text = "Tie 10"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(66, 307)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(31, 13)
        Me.Label42.TabIndex = 252
        Me.Label42.Text = "Tie 9"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(762, 281)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(42, 13)
        Me.Label41.TabIndex = 251
        Me.Label41.Text = "Line 16"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(654, 281)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(42, 13)
        Me.Label40.TabIndex = 250
        Me.Label40.Text = "Line 15"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(546, 281)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(42, 13)
        Me.Label39.TabIndex = 249
        Me.Label39.Text = "Line 14"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(438, 281)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(42, 13)
        Me.Label38.TabIndex = 248
        Me.Label38.Text = "Line 13"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(330, 281)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(42, 13)
        Me.Label37.TabIndex = 247
        Me.Label37.Text = "Line 12"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(222, 281)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(42, 13)
        Me.Label36.TabIndex = 246
        Me.Label36.Text = "Line 11"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(114, 281)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(42, 13)
        Me.Label35.TabIndex = 245
        Me.Label35.Text = "Line 10"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(9, 281)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(36, 13)
        Me.Label34.TabIndex = 244
        Me.Label34.Text = "Line 9"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(765, 101)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(36, 13)
        Me.Label33.TabIndex = 243
        Me.Label33.Text = "Line 8"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(657, 101)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(36, 13)
        Me.Label32.TabIndex = 242
        Me.Label32.Text = "Line 7"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(549, 101)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(36, 13)
        Me.Label31.TabIndex = 241
        Me.Label31.Text = "Line 6"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(441, 101)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(36, 13)
        Me.Label30.TabIndex = 240
        Me.Label30.Text = "Line 5"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(333, 101)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(36, 13)
        Me.Label29.TabIndex = 239
        Me.Label29.Text = "Line 4"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(225, 101)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(36, 13)
        Me.Label28.TabIndex = 238
        Me.Label28.Text = "Line 3"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(117, 101)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(36, 13)
        Me.Label27.TabIndex = 237
        Me.Label27.Text = "Line 2"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(9, 101)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(36, 13)
        Me.Label26.TabIndex = 236
        Me.Label26.Text = "Line 1"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(822, 129)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(31, 13)
        Me.Label25.TabIndex = 235
        Me.Label25.Text = "Tie 8"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(714, 129)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(31, 13)
        Me.Label24.TabIndex = 234
        Me.Label24.Text = "Tie 7"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(606, 129)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(31, 13)
        Me.Label23.TabIndex = 233
        Me.Label23.Text = "Tie 6"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(498, 129)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(31, 13)
        Me.Label22.TabIndex = 232
        Me.Label22.Text = "Tie 5"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(282, 129)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(31, 13)
        Me.Label20.TabIndex = 230
        Me.Label20.Text = "Tie 3"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(174, 129)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 13)
        Me.Label19.TabIndex = 229
        Me.Label19.Text = "Tie 2"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Location = New System.Drawing.Point(66, 129)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(31, 13)
        Me.Label18.TabIndex = 228
        Me.Label18.Text = "Tie 1"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Unit8TieBreaker
        '
        Me.Unit8TieBreaker.AccessibleName = "31"
        Me.Unit8TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit8TieBreaker.Image = CType(resources.GetObject("Unit8TieBreaker.Image"), System.Drawing.Image)
        Me.Unit8TieBreaker.Location = New System.Drawing.Point(800, 143)
        Me.Unit8TieBreaker.Name = "Unit8TieBreaker"
        Me.Unit8TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit8TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit8TieBreaker.TabIndex = 227
        Me.Unit8TieBreaker.TabStop = False
        Me.Unit8TieBreaker.Tag = "1"
        '
        'Unit15TieBreaker
        '
        Me.Unit15TieBreaker.AccessibleName = "46"
        Me.Unit15TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit15TieBreaker.Image = CType(resources.GetObject("Unit15TieBreaker.Image"), System.Drawing.Image)
        Me.Unit15TieBreaker.Location = New System.Drawing.Point(692, 321)
        Me.Unit15TieBreaker.Name = "Unit15TieBreaker"
        Me.Unit15TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit15TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit15TieBreaker.TabIndex = 226
        Me.Unit15TieBreaker.TabStop = False
        Me.Unit15TieBreaker.Tag = "1"
        '
        'Unit14TieBreaker
        '
        Me.Unit14TieBreaker.AccessibleName = "45"
        Me.Unit14TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit14TieBreaker.Image = CType(resources.GetObject("Unit14TieBreaker.Image"), System.Drawing.Image)
        Me.Unit14TieBreaker.Location = New System.Drawing.Point(584, 321)
        Me.Unit14TieBreaker.Name = "Unit14TieBreaker"
        Me.Unit14TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit14TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit14TieBreaker.TabIndex = 225
        Me.Unit14TieBreaker.TabStop = False
        Me.Unit14TieBreaker.Tag = "1"
        '
        'Unit13TieBreaker
        '
        Me.Unit13TieBreaker.AccessibleName = "37"
        Me.Unit13TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit13TieBreaker.Image = CType(resources.GetObject("Unit13TieBreaker.Image"), System.Drawing.Image)
        Me.Unit13TieBreaker.Location = New System.Drawing.Point(476, 321)
        Me.Unit13TieBreaker.Name = "Unit13TieBreaker"
        Me.Unit13TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit13TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit13TieBreaker.TabIndex = 224
        Me.Unit13TieBreaker.TabStop = False
        Me.Unit13TieBreaker.Tag = "1"
        '
        'Unit12TieBreaker
        '
        Me.Unit12TieBreaker.AccessibleName = "36"
        Me.Unit12TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit12TieBreaker.Image = CType(resources.GetObject("Unit12TieBreaker.Image"), System.Drawing.Image)
        Me.Unit12TieBreaker.Location = New System.Drawing.Point(368, 321)
        Me.Unit12TieBreaker.Name = "Unit12TieBreaker"
        Me.Unit12TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit12TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit12TieBreaker.TabIndex = 223
        Me.Unit12TieBreaker.TabStop = False
        Me.Unit12TieBreaker.Tag = "1"
        '
        'Unit11TieBreaker
        '
        Me.Unit11TieBreaker.AccessibleName = "35"
        Me.Unit11TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit11TieBreaker.Image = CType(resources.GetObject("Unit11TieBreaker.Image"), System.Drawing.Image)
        Me.Unit11TieBreaker.Location = New System.Drawing.Point(260, 321)
        Me.Unit11TieBreaker.Name = "Unit11TieBreaker"
        Me.Unit11TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit11TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit11TieBreaker.TabIndex = 222
        Me.Unit11TieBreaker.TabStop = False
        Me.Unit11TieBreaker.Tag = "1"
        '
        'Unit10TieBreaker
        '
        Me.Unit10TieBreaker.AccessibleName = "34"
        Me.Unit10TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit10TieBreaker.Image = CType(resources.GetObject("Unit10TieBreaker.Image"), System.Drawing.Image)
        Me.Unit10TieBreaker.Location = New System.Drawing.Point(152, 321)
        Me.Unit10TieBreaker.Name = "Unit10TieBreaker"
        Me.Unit10TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit10TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit10TieBreaker.TabIndex = 221
        Me.Unit10TieBreaker.TabStop = False
        Me.Unit10TieBreaker.Tag = "1"
        '
        'Unit9TieBreaker
        '
        Me.Unit9TieBreaker.AccessibleName = "33"
        Me.Unit9TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit9TieBreaker.Image = CType(resources.GetObject("Unit9TieBreaker.Image"), System.Drawing.Image)
        Me.Unit9TieBreaker.Location = New System.Drawing.Point(44, 321)
        Me.Unit9TieBreaker.Name = "Unit9TieBreaker"
        Me.Unit9TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit9TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit9TieBreaker.TabIndex = 220
        Me.Unit9TieBreaker.TabStop = False
        Me.Unit9TieBreaker.Tag = "1"
        '
        'Unit7TieBreaker
        '
        Me.Unit7TieBreaker.AccessibleName = "30"
        Me.Unit7TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit7TieBreaker.Image = CType(resources.GetObject("Unit7TieBreaker.Image"), System.Drawing.Image)
        Me.Unit7TieBreaker.Location = New System.Drawing.Point(692, 143)
        Me.Unit7TieBreaker.Name = "Unit7TieBreaker"
        Me.Unit7TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit7TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit7TieBreaker.TabIndex = 219
        Me.Unit7TieBreaker.TabStop = False
        Me.Unit7TieBreaker.Tag = "1"
        '
        'Unit6TieBreaker
        '
        Me.Unit6TieBreaker.AccessibleName = "29"
        Me.Unit6TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit6TieBreaker.Image = CType(resources.GetObject("Unit6TieBreaker.Image"), System.Drawing.Image)
        Me.Unit6TieBreaker.Location = New System.Drawing.Point(584, 143)
        Me.Unit6TieBreaker.Name = "Unit6TieBreaker"
        Me.Unit6TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit6TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit6TieBreaker.TabIndex = 218
        Me.Unit6TieBreaker.TabStop = False
        Me.Unit6TieBreaker.Tag = "1"
        '
        'Unit5TieBreaker
        '
        Me.Unit5TieBreaker.AccessibleName = "28"
        Me.Unit5TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit5TieBreaker.Image = CType(resources.GetObject("Unit5TieBreaker.Image"), System.Drawing.Image)
        Me.Unit5TieBreaker.Location = New System.Drawing.Point(476, 143)
        Me.Unit5TieBreaker.Name = "Unit5TieBreaker"
        Me.Unit5TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit5TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit5TieBreaker.TabIndex = 217
        Me.Unit5TieBreaker.TabStop = False
        Me.Unit5TieBreaker.Tag = "1"
        '
        'Unit3TieBreaker
        '
        Me.Unit3TieBreaker.AccessibleName = "26"
        Me.Unit3TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit3TieBreaker.Image = CType(resources.GetObject("Unit3TieBreaker.Image"), System.Drawing.Image)
        Me.Unit3TieBreaker.Location = New System.Drawing.Point(260, 143)
        Me.Unit3TieBreaker.Name = "Unit3TieBreaker"
        Me.Unit3TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit3TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit3TieBreaker.TabIndex = 215
        Me.Unit3TieBreaker.TabStop = False
        Me.Unit3TieBreaker.Tag = "1"
        '
        'Unit2TieBreaker
        '
        Me.Unit2TieBreaker.AccessibleName = "25"
        Me.Unit2TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit2TieBreaker.Image = CType(resources.GetObject("Unit2TieBreaker.Image"), System.Drawing.Image)
        Me.Unit2TieBreaker.InitialImage = CType(resources.GetObject("Unit2TieBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit2TieBreaker.Location = New System.Drawing.Point(152, 143)
        Me.Unit2TieBreaker.Name = "Unit2TieBreaker"
        Me.Unit2TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit2TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit2TieBreaker.TabIndex = 214
        Me.Unit2TieBreaker.TabStop = False
        Me.Unit2TieBreaker.Tag = "1"
        '
        'out16
        '
        Me.out16.AutoSize = True
        Me.out16.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out16.Location = New System.Drawing.Point(772, 207)
        Me.out16.Name = "out16"
        Me.out16.Size = New System.Drawing.Size(21, 15)
        Me.out16.TabIndex = 213
        Me.out16.Tag = "16"
        Me.out16.Text = "16"
        Me.out16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out15
        '
        Me.out15.AutoSize = True
        Me.out15.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out15.Location = New System.Drawing.Point(665, 207)
        Me.out15.Name = "out15"
        Me.out15.Size = New System.Drawing.Size(21, 15)
        Me.out15.TabIndex = 212
        Me.out15.Tag = "15"
        Me.out15.Text = "15"
        Me.out15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out14
        '
        Me.out14.AutoSize = True
        Me.out14.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out14.Location = New System.Drawing.Point(557, 207)
        Me.out14.Name = "out14"
        Me.out14.Size = New System.Drawing.Size(21, 15)
        Me.out14.TabIndex = 211
        Me.out14.Tag = "14"
        Me.out14.Text = "14"
        Me.out14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out13
        '
        Me.out13.AutoSize = True
        Me.out13.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out13.Location = New System.Drawing.Point(448, 207)
        Me.out13.Name = "out13"
        Me.out13.Size = New System.Drawing.Size(21, 15)
        Me.out13.TabIndex = 210
        Me.out13.Tag = "13"
        Me.out13.Text = "13"
        Me.out13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out12
        '
        Me.out12.AutoSize = True
        Me.out12.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out12.Location = New System.Drawing.Point(340, 207)
        Me.out12.Name = "out12"
        Me.out12.Size = New System.Drawing.Size(21, 15)
        Me.out12.TabIndex = 209
        Me.out12.Tag = "12"
        Me.out12.Text = "12"
        Me.out12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out11
        '
        Me.out11.AutoSize = True
        Me.out11.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out11.Location = New System.Drawing.Point(233, 207)
        Me.out11.Name = "out11"
        Me.out11.Size = New System.Drawing.Size(21, 15)
        Me.out11.TabIndex = 208
        Me.out11.Tag = "11"
        Me.out11.Text = "11"
        Me.out11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out10
        '
        Me.out10.AutoSize = True
        Me.out10.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out10.Location = New System.Drawing.Point(124, 207)
        Me.out10.Name = "out10"
        Me.out10.Size = New System.Drawing.Size(21, 15)
        Me.out10.TabIndex = 207
        Me.out10.Tag = "10"
        Me.out10.Text = "10"
        Me.out10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out9
        '
        Me.out9.AutoSize = True
        Me.out9.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out9.Location = New System.Drawing.Point(20, 207)
        Me.out9.Name = "out9"
        Me.out9.Size = New System.Drawing.Size(14, 15)
        Me.out9.TabIndex = 206
        Me.out9.Tag = "9"
        Me.out9.Text = "9"
        Me.out9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out8
        '
        Me.out8.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out8.Location = New System.Drawing.Point(776, 28)
        Me.out8.Name = "out8"
        Me.out8.Size = New System.Drawing.Size(13, 13)
        Me.out8.TabIndex = 205
        Me.out8.Tag = "8"
        Me.out8.Text = "8"
        Me.out8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out7
        '
        Me.out7.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out7.Location = New System.Drawing.Point(669, 28)
        Me.out7.Name = "out7"
        Me.out7.Size = New System.Drawing.Size(13, 13)
        Me.out7.TabIndex = 204
        Me.out7.Tag = "7"
        Me.out7.Text = "7"
        Me.out7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out6
        '
        Me.out6.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out6.Location = New System.Drawing.Point(561, 28)
        Me.out6.Name = "out6"
        Me.out6.Size = New System.Drawing.Size(13, 13)
        Me.out6.TabIndex = 203
        Me.out6.Tag = "6"
        Me.out6.Text = "6"
        Me.out6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out5
        '
        Me.out5.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out5.Location = New System.Drawing.Point(452, 28)
        Me.out5.Name = "out5"
        Me.out5.Size = New System.Drawing.Size(13, 13)
        Me.out5.TabIndex = 202
        Me.out5.Tag = "5"
        Me.out5.Text = "5"
        Me.out5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out4
        '
        Me.out4.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out4.Location = New System.Drawing.Point(344, 28)
        Me.out4.Name = "out4"
        Me.out4.Size = New System.Drawing.Size(13, 13)
        Me.out4.TabIndex = 201
        Me.out4.Tag = "4"
        Me.out4.Text = "4"
        Me.out4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out3
        '
        Me.out3.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out3.Location = New System.Drawing.Point(237, 28)
        Me.out3.Name = "out3"
        Me.out3.Size = New System.Drawing.Size(13, 13)
        Me.out3.TabIndex = 200
        Me.out3.Tag = "3"
        Me.out3.Text = "3"
        Me.out3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out2
        '
        Me.out2.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out2.Location = New System.Drawing.Point(128, 28)
        Me.out2.Name = "out2"
        Me.out2.Size = New System.Drawing.Size(13, 13)
        Me.out2.TabIndex = 199
        Me.out2.Tag = "2"
        Me.out2.Text = "2"
        Me.out2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'out1
        '
        Me.out1.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.out1.Location = New System.Drawing.Point(20, 28)
        Me.out1.Name = "out1"
        Me.out1.Size = New System.Drawing.Size(13, 13)
        Me.out1.TabIndex = 198
        Me.out1.Tag = "1"
        Me.out1.Text = "1"
        Me.out1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Unit1TieBreaker
        '
        Me.Unit1TieBreaker.AccessibleName = "24"
        Me.Unit1TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit1TieBreaker.Image = CType(resources.GetObject("Unit1TieBreaker.Image"), System.Drawing.Image)
        Me.Unit1TieBreaker.InitialImage = CType(resources.GetObject("Unit1TieBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit1TieBreaker.Location = New System.Drawing.Point(44, 143)
        Me.Unit1TieBreaker.Name = "Unit1TieBreaker"
        Me.Unit1TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit1TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit1TieBreaker.TabIndex = 197
        Me.Unit1TieBreaker.TabStop = False
        Me.Unit1TieBreaker.Tag = "1"
        '
        'Unit16LineBreaker
        '
        Me.Unit16LineBreaker.AccessibleName = "23"
        Me.Unit16LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit16LineBreaker.Image = CType(resources.GetObject("Unit16LineBreaker.Image"), System.Drawing.Image)
        Me.Unit16LineBreaker.Location = New System.Drawing.Point(766, 295)
        Me.Unit16LineBreaker.Name = "Unit16LineBreaker"
        Me.Unit16LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit16LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit16LineBreaker.TabIndex = 196
        Me.Unit16LineBreaker.TabStop = False
        Me.Unit16LineBreaker.Tag = "1"
        '
        'Unit15LineBreaker
        '
        Me.Unit15LineBreaker.AccessibleName = "22"
        Me.Unit15LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit15LineBreaker.Image = CType(resources.GetObject("Unit15LineBreaker.Image"), System.Drawing.Image)
        Me.Unit15LineBreaker.Location = New System.Drawing.Point(658, 295)
        Me.Unit15LineBreaker.Name = "Unit15LineBreaker"
        Me.Unit15LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit15LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit15LineBreaker.TabIndex = 195
        Me.Unit15LineBreaker.TabStop = False
        Me.Unit15LineBreaker.Tag = "1"
        '
        'Unit14LineBreaker
        '
        Me.Unit14LineBreaker.AccessibleName = "21"
        Me.Unit14LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit14LineBreaker.Image = CType(resources.GetObject("Unit14LineBreaker.Image"), System.Drawing.Image)
        Me.Unit14LineBreaker.Location = New System.Drawing.Point(550, 295)
        Me.Unit14LineBreaker.Name = "Unit14LineBreaker"
        Me.Unit14LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit14LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit14LineBreaker.TabIndex = 194
        Me.Unit14LineBreaker.TabStop = False
        Me.Unit14LineBreaker.Tag = "1"
        '
        'Unit13LineBreaker
        '
        Me.Unit13LineBreaker.AccessibleName = "13"
        Me.Unit13LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit13LineBreaker.Image = CType(resources.GetObject("Unit13LineBreaker.Image"), System.Drawing.Image)
        Me.Unit13LineBreaker.Location = New System.Drawing.Point(442, 295)
        Me.Unit13LineBreaker.Name = "Unit13LineBreaker"
        Me.Unit13LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit13LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit13LineBreaker.TabIndex = 193
        Me.Unit13LineBreaker.TabStop = False
        Me.Unit13LineBreaker.Tag = "1"
        '
        'Unit12LineBreaker
        '
        Me.Unit12LineBreaker.AccessibleName = "12"
        Me.Unit12LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit12LineBreaker.Image = CType(resources.GetObject("Unit12LineBreaker.Image"), System.Drawing.Image)
        Me.Unit12LineBreaker.Location = New System.Drawing.Point(334, 295)
        Me.Unit12LineBreaker.Name = "Unit12LineBreaker"
        Me.Unit12LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit12LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit12LineBreaker.TabIndex = 192
        Me.Unit12LineBreaker.TabStop = False
        Me.Unit12LineBreaker.Tag = "1"
        '
        'Unit11LineBreaker
        '
        Me.Unit11LineBreaker.AccessibleName = "11"
        Me.Unit11LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit11LineBreaker.Image = CType(resources.GetObject("Unit11LineBreaker.Image"), System.Drawing.Image)
        Me.Unit11LineBreaker.Location = New System.Drawing.Point(226, 295)
        Me.Unit11LineBreaker.Name = "Unit11LineBreaker"
        Me.Unit11LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit11LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit11LineBreaker.TabIndex = 191
        Me.Unit11LineBreaker.TabStop = False
        Me.Unit11LineBreaker.Tag = "1"
        '
        'Unit10LineBreaker
        '
        Me.Unit10LineBreaker.AccessibleName = "10"
        Me.Unit10LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit10LineBreaker.Image = CType(resources.GetObject("Unit10LineBreaker.Image"), System.Drawing.Image)
        Me.Unit10LineBreaker.Location = New System.Drawing.Point(118, 295)
        Me.Unit10LineBreaker.Name = "Unit10LineBreaker"
        Me.Unit10LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit10LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit10LineBreaker.TabIndex = 190
        Me.Unit10LineBreaker.TabStop = False
        Me.Unit10LineBreaker.Tag = "1"
        '
        'Unit9LineBreaker
        '
        Me.Unit9LineBreaker.AccessibleName = "9"
        Me.Unit9LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit9LineBreaker.Image = CType(resources.GetObject("Unit9LineBreaker.Image"), System.Drawing.Image)
        Me.Unit9LineBreaker.Location = New System.Drawing.Point(10, 295)
        Me.Unit9LineBreaker.Name = "Unit9LineBreaker"
        Me.Unit9LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit9LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit9LineBreaker.TabIndex = 189
        Me.Unit9LineBreaker.TabStop = False
        Me.Unit9LineBreaker.Tag = "1"
        '
        'Unit8LineBreaker
        '
        Me.Unit8LineBreaker.AccessibleName = "7"
        Me.Unit8LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit8LineBreaker.Image = CType(resources.GetObject("Unit8LineBreaker.Image"), System.Drawing.Image)
        Me.Unit8LineBreaker.InitialImage = CType(resources.GetObject("Unit8LineBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit8LineBreaker.Location = New System.Drawing.Point(766, 117)
        Me.Unit8LineBreaker.Name = "Unit8LineBreaker"
        Me.Unit8LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit8LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit8LineBreaker.TabIndex = 188
        Me.Unit8LineBreaker.TabStop = False
        Me.Unit8LineBreaker.Tag = "1"
        '
        'Unit7LineBreaker
        '
        Me.Unit7LineBreaker.AccessibleName = "6"
        Me.Unit7LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit7LineBreaker.Image = CType(resources.GetObject("Unit7LineBreaker.Image"), System.Drawing.Image)
        Me.Unit7LineBreaker.InitialImage = CType(resources.GetObject("Unit7LineBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit7LineBreaker.Location = New System.Drawing.Point(658, 117)
        Me.Unit7LineBreaker.Name = "Unit7LineBreaker"
        Me.Unit7LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit7LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit7LineBreaker.TabIndex = 187
        Me.Unit7LineBreaker.TabStop = False
        Me.Unit7LineBreaker.Tag = "1"
        '
        'Unit6LineBreaker
        '
        Me.Unit6LineBreaker.AccessibleName = "5"
        Me.Unit6LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit6LineBreaker.Image = CType(resources.GetObject("Unit6LineBreaker.Image"), System.Drawing.Image)
        Me.Unit6LineBreaker.InitialImage = CType(resources.GetObject("Unit6LineBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit6LineBreaker.Location = New System.Drawing.Point(550, 117)
        Me.Unit6LineBreaker.Name = "Unit6LineBreaker"
        Me.Unit6LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit6LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit6LineBreaker.TabIndex = 186
        Me.Unit6LineBreaker.TabStop = False
        Me.Unit6LineBreaker.Tag = "1"
        '
        'Unit5LineBreaker
        '
        Me.Unit5LineBreaker.AccessibleName = "4"
        Me.Unit5LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit5LineBreaker.Image = CType(resources.GetObject("Unit5LineBreaker.Image"), System.Drawing.Image)
        Me.Unit5LineBreaker.InitialImage = CType(resources.GetObject("Unit5LineBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit5LineBreaker.Location = New System.Drawing.Point(442, 117)
        Me.Unit5LineBreaker.Name = "Unit5LineBreaker"
        Me.Unit5LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit5LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit5LineBreaker.TabIndex = 185
        Me.Unit5LineBreaker.TabStop = False
        Me.Unit5LineBreaker.Tag = "1"
        '
        'Unit4LineBreaker
        '
        Me.Unit4LineBreaker.AccessibleName = "3"
        Me.Unit4LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit4LineBreaker.Image = CType(resources.GetObject("Unit4LineBreaker.Image"), System.Drawing.Image)
        Me.Unit4LineBreaker.InitialImage = CType(resources.GetObject("Unit4LineBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit4LineBreaker.Location = New System.Drawing.Point(334, 117)
        Me.Unit4LineBreaker.Name = "Unit4LineBreaker"
        Me.Unit4LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit4LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit4LineBreaker.TabIndex = 184
        Me.Unit4LineBreaker.TabStop = False
        Me.Unit4LineBreaker.Tag = "1"
        '
        'Unit3LineBreaker
        '
        Me.Unit3LineBreaker.AccessibleName = "2"
        Me.Unit3LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit3LineBreaker.Image = CType(resources.GetObject("Unit3LineBreaker.Image"), System.Drawing.Image)
        Me.Unit3LineBreaker.InitialImage = CType(resources.GetObject("Unit3LineBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit3LineBreaker.Location = New System.Drawing.Point(226, 117)
        Me.Unit3LineBreaker.Name = "Unit3LineBreaker"
        Me.Unit3LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit3LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit3LineBreaker.TabIndex = 183
        Me.Unit3LineBreaker.TabStop = False
        Me.Unit3LineBreaker.Tag = "1"
        '
        'Unit2LineBreaker
        '
        Me.Unit2LineBreaker.AccessibleName = "1"
        Me.Unit2LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit2LineBreaker.Image = CType(resources.GetObject("Unit2LineBreaker.Image"), System.Drawing.Image)
        Me.Unit2LineBreaker.InitialImage = CType(resources.GetObject("Unit2LineBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit2LineBreaker.Location = New System.Drawing.Point(118, 117)
        Me.Unit2LineBreaker.Name = "Unit2LineBreaker"
        Me.Unit2LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit2LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit2LineBreaker.TabIndex = 182
        Me.Unit2LineBreaker.TabStop = False
        Me.Unit2LineBreaker.Tag = "1"
        '
        'Unit1LineBreaker
        '
        Me.Unit1LineBreaker.AccessibleName = "0"
        Me.Unit1LineBreaker.BackColor = System.Drawing.SystemColors.Control
        Me.Unit1LineBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit1LineBreaker.Image = CType(resources.GetObject("Unit1LineBreaker.Image"), System.Drawing.Image)
        Me.Unit1LineBreaker.InitialImage = CType(resources.GetObject("Unit1LineBreaker.InitialImage"), System.Drawing.Image)
        Me.Unit1LineBreaker.Location = New System.Drawing.Point(10, 117)
        Me.Unit1LineBreaker.Name = "Unit1LineBreaker"
        Me.Unit1LineBreaker.Size = New System.Drawing.Size(35, 60)
        Me.Unit1LineBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit1LineBreaker.TabIndex = 181
        Me.Unit1LineBreaker.TabStop = False
        Me.Unit1LineBreaker.Tag = "1"
        '
        'PictureBox6
        '
        Me.PictureBox6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox6.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        Me.PictureBox6.InitialImage = CType(resources.GetObject("PictureBox6.InitialImage"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(118, 44)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(35, 57)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 170
        Me.PictureBox6.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = Global.ExtIO.My.Resources.Resources.M_2001d_sml
        Me.PictureBox1.InitialImage = CType(resources.GetObject("PictureBox1.InitialImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(10, 44)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(35, 57)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 165
        Me.PictureBox1.TabStop = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(390, 129)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(31, 13)
        Me.Label21.TabIndex = 231
        Me.Label21.Tag = "0"
        Me.Label21.Text = "Tie 4"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Unit4TieBreaker
        '
        Me.Unit4TieBreaker.AccessibleName = "27"
        Me.Unit4TieBreaker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Unit4TieBreaker.Image = CType(resources.GetObject("Unit4TieBreaker.Image"), System.Drawing.Image)
        Me.Unit4TieBreaker.Location = New System.Drawing.Point(368, 143)
        Me.Unit4TieBreaker.Name = "Unit4TieBreaker"
        Me.Unit4TieBreaker.Size = New System.Drawing.Size(75, 34)
        Me.Unit4TieBreaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Unit4TieBreaker.TabIndex = 216
        Me.Unit4TieBreaker.TabStop = False
        Me.Unit4TieBreaker.Tag = "1"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConnectionStatusLabel, Me.UdpProgressBar})
        Me.StatusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 508)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StatusStrip1.Size = New System.Drawing.Size(897, 21)
        Me.StatusStrip1.TabIndex = 363
        '
        'ConnectionStatusLabel
        '
        Me.ConnectionStatusLabel.AutoSize = False
        Me.ConnectionStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Bump
        Me.ConnectionStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ConnectionStatusLabel.Name = "ConnectionStatusLabel"
        Me.ConnectionStatusLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ConnectionStatusLabel.Size = New System.Drawing.Size(100, 16)
        Me.ConnectionStatusLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay
        '
        'UdpProgressBar
        '
        Me.UdpProgressBar.Name = "UdpProgressBar"
        Me.UdpProgressBar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.UdpProgressBar.Size = New System.Drawing.Size(100, 15)
        '
        'Label119
        '
        Me.Label119.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label119.AutoSize = True
        Me.Label119.Location = New System.Drawing.Point(6, 510)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(0, 13)
        Me.Label119.TabIndex = 166
        Me.Label119.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(897, 28)
        Me.ToolStripContainer1.LeftToolStripPanelVisible = False
        Me.ToolStripContainer1.Location = New System.Drawing.Point(0, 527)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.RightToolStripPanelVisible = False
        Me.ToolStripContainer1.Size = New System.Drawing.Size(897, 28)
        Me.ToolStripContainer1.TabIndex = 167
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        Me.ToolStripContainer1.TopToolStripPanelVisible = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip1.Size = New System.Drawing.Size(897, 24)
        Me.MenuStrip1.TabIndex = 131
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(31, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(85, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ContentsToolStripMenuItem, Me.toolStripSeparator5, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.HelpToolStripMenuItem.Text = "&Help"
        '
        'ContentsToolStripMenuItem
        '
        Me.ContentsToolStripMenuItem.Name = "ContentsToolStripMenuItem"
        Me.ContentsToolStripMenuItem.Size = New System.Drawing.Size(109, 22)
        Me.ContentsToolStripMenuItem.Text = "&Contents"
        '
        'toolStripSeparator5
        '
        Me.toolStripSeparator5.Name = "toolStripSeparator5"
        Me.toolStripSeparator5.Size = New System.Drawing.Size(106, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(109, 22)
        Me.AboutToolStripMenuItem.Text = "&About..."
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(897, 529)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Controls.Add(Me.Label119)
        Me.Controls.Add(Me.MainBlock)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "Extented IO Control"
        CType(PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.SerialPanel.ResumeLayout(False)
        Me.NetworkPanel.ResumeLayout(False)
        Me.SettingsBlock.ResumeLayout(False)
        Me.SettingsBlock.PerformLayout()
        CType(Me.UdpRefreshRate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LedBlock.ResumeLayout(False)
        Me.LedBlock.PerformLayout()
        Me.grpAllBreakers.ResumeLayout(False)
        Me.grpAllBreakers.PerformLayout()
        CType(Me.Led31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainBlock.ResumeLayout(False)
        Me.MainBlock.PerformLayout()
        CType(Me.counter16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.counter1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.raise1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lower1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.alarm1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit8TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit15TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit14TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit13TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit12TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit11TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit10TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit9TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit7TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit6TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit5TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit3TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit2TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit1TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit16LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit15LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit14LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit13LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit12LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit11LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit10LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit9LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit8LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit7LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit6LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit5LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit4LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit3LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit2LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit1LineBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Unit4TieBreaker, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonConnect As System.Windows.Forms.Button
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Led1 As System.Windows.Forms.PictureBox
    Friend WithEvents Led31 As System.Windows.Forms.PictureBox
    Friend WithEvents Led30 As System.Windows.Forms.PictureBox
    Friend WithEvents Led29 As System.Windows.Forms.PictureBox
    Friend WithEvents Led28 As System.Windows.Forms.PictureBox
    Friend WithEvents Led27 As System.Windows.Forms.PictureBox
    Friend WithEvents Led26 As System.Windows.Forms.PictureBox
    Friend WithEvents Led25 As System.Windows.Forms.PictureBox
    Friend WithEvents Led24 As System.Windows.Forms.PictureBox
    Friend WithEvents Led23 As System.Windows.Forms.PictureBox
    Friend WithEvents Led22 As System.Windows.Forms.PictureBox
    Friend WithEvents Led21 As System.Windows.Forms.PictureBox
    Friend WithEvents Led20 As System.Windows.Forms.PictureBox
    Friend WithEvents Led19 As System.Windows.Forms.PictureBox
    Friend WithEvents Led18 As System.Windows.Forms.PictureBox
    Friend WithEvents Led17 As System.Windows.Forms.PictureBox
    Friend WithEvents Led16 As System.Windows.Forms.PictureBox
    Friend WithEvents Led15 As System.Windows.Forms.PictureBox
    Friend WithEvents Led14 As System.Windows.Forms.PictureBox
    Friend WithEvents Led13 As System.Windows.Forms.PictureBox
    Friend WithEvents Led12 As System.Windows.Forms.PictureBox
    Friend WithEvents Led11 As System.Windows.Forms.PictureBox
    Friend WithEvents Led10 As System.Windows.Forms.PictureBox
    Friend WithEvents Led9 As System.Windows.Forms.PictureBox
    Friend WithEvents Led8 As System.Windows.Forms.PictureBox
    Friend WithEvents Led7 As System.Windows.Forms.PictureBox
    Friend WithEvents Led6 As System.Windows.Forms.PictureBox
    Friend WithEvents Led5 As System.Windows.Forms.PictureBox
    Friend WithEvents Led4 As System.Windows.Forms.PictureBox
    Friend WithEvents Led3 As System.Windows.Forms.PictureBox
    Friend WithEvents Led2 As System.Windows.Forms.PictureBox
    Friend WithEvents LedBlock As System.Windows.Forms.Panel
    Friend WithEvents button_all_off As System.Windows.Forms.RadioButton
    Friend WithEvents button_all_on As System.Windows.Forms.RadioButton
    Friend WithEvents DelayAll As System.Windows.Forms.CheckBox
    'Friend WithEvents ConnectionStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Public WithEvents NetworkDevices As System.Windows.Forms.Button
    'Friend WithEvents UdpProgressBar As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents UdpRefreshRate As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label117 As System.Windows.Forms.Label
    Friend WithEvents MainBlock As System.Windows.Forms.Panel
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown15 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown14 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown13 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown12 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown11 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown10 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown9 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown8 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown7 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown6 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown5 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown0 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Unit8TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit15TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit14TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit13TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit12TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit11TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit10TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit9TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit7TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit6TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit5TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit4TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit3TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit2TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents out16 As System.Windows.Forms.Label
    Friend WithEvents out15 As System.Windows.Forms.Label
    Friend WithEvents out14 As System.Windows.Forms.Label
    Friend WithEvents out13 As System.Windows.Forms.Label
    Friend WithEvents out12 As System.Windows.Forms.Label
    Friend WithEvents out11 As System.Windows.Forms.Label
    Friend WithEvents out10 As System.Windows.Forms.Label
    Friend WithEvents out9 As System.Windows.Forms.Label
    Friend WithEvents out8 As System.Windows.Forms.Label
    Friend WithEvents out7 As System.Windows.Forms.Label
    Friend WithEvents out6 As System.Windows.Forms.Label
    Friend WithEvents out5 As System.Windows.Forms.Label
    Friend WithEvents out4 As System.Windows.Forms.Label
    Friend WithEvents out3 As System.Windows.Forms.Label
    Friend WithEvents out2 As System.Windows.Forms.Label
    Friend WithEvents out1 As System.Windows.Forms.Label
    Friend WithEvents Unit1TieBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit16LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit15LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit14LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit13LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit12LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit11LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit10LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit9LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit8LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit7LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit6LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit5LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit4LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit3LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit2LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents Unit1LineBreaker As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label119 As System.Windows.Forms.Label
    Friend WithEvents alarm16 As System.Windows.Forms.PictureBox
    Friend WithEvents lower16 As System.Windows.Forms.PictureBox
    Friend WithEvents raise16 As System.Windows.Forms.PictureBox
    Friend WithEvents SettingsBlock As System.Windows.Forms.Panel
    Friend WithEvents NetworkPanel As System.Windows.Forms.Panel
    Friend WithEvents SerialPanel As System.Windows.Forms.Panel
    Friend WithEvents ConnectAdapter As System.Windows.Forms.Button
    Friend WithEvents NetworkAdaptersAvailable As System.Windows.Forms.ComboBox
    Friend WithEvents raise14 As System.Windows.Forms.PictureBox
    Friend WithEvents lower14 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm14 As System.Windows.Forms.PictureBox
    Friend WithEvents raise15 As System.Windows.Forms.PictureBox
    Friend WithEvents lower15 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm15 As System.Windows.Forms.PictureBox
    Friend WithEvents raise8 As System.Windows.Forms.PictureBox
    Friend WithEvents lower8 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm8 As System.Windows.Forms.PictureBox
    Friend WithEvents raise7 As System.Windows.Forms.PictureBox
    Friend WithEvents lower7 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm7 As System.Windows.Forms.PictureBox
    Friend WithEvents raise6 As System.Windows.Forms.PictureBox
    Friend WithEvents lower6 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm6 As System.Windows.Forms.PictureBox
    Friend WithEvents raise5 As System.Windows.Forms.PictureBox
    Friend WithEvents lower5 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm5 As System.Windows.Forms.PictureBox
    Friend WithEvents raise4 As System.Windows.Forms.PictureBox
    Friend WithEvents lower4 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm4 As System.Windows.Forms.PictureBox
    Friend WithEvents raise3 As System.Windows.Forms.PictureBox
    Friend WithEvents lower3 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm3 As System.Windows.Forms.PictureBox
    Friend WithEvents raise2 As System.Windows.Forms.PictureBox
    Friend WithEvents lower2 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm2 As System.Windows.Forms.PictureBox
    Friend WithEvents raise1 As System.Windows.Forms.PictureBox
    Friend WithEvents lower1 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm1 As System.Windows.Forms.PictureBox
    Friend WithEvents raise9 As System.Windows.Forms.PictureBox
    Friend WithEvents lower9 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm9 As System.Windows.Forms.PictureBox
    Friend WithEvents raise10 As System.Windows.Forms.PictureBox
    Friend WithEvents lower10 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm10 As System.Windows.Forms.PictureBox
    Friend WithEvents raise11 As System.Windows.Forms.PictureBox
    Friend WithEvents lower11 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm11 As System.Windows.Forms.PictureBox
    Friend WithEvents raise12 As System.Windows.Forms.PictureBox
    Friend WithEvents lower12 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm12 As System.Windows.Forms.PictureBox
    Friend WithEvents raise13 As System.Windows.Forms.PictureBox
    Friend WithEvents lower13 As System.Windows.Forms.PictureBox
    Friend WithEvents alarm13 As System.Windows.Forms.PictureBox
    Friend WithEvents lt16 As System.Windows.Forms.Label
    Friend WithEvents lt15 As System.Windows.Forms.Label
    Friend WithEvents lt14 As System.Windows.Forms.Label
    Friend WithEvents lt13 As System.Windows.Forms.Label
    Friend WithEvents lt12 As System.Windows.Forms.Label
    Friend WithEvents lt11 As System.Windows.Forms.Label
    Friend WithEvents lt10 As System.Windows.Forms.Label
    Friend WithEvents lt9 As System.Windows.Forms.Label
    Friend WithEvents lt8 As System.Windows.Forms.Label
    Friend WithEvents lt7 As System.Windows.Forms.Label
    Friend WithEvents lt6 As System.Windows.Forms.Label
    Friend WithEvents lt5 As System.Windows.Forms.Label
    Friend WithEvents lt4 As System.Windows.Forms.Label
    Friend WithEvents lt3 As System.Windows.Forms.Label
    Friend WithEvents lt2 As System.Windows.Forms.Label
    Friend WithEvents lt1 As System.Windows.Forms.Label
    Friend WithEvents AlarmReset As System.Windows.Forms.CheckBox
    Friend WithEvents grpAllBreakers As System.Windows.Forms.GroupBox
    Friend WithEvents CommBlock As System.Windows.Forms.RadioButton
    Friend WithEvents AutoControl As System.Windows.Forms.RadioButton
    Friend WithEvents counter1 As System.Windows.Forms.PictureBox
    Friend WithEvents counter11 As System.Windows.Forms.PictureBox
    Friend WithEvents counter10 As System.Windows.Forms.PictureBox
    Friend WithEvents counter9 As System.Windows.Forms.PictureBox
    Friend WithEvents counter8 As System.Windows.Forms.PictureBox
    Friend WithEvents counter7 As System.Windows.Forms.PictureBox
    Friend WithEvents counter6 As System.Windows.Forms.PictureBox
    Friend WithEvents counter5 As System.Windows.Forms.PictureBox
    Friend WithEvents counter4 As System.Windows.Forms.PictureBox
    Friend WithEvents counter3 As System.Windows.Forms.PictureBox
    Friend WithEvents counter2 As System.Windows.Forms.PictureBox
    Friend WithEvents counter12 As System.Windows.Forms.PictureBox
    Friend WithEvents counter16 As System.Windows.Forms.PictureBox
    Friend WithEvents counter15 As System.Windows.Forms.PictureBox
    Friend WithEvents counter14 As System.Windows.Forms.PictureBox
    Friend WithEvents counter13 As System.Windows.Forms.PictureBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ConnectionStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents UdpProgressBar As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
