using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using Utility;
using DeviceDataSet;
using System.IO;
using BasicControls;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Collections;



namespace S2001D//DiscoveryDevice
{

    public partial class MasterFollower : Form
    {
        CAutoSize autoSize;

        CMFBroadcast m_Broadcast;
        CMFDeviceCollection m_Devices;
        int CurSelItem =0;
        int itemcount = 0;
        uint num_Port = 55000;
        private ListViewColumnSorter lvwColumnSorter;
        private static SortOrder toggle = SortOrder.Ascending;
        private ArrayList NicAddress = new ArrayList();
        public MasterFollower()
        {
            InitializeComponent();
            // Create an instance of a ListView column sorter and assign it 
            // to the ListView control.
            lvwColumnSorter = new ListViewColumnSorter();
            this.listDevice.ListViewItemSorter = lvwColumnSorter;
            autoSize = new CAutoSize(this);
        }


        private void Form1_Load(object sender, EventArgs e)
        {

            //NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            //foreach (NetworkInterface n in adapters)
            //{
            //    richTextBox1.Text = n.Name +"\n"+ n.OperationalStatus +"\n"+ n.Id;
            //}

            btnAlarmReset.Enabled = btnApply.Enabled =  false; 
            m_Devices = new CMFDeviceCollection();
            m_Broadcast = new CMFBroadcast(listDevice);
            btnApply.Enabled = false;
            m_Broadcast.OnFindMFDevice += OnFindMFDevice;           
            listDevice.Associate(8, ParllelAddr);
            listDevice.Associate(9, listBoxMode);       
            listDevice.Associate(10, lbTDiff);    
            listDevice.Associate(11, exUpDownMTO);
            listDevice.Associate(12, lbFlRs);
            listDevice.Associate(13, listBoxUse);
            listDevice.Associate(14, listBoxPolar);
            listDevice.Associate(15, listBoxUse);
            listDevice.Associate(16, listBoxPolar);
            listDevice.Associate(17, listBoxUse);
            listDevice.Associate(18, listBoxPolar);
            //Populate network interface card combo box
            
            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface n in interfaces)
            {
                
                IPInterfaceProperties p = n.GetIPProperties();

                foreach (IPAddressInformation unicast in p.UnicastAddresses)
                {
                    if (unicast.Address.AddressFamily == AddressFamily.InterNetwork && !n.Description.ToLower().Contains("loopback"))
                    {
                        cbNic.Items.Add(n.Description + " --" +unicast.Address.ToString());
                        NicAddress.Add(unicast.Address); 
                    }
                        
                }

            }
            if (cbNic.Items.Count >=1) cbNic.SelectedIndex = 0;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (cbNic.SelectedIndex == -1)
            {
                MessageBox.Show("No Network Interface Selected.", "Error");
                return;
            }
            try
            {
                //m_Broadcast.Send(ipAddress.Address, (uint)num_Port.Value);
                listDevice.BeginUpdate();
                listDevice.Items.Clear();                
                listDevice.EndUpdate();
                m_Devices.Clear();
                this.btnDiscover.Enabled = false;
                timer1.Start();
                m_Broadcast.Send((System.Net.IPAddress)NicAddress[cbNic.SelectedIndex], num_Port);
            }
            catch { };

        }

        private void OnFindMFDevice(CMFDevice device)
        {
            if (device != null)
            {
                string LOCK = "";
                listDevice.BeginUpdate();
                if (m_Devices[device.IPAddress] == null)
                {
                    m_Devices.Add(device.IPAddress, device);
                    ListViewItem item = listDevice.Items.Add(device.SN.ToString());
                    item.UseItemStyleForSubItems = false;

                    item.SubItems.Add(device.IPAddress);
                    item.SubItems.Add(device.UserLine);
                    if ((device.status & 0x80) == 0x80)
                    {
                        LOCK = "-Lock";
                        device.status = (short)(device.status - 0x0080);
                    }
                    item.SubItems.Add(device.status == 0 ? "N"+LOCK : (device.status == 1 ? "F"+LOCK: "M"+LOCK));
                    item.SubItems.Add(device.TapPos.ToString());
                    item.SubItems.Add(((device.AuxStatus & 1) == 1) ? "1" : "0");
                    item.SubItems.Add(((device.AuxStatus & 2) == 2) ? "1" : "0");
                    item.SubItems.Add(((device.AuxStatus & 4) == 4) ? "1" : "0");
                    item.SubItems.Add(device.Mode==0?"NA":device.Para_Addr.ToString());                     
                    item.SubItems.Add(device.Mode==0?"N":(device.Mode==1?"F":"M"));
                    item.SubItems.Add(device.Mode==0? "NA" : device.TapDiff.ToString());
                    item.SubItems.Add(device.Mode==0?"NA":device.MasterTO.ToString());
                    item.SubItems.Add(device.Mode==0 ? "NA" : device.numFollowers.ToString());
                    if (device.Mode != 0)
                    {
                        item.SubItems.Add((device.option & 0x01) == 0x01 ? "Yes" : "No");
                        item.SubItems.Add((device.option & 0x02) == 0x02 ? "Neg." : "Pos.");

                        item.SubItems.Add((device.option & 0x04) == 0x04 ? "Yes" : "No");
                        item.SubItems.Add((device.option & 0x08) == 0x08 ? "Neg." : "Pos.");

                        item.SubItems.Add((device.option & 0x10) == 0x10 ? "Yes" : "No");
                        item.SubItems.Add((device.option & 0x20) == 0x20 ? "Neg." : "Pos.");
                    }
                    else
                    {
                        item.SubItems.Add("NA");
                        item.SubItems.Add("NA");
                        item.SubItems.Add("NA");
                        item.SubItems.Add("NA");
                        item.SubItems.Add("NA");
                        item.SubItems.Add("NA");
                    }
                    item.SubItems[0].BackColor = item.SubItems[1].BackColor = item.SubItems[2].BackColor = Color.Yellow;
                    item.SubItems[3].BackColor = item.SubItems[4].BackColor = item.SubItems[5].BackColor = item.SubItems[6].BackColor = item.SubItems[7].BackColor = Color.LightGreen;
                    item.Tag = device.option.ToString();
                    btnApply.Enabled = btnAlarmReset.Enabled = (CDataSet.DataSet.Device.AccessControl.AccessLevel > 1) ? true : false;//Level 2 only 
                }
                listDevice.EndUpdate();
                this.listDevice.Sort();
                lvwColumnSorter.SortColumn = 0;//fixes weird bug, don't know why
                lvwColumnSorter.Order = SortOrder.None;//fixes weird bug, don't know why
                 
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_Broadcast != null)
                m_Broadcast.Close();
        }

        private void MasterFollower_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            CCommonFunctions.OpenFileWithSystemProgram("pdf files (*.pdf)|*.pdf");
        }

        private void applyConfig(object sender, EventArgs e)
        {
            string ipaddress;
            TCPFileTransfer FileTransfer = new TCPFileTransfer();
            int cnt;
            bool noneSelected=true;

            cnt = listDevice.Items.Count;
            if (listDevice.Items.Count != 0)
            {
                for (int i = 0; i < cnt; i++)
                {
                    if (listDevice.Items[i].Selected)
                    {
                        noneSelected=false;
                        if (ValidateUserInput(listDevice.Items[i]))
                        {
                            string mfSettings = buildStream(listDevice.Items[i]);
                            byte[] byteArray = Encoding.ASCII.GetBytes(mfSettings);
                            ipaddress = listDevice.Items[i].SubItems[1].Text;
                            if (CMessages.ShowConfirmationMessage("Click OK to send to IP Address: " + ipaddress, "Apply Configuration") == DialogResult.OK)
                            {
                                
                                MemoryStream sendStream = new MemoryStream(byteArray);
                                if (FileTransfer.UpdateMFsettings(ipaddress, sendStream))
                                {
                                    CMessages.ShowInformationMessage("Successfully uploaded settings.", "OK");
                                }
                                else
                                {
                                    CMessages.ShowInformationMessage("Failed to transfer settings.", "ERROR");
                                }
                            }
                        }
                        else
                        {
                            CMessages.ShowInformationMessage("Out of Range values for device IP Address: " + listDevice.Items[i].SubItems[1].Text, "Error - Out of range");
                        }

                    }
                }
                listDevice.Focus();
            }
            if(noneSelected)
                CMessages.ShowInformationMessage("No device(s) selected.","Error");
        }

        private bool ValidateUserInput(ListViewItem listViewItem)
        {
            int MstrTO;
            if (listViewItem.SubItems[11].Text != "NA")
            {
               MstrTO= Convert.ToUInt16( listViewItem.SubItems[11].Text);
               if (MstrTO < 1000 || MstrTO >60000)
                    MessageBox.Show("'Master Timeout' value is out of range. Timeout setting will not be changed.", "ERROR - Illegal Value");
            }
            return true;
        }

        private string buildStream(ListViewItem listDevice)
        {
            try
            { 
               UInt16 currentOption = Convert.ToUInt16(listDevice.Tag);
               SetCurrentOption(ref currentOption, listDevice);
               string MFMODE = listDevice.SubItems[9].Text=="N"?"0":(listDevice.SubItems[9].Text=="F"?"1":"2");
               string rtnStr =
                   @"<SETPOINTS><SPs ID=""IEC61850""><SP    ID=""MFMODE""	V=""" + MFMODE + @""" REG=""5050"" />";
               if (listDevice.SubItems[12].Text != "NA")
                   rtnStr = rtnStr + @"<SP	ID=""FOLLOWERS""	V=""" + listDevice.SubItems[12].Text + @""" REG=""5051""	/>";
               if (listDevice.SubItems[11].Text != "NA")
                   rtnStr = rtnStr + @"<SP	ID=""FOLLOWERTIMEOUT""	V=""" + listDevice.SubItems[11].Text + @""" REG=""5052""/>";                               
               rtnStr = rtnStr + @"<SP	ID=""BRKOPT""	V=""" + currentOption.ToString() +@""" REG=""5054""/>";
               if (listDevice.SubItems[10].Text != "NA")   
               rtnStr = rtnStr + @"<SP ID=""TAPDIFF"" V=""" + listDevice.SubItems[10].Text + @""" REG=""5055""/>";
               rtnStr = rtnStr + @"<SP ID=""PARADDR"" V=""" + listDevice.SubItems[8].Text + @""" REG=""5056""/>";
               rtnStr = rtnStr + @"<SP ID=""PARALTYP"" V=""5"" REG=""4717"" />";//change paralleling type (configuration menu) 
               rtnStr = rtnStr + @"</SPs>"+ @"</SETPOINTS>";
               return rtnStr;
            }
            catch { return null; }
        }

        private void SetCurrentOption(ref ushort currentOption, ListViewItem listDevice)
        {
            //assumes all breaker options are in succession in the table
            int i;
            for (i = 13; i < 19; i++)
            {
                if (listDevice.SubItems[i].Text != "NA")
                {
                    int mask = ( 0x1 << (i - 13));   
                    if (listDevice.SubItems[i].Text == "No" || listDevice.SubItems[i].Text == "Pos.")
                        currentOption &= (ushort)(~mask);
                    else
                        currentOption |= (ushort)mask;
                }
            }

        }


        private void CloseForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }          

       

        private void lbTDiff_Validated(object sender, EventArgs e)
        {
           
            if (listDevice.Items.Count > 0)
            {
                CurSelItem = listDevice.LVItemIndex;
                itemcount = listDevice.Items.Count;
            }
            for (int i = 0; i < itemcount; i++)
            {
                if (i != CurSelItem)
                    listDevice.Items[i].SubItems[10].Text = listDevice.Items[CurSelItem].SubItems[10].Text;
            }
            MessageBox.Show("'Tap Difference' for all the devices are now equal. Apply the configuration to all the devices for the change to take effect.", "Warning - TAP DIFF. EQUAL");

        }      

        private void lbFlRs_Validated(object sender, EventArgs e)
        {
            if (listDevice.Items.Count > 0)
            {
                CurSelItem = listDevice.LVItemIndex;
                itemcount = listDevice.Items.Count;
            }
            for (int i = 0; i < itemcount; i++)
            {
                if (i != CurSelItem)
                    listDevice.Items[i].SubItems[12].Text = listDevice.Items[CurSelItem].SubItems[12].Text;
            }
            MessageBox.Show("'Number of Devices' for all the devices are now equal. Apply the configuration to all the devices for the change to take effect.", "Warning - NUMBER OF FOLLOWERS");

        }
      

     
        private void exUpDownMTO_Leave(object sender, EventArgs e)
        {
             ExUpDown c =(ExUpDown)sender;
            if (listDevice.Items.Count > 0)
            {
                CurSelItem = listDevice.LVItemIndex;
                itemcount = listDevice.Items.Count;
            }
            for (int i = 0; i < itemcount; i++)
            {
                if (i != CurSelItem)
                    listDevice.Items[i].SubItems[11].Text =c.Value.ToString();
            }
            MessageBox.Show("'Master TimeOut' for all the devices are now equal.Upload the configuration to all the devices for the change to take effect.", "Warning - MASTER TIMEOUT");
         
        }

        private void listDevice_ColumnClick(object sender, ColumnClickEventArgs e)
        {

             
            //// Determine if clicked column is already the column that is being sorted.
            //if (e.Column == 0)
            //{
            //    // Reverse the current sort direction for this column.
            //    if (lvwColumnSorter.Order == SortOrder.Ascending)
            //    {
            //        lvwColumnSorter.Order = SortOrder.Descending;
            //    }
            //    else
            //    {
            //        lvwColumnSorter.Order = SortOrder.Ascending;
            //    }
            //}
            //else
            //{
            //    // Set the column number that is to be sorted; default to ascending.
            //    lvwColumnSorter.SortColumn = e.Column;
            //    // Reverse the current sort direction for this column.
            //    if (lvwColumnSorter.Order == SortOrder.Ascending)
            //    {
            //        lvwColumnSorter.Order = SortOrder.Descending;
            //    }
            //    else
            //    {
            //        lvwColumnSorter.Order = SortOrder.Ascending;
            //    }
            //}
            if (toggle == SortOrder.Ascending)
                toggle = SortOrder.Descending;
            else
                toggle = SortOrder.Ascending;
            lvwColumnSorter.Order = toggle;
            lvwColumnSorter.SortColumn = e.Column;

            // Perform the sort with these new sort options.
            this.listDevice.Sort();
            lvwColumnSorter.SortColumn = 0;//fixes weird bug, don't know why
            lvwColumnSorter.Order = SortOrder.None;//fixes weird bug, don't know why        
        }

        private void btnAlarmReset_Click(object sender, EventArgs e)
        {

             string ipaddress;
             TCPFileTransfer FileTransfer = new TCPFileTransfer();
             int cnt;
             bool noneSelected = true;

             cnt = listDevice.Items.Count;
             if (listDevice.Items.Count != 0)
             {
                 for (int i = 0; i < cnt; i++)
                 {
                     if (listDevice.Items[i].Selected)
                     {
                         noneSelected = false;
                         string mfSettings = @"<SETPOINTS><SPs ID=""IEC61850""><SP  ID=""MFMODE""	V=""1"" REG=""5053"" />";
                         mfSettings = mfSettings + @"</SPs>" + @"</SETPOINTS>";
                         byte[] byteArray = Encoding.ASCII.GetBytes(mfSettings);
                         ipaddress = listDevice.Items[i].SubItems[1].Text;
                         if (CMessages.ShowConfirmationMessage("Click OK to send to IP Address: " + ipaddress, "Alarm Reset") == DialogResult.OK)
                         {
                             MemoryStream sendStream = new MemoryStream(byteArray);
                             if (FileTransfer.UpdateMFsettings(ipaddress, sendStream))
                                 CMessages.ShowInformationMessage("Successfully sent reset command.", "OK");
                             else
                                 CMessages.ShowInformationMessage("Failed to send reset command.", "ERROR");
                         }


                     }
                 }
                 listDevice.Focus();
             }
             if (noneSelected)
                 CMessages.ShowInformationMessage("No device(s) selected.", "Error");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            btnDiscover.Enabled = true;
            timer1.Stop();
        }

        private void MasterFollower_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                if (autoSize == null) return;
                CAutoSize.EnableRedraw(this, false);
                this.SuspendLayout();
                autoSize.ChangeSize();
                this.ResumeLayout();
                CAutoSize.EnableRedraw(this, true);
                this.Refresh();
            }
            catch { }
        }

    }
}