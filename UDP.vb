﻿Imports System.Net
Imports System.Text
Imports System.Net.Sockets
Imports System.Threading
Imports System.Net.NetworkInformation

Namespace S2001D
    Public Class M2001D
        Public Delegate Sub Delegate_FindMFDevice(ByVal device As CMFDevice)

        Public Class CMFDevice
            Public SN As Integer
            Public UserLine As String
            Public IPAddress As String
            Public Mode As Short
            Public MasterTO As Integer
            Public numFollowers As Short
            Public options As Short
            Public TapDiff As Short
            Public status As Short
            Public Para_Addr As Short
            Public AuxStatus As Short
            Public TapPos As SByte
        End Class

        Public Class CMFBroadcast
            Private m_Receiver As System.Windows.Forms.Control
            Const m_HostPort As UInteger = 55000
            Const m_Timeout As Integer = 3200   '5000
            Const m_ReceivedDataSize As Integer = 62        'in bytes
            Private m_ListenPort As UInteger
            Public good_counter As Integer = 0
            Public bad_counter As Integer = 0

            Private m_Thread As Thread
            Private m_Running As Boolean = False
            Private m_SendSocket As Socket
            Private NicAddress As IPEndPoint

            Public OnFindMFDevice As Delegate_FindMFDevice

            Public Sub CMFBroadcast(ByVal control As System.Windows.Forms.Control)
                m_Receiver = control
                m_Running = False
            End Sub

            Public Sub Close()
                If m_Thread IsNot Nothing Then
                    If m_Running Then
                        m_Running = False
                        Thread.Sleep(100)
                    End If

                    If m_Thread.IsAlive Then
                        If Not m_Thread.Join(100) Then
                            m_Thread.Abort()
                        End If
                    End If
                End If
                m_Thread = Nothing
            End Sub

            Public Function Send(ByVal ipAddress As IPAddress, ByVal port As UInteger) As Boolean
                Try

                    m_ListenPort = port
                    m_SendSocket = New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
                    m_SendSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, True)
                    m_SendSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, False)

                    Dim ipe As IPEndPoint = New IPEndPoint(ipAddress, port)
                    NicAddress = ipe    'save address,port of NIC card to bind the listen socket too(fixes XP issue)
                    m_SendSocket.Bind(ipe)  'Xp ignores bind and send to all NIC installed on the computer support.microsoft.com/kb/175396
                    m_SendSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1)   'UDP broadcast message 

                    Dim iep As New IPEndPoint(ipAddress.Broadcast, CInt(m_HostPort))
                    If Not m_Running Then
                        Listen()
                    End If
                    Thread.Sleep(100)
                    m_SendSocket.SendTo(GetRequestData(port), iep)
                    m_SendSocket.Close()
                Catch ex As Exception
                    'MessageBox.Show(ex.ToString)
                    Return False
                End Try
                Return True
            End Function

            Private Sub Listen()
                m_Thread = New Thread(New ThreadStart(AddressOf OnListening))
                'm_Thread.Priority = ThreadPriority.Highest
                m_Thread.Start()
                'Thread.Sleep(100)
            End Sub

            'Dim buffer As Byte() = New Byte(11) {}

            Private Function GetRequestData(ByVal port As UInteger) As Byte()
                Dim buffer() As Byte = New Byte(12) {}
                Dim tag As Byte() = Encoding.ASCII.GetBytes("GETBECKWID")
                For i As Integer = 0 To 9
                    buffer(i) = tag(i)
                Next
                buffer(10) = CByte(port And &HFF)
                buffer(11) = CByte(port >> 8)
                Return buffer
            End Function

            Public m_ReceiverSocket As Socket
            Public m_ReceiveBuffer As Byte()
            Private m_IPSender As IPEndPoint
            Private m_bRunReceive As Boolean
            Private m_Args() As Object

            Private Sub OnListening()
                Try
                    m_bRunReceive = True
                    m_Running = True
                    m_Args = New Object(1) {}
                    m_ReceiveBuffer = New Byte(m_ReceivedDataSize) {}
                    m_ReceiverSocket = New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
                    m_ReceiverSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, True)
                    m_ReceiverSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, False)
                    m_ReceiverSocket.Bind(NicAddress)

                    'Dim ipEndPoint As New IPEndPoint(IPAddress.Any, CInt(m_ListenPort))
                    'm_ReceiverSocket.Bind(ipEndPoint)

                    Dim ipeSender As IPEndPoint = New IPEndPoint(IPAddress.Any, CInt(m_ListenPort))
                    Dim epSender As EndPoint = DirectCast(ipeSender, EndPoint)
                    m_IPSender = ipeSender 

                    Dim watch As New Stopwatch()
                    watch.Start()
                    While m_Running
                        If watch.ElapsedMilliseconds >= m_Timeout Then
                            watch.Stop()
                            m_Running = False
                            Exit While
                        End If
                        If m_bRunReceive Then
                            m_bRunReceive = False
                            m_ReceiverSocket.BeginReceiveFrom(m_ReceiveBuffer, 0, m_ReceiveBuffer.Length,
                                                              SocketFlags.None, epSender,
                                                              New AsyncCallback(AddressOf OnReceive), m_ReceiverSocket)

                        End If
                    End While
                    m_ReceiverSocket.Close()
                    m_ReceiverSocket = Nothing
                Catch
                    m_Running = False
                End Try
                m_Running = False
                Form1.udp_message_textbox.Text = MasterFollower
            End Sub

            Dim MasterFollower As String = ""

            Private Sub OnReceive(ByVal ar As IAsyncResult)
                'Char[10] "MFBECKWDEV" 0-9
                'Short Serial Number   10-11
                'Char[20] Logo1        12-31
                'Char[20] Logo2        32-51
                'Short Timeout         52-53
                'short Mode             54
                'short Num of Followers 55
                'short Option           56
                'short Tap Difference   57
                'short Status           58
                'short Parallel.Addr    59
                'short Aux Status       60
                'short Tap Position     61
                'Total: 62Bytes

                Try

                    If m_ReceiverSocket Is Nothing Then
                        m_bRunReceive = True
                        Return
                    End If
                    Dim socket As Socket = DirectCast(ar.AsyncState, Socket)
                    Dim epSender As EndPoint = DirectCast(m_IPSender, EndPoint)
                    Dim recv As Integer = m_ReceiverSocket.EndReceiveFrom(ar, epSender)

                    If recv = m_ReceivedDataSize Then
                        good_counter += 1
                        Dim header As String = Encoding.ASCII.GetString(m_ReceiveBuffer, 0, 10)
                        If header = "MFBECKWDEV" Then
                            Dim device As New CMFDevice()
                            device.SN = CInt(CUInt(m_ReceiveBuffer(10)) Or (CUInt(m_ReceiveBuffer(11)) << 8))
                            device.UserLine = Encoding.ASCII.GetString(m_ReceiveBuffer, 12, 40)
                            device.IPAddress = DirectCast(epSender, IPEndPoint).Address.ToString()
                            Dim a_IPAddress As IPAddress = DirectCast(epSender, IPEndPoint).Address
                            device.Mode = CShort(m_ReceiveBuffer(54))
                            device.MasterTO = CInt(CUInt(m_ReceiveBuffer(52)) Or (CUInt(m_ReceiveBuffer(53)) << 8))
                            device.numFollowers = CShort(m_ReceiveBuffer(55))
                            device.options = CShort(m_ReceiveBuffer(56))
                            device.TapDiff = CShort(m_ReceiveBuffer(57))
                            device.status = CShort(m_ReceiveBuffer(58))
                            device.Para_Addr = CShort(m_ReceiveBuffer(59))
                            device.AuxStatus = CShort(m_ReceiveBuffer(60))

                            'Dim deviceTapPosition As String = ""    'space holder
                            'Select Case m_ReceiveBuffer(61)
                            '    Case Is = 0
                            '        device.TapPos = 0
                            '        deviceTapPosition &= device.TapPos
                            '    Case Is < 125
                            '        'MessageBox.Show("Tap Pos is positive or 0")
                            '        device.TapPos = CSByte(m_ReceiveBuffer(61))
                            '        deviceTapPosition &= device.TapPos & "R"
                            '    Case Is > 125
                            '        'MessageBox.Show("Tap Pos is negative")
                            '        device.TapPos = CSByte(&HFF - (m_ReceiveBuffer(61)) + 1)
                            '        deviceTapPosition &= device.TapPos & "L"
                            'End Select

                            Dim devicestatus As String = ""
                            If device.status > &H80 Then
                                devicestatus = "Alarm"
                            End If

                            Dim MorF As String = ""
                            Select Case device.status
                                Case &H80, &H0
                                    MorF = "None"
                                Case &H81, &H1
                                    MorF = "Follower"
                                Case &H82, &H2
                                    MorF = "Master"
                            End Select
                            
                            MasterFollower &= device.Para_Addr & "," & device.IPAddress & "," & MorF & ",_" & device.AuxStatus & "," & devicestatus & "," &
                                                device.MasterTO & ",_" & device.SN & ","
                            Form1.IPAddresses.SetValue(a_IPAddress, good_counter - 1)
                        End If
                    Else
                        bad_counter += 1
                    End If
                    m_bRunReceive = True
                Catch
                End Try
            End Sub
        End Class
    End Class
End Namespace

