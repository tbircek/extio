using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Setpoint;
using System.Threading;
using System.Timers;
using System.Diagnostics;
using System.Net.NetworkInformation;

namespace S2001D //DiscoveryDevice
{
    public delegate void Delegate_FindMFDevice(CMFDevice device);

    public class CMFDevice
    {
        public int SN;
        public string UserLine;
        public string IPAddress;
        public short Mode;
        public int MasterTO;
        public short numFollowers;
        public short option;
        public short TapDiff;
        public short status;
        public short Para_Addr;
        public short AuxStatus;
        public sbyte TapPos;
    }

    public class CMFDeviceCollection : CCollection<CMFDevice>
    {
    }

    public class CMFBroadcast
    {
        System.Windows.Forms.Control m_Receiver;
        const uint m_HostPort = 55000;
        const int m_Timeout = 3200;        
        const int m_ReceivedDataSize = 62; //in bytes
        uint m_ListenPort;
        public   int good_counter = 0;
        public  int bad_counter = 0;


        Thread m_Thread;
        bool m_Running = false;
        Socket m_SendSocket;
        IPEndPoint NicAddress;

        public Delegate_FindMFDevice OnFindMFDevice;

        public CMFBroadcast(System.Windows.Forms.Control control)
        {
            m_Receiver = control;
            m_Running = false;
        }

		public void Close()
		{
			if (m_Thread != null) {
                if (m_Running)
                {
                    m_Running = false;
                    Thread.Sleep(100);
                }
              
				if (m_Thread.IsAlive) {
					if (!m_Thread.Join(100)) {
						m_Thread.Abort();
					}
				}
			}
            m_Thread = null;
		}

        public bool Send(IPAddress ipAddress, uint port)
        {
            try
            {
                m_ListenPort =  port;
                m_SendSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                m_SendSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                m_SendSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, false);
                IPEndPoint ipe = new IPEndPoint(ipAddress, (int)port);
                NicAddress = ipe;//save address,port of NIC card to bind the listen socket too(fixes XP issue)
                m_SendSocket.Bind(ipe);//Xp ignores bind and send to all NIC installed on the computer support.microsoft.com/kb/175396
                m_SendSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);//UDP broadcast message 
                IPEndPoint iep = new IPEndPoint(IPAddress.Broadcast, (int)m_HostPort);

                if (!m_Running)
                    Listen();
                Thread.Sleep(100);
                m_SendSocket.SendTo(GetRequestData(port), iep);
                m_SendSocket.Close();
                //m_SendSocket = null;
            }
            catch { return false; };
            return true;
        }

        private void Listen()
        {
            m_Thread = new Thread(new ThreadStart(OnListening));
            m_Thread.Priority = ThreadPriority.Highest;
            m_Thread.Start();
        }

        private byte[] GetRequestData(uint port)
        {
            byte[] buffer = new byte[12];
            byte[] tag = Encoding.ASCII.GetBytes("GETBECKWID");
            for (int i = 0; i < 10; i++)
                buffer[i] = tag[i];
            buffer[10] = (byte)port;
            buffer[11] = (byte)(port >> 8);
            return buffer;
        }

        Socket m_ReceiverSocket;
        byte[] m_ReceiveBuffer;
        IPEndPoint m_IPSender;
        bool m_bRunReceive;
        object[] m_Args;
        //		CDeviceCollection m_Devices;
        private void OnListening()
        {
            
            try
            {
                m_bRunReceive = true;
                m_Running = true;
                m_Args = new object[1];
                m_ReceiveBuffer = new byte[m_ReceivedDataSize];
                //				m_Devices = new CDeviceCollection();

                m_ReceiverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                m_ReceiverSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                m_ReceiverSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, false);
                
                m_ReceiverSocket.Bind(NicAddress);

                IPEndPoint ipeSender = new IPEndPoint(IPAddress.Any, (int) m_ListenPort);
                EndPoint epSender = (EndPoint)ipeSender;
                m_IPSender = ipeSender;


                Stopwatch watch = new Stopwatch();
                watch.Start();
                while (m_Running)
                {
                    if (watch.ElapsedMilliseconds > m_Timeout)
                    {
                        watch.Stop();
                        m_Running = false;
                        m_bRunReceive = true;
                        break;
                    }
                    if (m_bRunReceive)
                    {
                        m_bRunReceive = false;
                        m_ReceiverSocket.BeginReceiveFrom(m_ReceiveBuffer, 0, m_ReceiveBuffer.Length, SocketFlags.None, ref epSender, new AsyncCallback(OnReceive), m_ReceiverSocket);  
                    }
           
                }

                m_ReceiverSocket.Close();
                m_ReceiverSocket = null;

            }
            catch
            {
                m_Running = false;
            }
            m_Running = false;
        }

        private void OnReceive(IAsyncResult ar)
        {
            //Char[10] "MFBECKWDEV" 0-9
            //Short Serial Number   10-11
            //Char[20] Logo1        12-31
            //Char[20] Logo2        32-51
            //Short Timeout         52-53
            //short Mode             54
            //short Num of Followers 55
            //short Option           56
            //short Tap Difference   57
            //short Status  (         58
            //short Parallel.Addr    59
            //short Aux Status       60
            //short Tap Position     61
            //Total: 62Bytes

            try
            {

                if (m_ReceiverSocket == null)
                {
                    m_bRunReceive = true;
                    return;
                }
            
                Socket socket = (Socket)ar.AsyncState;

                EndPoint epSender = (EndPoint)m_IPSender;
                int recv = socket.EndReceiveFrom(ar, ref epSender);

                if (recv == m_ReceivedDataSize)
                {
                    good_counter++;
                    string header = Encoding.ASCII.GetString(m_ReceiveBuffer, 0, 10);
                    if (header == "MFBECKWDEV")
                    {
                        CMFDevice device = new CMFDevice();
                        device.SN = (int)(((uint)m_ReceiveBuffer[10]) | (((uint)m_ReceiveBuffer[11]) << 8));
                        device.UserLine = Encoding.ASCII.GetString(m_ReceiveBuffer, 12, 40);
                        device.IPAddress = ((IPEndPoint)epSender).Address.ToString();
                        device.Mode = (short)m_ReceiveBuffer[54];
                        device.MasterTO = (int)(((uint)m_ReceiveBuffer[52]) | (((uint)m_ReceiveBuffer[53]) << 8));
                        device.numFollowers = (short)m_ReceiveBuffer[55];
                        device.option = (short)m_ReceiveBuffer[56];
                        device.TapDiff = (short)m_ReceiveBuffer[57];
                        device.status = (short)m_ReceiveBuffer[58];
                        device.Para_Addr = (short)m_ReceiveBuffer[59];
                        device.AuxStatus = (short)m_ReceiveBuffer[60];
                        device.TapPos = (sbyte)m_ReceiveBuffer[61];

                        if (OnFindMFDevice != null)
                        {
                            m_Args[0] = device;
                            m_Receiver.Invoke(OnFindMFDevice, m_Args);
                        }
                        //						}

                    }
                }
                else
                    bad_counter++;
                m_bRunReceive = true;
            }
            catch (System.ObjectDisposedException) { m_bRunReceive = true; return; }
            catch (SocketException) { m_bRunReceive = true; return; }
        }
    }
}
