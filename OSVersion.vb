﻿Imports System.Management

Namespace WMISample
    Module OSVersion
        Public Class MyWMIQuery

            Public Overloads Shared Function OSVersion() As String
                OSVersion = ""
                Try
                    Dim searcher As New ManagementObjectSearcher("root\CIMV2", "SELECT * FROM Win32_OperatingSystem")

                    For Each queryObj As ManagementObject In searcher.Get()
                        OSVersion = queryObj("Caption")
                    Next
                Catch err As ManagementException
                    Form1.SetTextMsgCenter(Form1.ConnectionStatusLabel, "An error occurred while retriving OS version ")
                End Try
                Return OSVersion
            End Function
        End Class
    End Module
End Namespace