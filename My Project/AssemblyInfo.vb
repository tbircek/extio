﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ExtIO")> 
<Assembly: AssemblyDescription("Extended input/output control")> 
<Assembly: AssemblyCompany("Beckwith Electric Inc.Co.")> 
<Assembly: AssemblyProduct("ExtIO")> 
<Assembly: AssemblyCopyright("Turgay Bircek")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("9035e1d0-f504-4c1a-b664-0f4dcf5d4853")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.1.5.12")> 
<Assembly: AssemblyFileVersion("1.2.0.0")> 
